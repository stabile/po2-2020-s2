package ar.edu.unq.po2.tp6.banco;

import java.util.ArrayList;
import ar.edu.unq.po2.tp6.cliente.Cliente;

public class BancoClientes {

    private ArrayList<Cliente> clientes;

    // Dependency injection
    public BancoClientes(ArrayList<Cliente> clientes) {
        super();
        this.clientes = clientes;
    }

    public void agregar(Cliente nuevoCliente) { clientes().add(nuevoCliente); }

    public ArrayList<Cliente> clientes() { return this.clientes; }

}
