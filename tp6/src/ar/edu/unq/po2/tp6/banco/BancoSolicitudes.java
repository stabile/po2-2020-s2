package ar.edu.unq.po2.tp6.banco;

import java.util.ArrayList;
import ar.edu.unq.po2.tp6.solicitud.SolicitudDeCredito;

public class BancoSolicitudes {

    protected ArrayList<SolicitudDeCredito> solicitudes;

    public BancoSolicitudes() {
        this.solicitudes = new ArrayList<SolicitudDeCredito>();
    }

    public void agregarSolicitudDeCredito(SolicitudDeCredito solicitud) {
        solicitudes.add(solicitud);
    }

    public double montoTotalEnSolicitudesAceptables() { 
        return solicitudes.stream()
                          .filter(s -> s.esAceptable())
                          .mapToDouble( s -> s.getMonto()) 
                          .sum();
    }


}
