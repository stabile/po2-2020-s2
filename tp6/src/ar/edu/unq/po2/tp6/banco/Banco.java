package ar.edu.unq.po2.tp6.banco;

import ar.edu.unq.po2.tp6.cliente.ClienteHumano;
import ar.edu.unq.po2.tp6.cliente.Cliente;
import ar.edu.unq.po2.tp6.solicitud.SolicitudDeCredito;

import java.util.ArrayList;

public class Banco { // Fecade

    private BancoClientes bancoClientes;
    private BancoSolicitudes bancoSolicitudes;

    // Dependency injection :|
    public Banco(BancoClientes bancoClientes, BancoSolicitudes bancoSolicitudes) {
        super();
        setBancoClientes(bancoClientes);
        setBancoSolicitudes(bancoSolicitudes);
    }
    
    public void agregar(Cliente nuevoCliente) {
        bancoClientes.agregar(nuevoCliente);
    }

    public void agregarSolicitudDeCredito(SolicitudDeCredito solicitud) {
        bancoSolicitudes.agregarSolicitudDeCredito(solicitud);
    }

    private void setBancoClientes(BancoClientes bancoClientes) { 
        this.bancoClientes    = bancoClientes;
    }

    private void setBancoSolicitudes(BancoSolicitudes bancoSolicitudes) {
        this.bancoSolicitudes = bancoSolicitudes;
    }

    public double montoTotalEnSolicitudesAceptables() {
        return bancoSolicitudes.montoTotalEnSolicitudesAceptables();
    }

}
