package ar.edu.unq.po2.tp6.propiedad;

public class Propiedad {

    private double valorFiscal;

    public Propiedad(String descripción, String dirección, double valorFiscal ) {
        setValorFiscal(valorFiscal);
    }

    public double getValorFiscal() {return this.valorFiscal;}

    private void setValorFiscal(double valor) { this.valorFiscal = valor; }
}
