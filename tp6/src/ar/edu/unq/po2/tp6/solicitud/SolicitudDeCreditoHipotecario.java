package ar.edu.unq.po2.tp6.solicitud;

import ar.edu.unq.po2.tp6.cliente.Cliente;
import ar.edu.unq.po2.tp6.propiedad.Propiedad;

public class SolicitudDeCreditoHipotecario extends Solicitud{

    private Propiedad propiedad;
    private final double RATIO_CUOTA_INGRESO_MENSUAL = 0.5;
    private final double RATIO_MONTO_VALOR_FISCAL = 0.7;
    private final int    LIMITE_EDAD_A_LA_CANCELACION = 65;

    public SolicitudDeCreditoHipotecario(Cliente cliente, double monto, int cuotas, Propiedad propiedad) {
    	super(cliente, monto, cuotas);
        this.propiedad = propiedad;
    }
    
    /*
     * Para ser aceptadas, las solicitudes de créditos hipotecarios requieren que el monto
     *  de la cuota no supere el 50% de los ingresos mensuales del titular, que el monto 
     *  total solicitado no sea mayor al 70% del valor fiscal de la garantía, y que la 
     *  persona no supere los 65 años de edad antes de terminar de pagar el crédito.
     *         montoDeCuota <= ingresoMensual / 2 
     *      && montoCredito <= propiedad * 0.7 
     *      && cliente.fechaDeNacimiento + solicitudDeCredito.cantidadDeCuotas <= 65 años
     */
    public boolean esAceptable() { return montoDeCuota() <= getCliente().getSueldoNetoMensual() * RATIO_CUOTA_INGRESO_MENSUAL
                                       && getMonto() <= getPropiedad().getValorFiscal() * RATIO_MONTO_VALOR_FISCAL
                                       && getCliente().getEdad() + getCuotas() / 12 < LIMITE_EDAD_A_LA_CANCELACION
                                           ; }

    private Propiedad getPropiedad() {
        return this.propiedad;
    }
}
