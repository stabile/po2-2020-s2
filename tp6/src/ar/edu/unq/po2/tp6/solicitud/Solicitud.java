package ar.edu.unq.po2.tp6.solicitud;

import ar.edu.unq.po2.tp6.cliente.Cliente;

public abstract class Solicitud implements SolicitudDeCredito {

    private double monto;
	private int    cuotas;
	private Cliente cliente;

	public Solicitud(Cliente cliente, double monto, int cuotas) {
		this.monto  = monto;
		this.cuotas = cuotas;
		this.cliente = cliente;
    }
    
    public abstract boolean esAceptable();

	protected double montoDeCuota() {
		return this.monto / this.cuotas;
	}

	protected Cliente getCliente() {
		return this.cliente;
	}

    public double getMonto() { 
        return this.monto;
    }
    
    protected int getCuotas() {
        return this.cuotas;
    }
    
}
