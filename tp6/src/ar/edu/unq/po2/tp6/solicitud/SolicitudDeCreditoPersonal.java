package ar.edu.unq.po2.tp6.solicitud;

import ar.edu.unq.po2.tp6.cliente.Cliente;

public class SolicitudDeCreditoPersonal extends Solicitud {

    private final double SUELDO_ANUAL_MINIMO_REQUERIDO = 15000.0;
    private final double RATIO_CUOTA_INGRESO_MENSUAL = 0.7;
	
	public SolicitudDeCreditoPersonal(Cliente cliente, double monto, int cuotas) {
		super(cliente, monto, cuotas);
	}

    /*Las solicitudes para los créditos personales requieren que el 
     * solicitante tenga ingresos anuales por al menos $15000, y 
     * que el monto de la cuota no supere el 70% de sus ingresos mensuales.
     * */

	@Override
	public boolean esAceptable() {
		return getCliente().sueldoNetoAnual() >= SUELDO_ANUAL_MINIMO_REQUERIDO 
            && montoDeCuota() <= getCliente().getSueldoNetoMensual() * RATIO_CUOTA_INGRESO_MENSUAL;
	}

}
