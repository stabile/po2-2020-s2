package ar.edu.unq.po2.tp6.solicitud;

public interface SolicitudDeCredito {

	public boolean esAceptable();
	public double getMonto();
}
