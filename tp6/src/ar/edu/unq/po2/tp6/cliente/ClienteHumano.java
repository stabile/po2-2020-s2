package ar.edu.unq.po2.tp6.cliente;

public class ClienteHumano implements Cliente {

    private String nombre;
    private String apellido;
    private String dirección;
    private int edad;
    private double sueldoNetoMensual;

    public ClienteHumano(String nombre, String apellido, String dirección, int edad, double sueldoNetoMensual) {
        super();
        this.nombre = nombre;
        this.apellido = apellido;
        this.dirección = dirección;
        this.edad = edad;
        this.sueldoNetoMensual = sueldoNetoMensual;
    }

    protected String getNombre() { return this.nombre; }

    protected String getApellido() { return this.apellido; }

    protected String getDirección() { return this.dirección; }

    public  int getEdad() { return this.edad; }

    public double getSueldoNetoMensual() { return this.sueldoNetoMensual; }

    @Override
	public double sueldoNetoAnual() { return getSueldoNetoMensual() * 12; }

}
