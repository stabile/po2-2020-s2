package openClosedPrinciple.caso1;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

public class ClientEmailTest {

    @DisplayName(" Todo Junto para el Coverage")
    @Test
    void test1() {
        Correo correo = new Correo("Asunto siniestro", "yo@here.now", "Nada que decir");
        IServidor servidorPop = new ServidorPop();
        servidorPop.resetear();
        servidorPop.realizarBackUp();
        servidorPop.tazaDeTransferencia();
        ClienteEMail clientEmail = new ClienteEMail(servidorPop, "A", "B");
        assertNotNull(clientEmail);
        clientEmail.borrarCorreo(correo);
        assertEquals(0, clientEmail.contarBorrados());
        assertEquals(0, clientEmail.contarInbox());
        clientEmail.eliminarBorrado(correo);
        clientEmail.recibirNuevos();
        clientEmail.enviarCorreo("Otro asunto", "to@here.now", "sin resolver");


    }
}
