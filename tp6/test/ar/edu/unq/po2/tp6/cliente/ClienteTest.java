package ar.edu.unq.po2.tp6.cliente;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

public class ClienteTest {

    ClienteHumano clienteJuan;
    Cliente clienteMaria;

    @BeforeEach
    void setUp() {
        clienteJuan  = new ClienteHumano("Juan", "Palotes", "Aqui 0", 42, 1000d);
        clienteMaria = new ClienteHumano("Maria", "Doe", "Alla 10", 37, 2000d);
    }

    @DisplayName("Cuando un creamos un ClienteHumano, se llama Juan Palotes vive en Aqui 0 tiene 42 años y gana 1000 mensuales")
    @Test
    void test1() {
        assertNotNull(clienteJuan);
        assertEquals("Juan" , clienteJuan.getNombre());
        assertEquals("Palotes" , clienteJuan.getApellido());
        assertEquals("Aqui 0" , clienteJuan.getDirección());
        assertEquals(42 , clienteJuan.getEdad());
        assertEquals(1000d , clienteJuan.getSueldoNetoMensual());
        assertEquals(12000d , clienteJuan.sueldoNetoAnual());
    }

    @DisplayName("Cuando Maria Doe tiene un sueldo Neto mensual del 2000, el sueldo neto anual es de 24000")
    @Test
    void testSueldoNetoAnual() {
        assertEquals(2000.0 , clienteMaria.getSueldoNetoMensual(), "El sueldo neto mensúal debería ser de 2000");
        assertEquals(24000.0, clienteMaria.sueldoNetoAnual(), "El sueldo anual debería ser de 24000");
    }
}
