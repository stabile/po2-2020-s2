package ar.edu.unq.po2.tp6.solicitud;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import ar.edu.unq.po2.tp6.propiedad.Propiedad;
import ar.edu.unq.po2.tp6.cliente.*;

public class SolicitudDeCreditoTest {
    Cliente clienteJuan ;
    Cliente clienteMaria ;
    Propiedad propiedadDeJuan ;
    Propiedad propiedadDeMaria ;
	SolicitudDeCredito solicitudDeCreditoHipotecarioDeJuanx10000;
	SolicitudDeCredito solicitudDeCreditoHipotecarioDeMaria;
	

 
    @BeforeEach
    void setUp() {
        clienteJuan = new ClienteHumano("Juan", "Palotes", "Aquí 0", 42, 1000d);
        clienteMaria = new ClienteHumano("Maria", "Crepes", "Allá  Quizá 21", 37, 2000d);
        propiedadDeJuan = new Propiedad("Casa", "Acá Tambien 7", 10000.0);
        propiedadDeMaria = new Propiedad("Casa", "Allá Podría 7", 10000.0);
		solicitudDeCreditoHipotecarioDeJuanx10000 = 
				new SolicitudDeCreditoHipotecario(clienteJuan,      // cliente
										          10000d,            // monto solicitado
										          25,               // cantidad de cuotas
										          propiedadDeJuan); // propiedad
		solicitudDeCreditoHipotecarioDeMaria = 
				new SolicitudDeCreditoHipotecario(clienteMaria,  
						                          1000d, 
						                          5, 
						                          propiedadDeMaria);
    }


    @DisplayName("Hay nueva solicitud de credito hipotecario")
    @Test
    void testCreditoHipotecario() {
        assertNotNull(solicitudDeCreditoHipotecarioDeJuanx10000);
    }
    
    @DisplayName("Monto de cuota de solicitud es igual al monto pedido dividido la cantidad de cuotas")
    @Test
    void testMontoDeCuota() {
    	assertEquals(400.0, ((Solicitud)solicitudDeCreditoHipotecarioDeJuanx10000).montoDeCuota());
    	assertEquals(200.0, ((Solicitud)solicitudDeCreditoHipotecarioDeMaria).montoDeCuota());
    }
    
    /*
     * Para ser aceptadas, las solicitudes de créditos hipotecarios requieren que el monto
     *  de la cuota no supere el 50% de los ingresos mensuales del titular, que el monto 
     *  total solicitado no sea mayor al 70% del valor fiscal de la garantía, y que la 
     *  persona no supere los 65 años de edad antes de terminar de pagar el crédito.
     *      montoDeCuota <= ingresoMensual / 2 
     *      && montoCredito <= propiedad * 0.7 
     *      && cliente.fechaDeNacimiento + solicitudDeCredito.cantidadDeCuotas <= 65 años
     */
    @DisplayName("Cuando Juan solicita un credito hipotecario le es rechazado monto > sueldoMensual")
    @Test
    void testSolicitudDeJuanNoEsAceptablePorBajoIngreso() {
        Cliente cliente = new ClienteHumano("Juan", "Palotes", "Aquí 0", 42, 1000d);
        Propiedad propiedad = new Propiedad("Casa", "Acá Tambien 7", 10000.0);
		Solicitud solicitudDeCreditoHipotecario= 
				new SolicitudDeCreditoHipotecario(cliente,      // cliente
										          10010d,       // monto solicitado
										          20,           // cantidad de cuotas
										          propiedad);   // propiedad
		double sueldo = cliente.getSueldoNetoMensual();
		double cuota  = solicitudDeCreditoHipotecario.montoDeCuota();
        assertEquals(500.5, cuota);
        assertEquals(1000, sueldo);
        assertFalse(suficienteSueldoParaCuota(sueldo, cuota));
    	assertFalse(solicitudDeCreditoHipotecario.esAceptable());
    }
    
    private boolean suficienteSueldoParaCuota(double sueldo, double cuota) {
    	return sueldo * 0.5 >= cuota;
    }

    @DisplayName("Cuando el 70% del valor fiscal de la propiedad en garantía es menor al monto solicitado es rechazado")
    @Test
    void test() {
        double valorFiscal = 48000.0;
        double montoSolicitado = valorFiscal * 0.7 + 1.0;
        double sueldo = 16000.0;
        int    cantidadDeCuotas  = 9;
        double cuota  = montoSolicitado / cantidadDeCuotas;
       
        Cliente cliente     = new ClienteHumano("", "", "", 42, sueldo);
        Propiedad propiedad = new Propiedad("", "", valorFiscal);
		Solicitud solicitudDeCreditoHipotecario = 
				new SolicitudDeCreditoHipotecario(cliente,      // cliente
										          montoSolicitado,            // monto solicitado
										          cantidadDeCuotas,               // cantidad de cuotas
										          propiedad); // propiedad
        assertEquals(montoSolicitado, solicitudDeCreditoHipotecario.getMonto());
        assertEquals(valorFiscal, propiedad.getValorFiscal());
        assertEquals(cuota, solicitudDeCreditoHipotecario.montoDeCuota());
        assertTrue(suficienteSueldoParaCuota(sueldo, cuota));
        assertFalse(suficienteValorParaMonto(valorFiscal, montoSolicitado));
    	assertFalse(solicitudDeCreditoHipotecario.esAceptable());
    }

	private boolean suficienteValorParaMonto(double valorFiscal, double montoSolicitado) {
		return valorFiscal * 0.7 >= montoSolicitado;
	} 
   
    @DisplayName("Cuando el cliente superare los 65 años al cancelar el crédito este es rechazado")
    @Test
    void test65() {
        Cliente cliente = new ClienteHumano("Juan", "Palotes", "Aquí 0", 63, 1000d);
        Propiedad propiedad = new Propiedad("Casa", "Acá Tambien 7", 48000);
		Solicitud solicitudDeCreditoHipotecario = 
				new SolicitudDeCreditoHipotecario(cliente,      // cliente
										          1000d,            // monto solicitado
										          24,           // cantidad de cuotas
										          propiedad); // propiedad

        assertTrue(suficienteValorParaMonto(propiedad.getValorFiscal(), solicitudDeCreditoHipotecario.getMonto()));
        assertFalse(solicitudDeCreditoHipotecario.esAceptable(), "El credito deberia haber sido rechazado");
    }

    @DisplayName("Cuando todos los requisistos son cumplidos el credito es aceptado")
    @Test
    void testSolucitudAceptada() {
        Cliente cliente = new ClienteHumano("J", "P", "A 3", 63, 1000d);
        Propiedad propiedad = new Propiedad("X" , "D", 10000d);
        Solicitud solicitudDeCreditoHipotecario = 
            new SolicitudDeCreditoHipotecario(cliente, 7000d, 14, propiedad);
        assertTrue(solicitudDeCreditoHipotecario.esAceptable(), "El credito debería haber sido aceptado");
    }
    
    @DisplayName("Cuando cliente gana menos de 15000 anuales la solicitud credito personal es rechazada")
    @Test
    void testSueldoNoSuficiente() {
    	Cliente cliente = new ClienteHumano("A", "B", "V", 25, 1249.0 );
    	Solicitud solicitudDeCreditoPersonal = 
    			new SolicitudDeCreditoPersonal(cliente, 10d, 1);
        assertFalse(solicitudDeCreditoPersonal.esAceptable(), "El crédito debería haber sido rechazado");
    }

    @DisplayName("Cuando la cuota supere el 70% del sueldoNetoMensual la solicitud de crédito es rechazada")
    @Test
    void testCuotaSuperaIngreso() {
        Cliente cliente = new ClienteHumano("A", "B", "V", 25 , 1250.0);
        Solicitud solicitudDeCreditoPersonal =
            new SolicitudDeCreditoPersonal(cliente, 875.1, 1);
        assertFalse(solicitudDeCreditoPersonal.esAceptable(), "El crédito debería ser rechazado");
    }

    @DisplayName("Cuando gana al menos 15000 y el monto de cuota no supera el 70% del ingreso mensual es aceptada la solicitud")
    @Test
    void testSueldoSuficiente() {
        Cliente cliente = new ClienteHumano("A", "B", "V", 25 , 1250.0);
        Solicitud solicitudDeCreditoPersonal =
            new SolicitudDeCreditoPersonal(cliente, 875.0, 1);
        assertTrue(solicitudDeCreditoPersonal.esAceptable(), "El crédito debería ser aceptado");
    }
}
