package ar.edu.unq.po2.tp6.propiedad;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

public class PropiedadTest {
    Propiedad propiedad; 


    @BeforeEach
    void setUp() {
        propiedad = new Propiedad("Casa", "Mas allá 17", 100000.0);
    }


    @DisplayName("Cuando creo un propiedad con valor fiscal 100000 su valor fiscal es 100000")
    @Test
    void testContructor() {
        assertEquals(100000.0, propiedad.getValorFiscal());
    }
}
