package ar.edu.unq.po2.tp6.banco;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import ar.edu.unq.po2.tp6.cliente.Cliente;
import ar.edu.unq.po2.tp6.cliente.ClienteHumano;
import ar.edu.unq.po2.tp6.solicitud.*;
import ar.edu.unq.po2.tp6.propiedad.*;

public class BancoTest {
    Banco banco;
    ArrayList<Cliente> clientesDeBanco ;
    BancoClientes bancoClientes ;
    BancoSolicitudes bancoSolicitudes ;
    
    private void assertCantidadDeClientes(int esperado) {
        assertEquals(esperado, clientesDeBanco.size(), "Deberia tener "+esperado+" clientes");
    }

    @BeforeEach
    void setUp() {
        clientesDeBanco = new ArrayList<Cliente>();
        bancoClientes = new BancoClientes(clientesDeBanco) { };
        bancoSolicitudes = new BancoSolicitudes() {};
        banco = new Banco(bancoClientes, bancoSolicitudes );
    }

    @DisplayName("hay banco y tiene una lista de clientes vacia")
    @Test
    void testHayBanco() {
        assertNotNull(bancoClientes.clientes());
        assertCantidadDeClientes(0);
    }

    @DisplayName("un banco tiene cliente nuevo")
    @Test
    void testClienteNuevo() {
        assertCantidadDeClientes(0);
        banco.agregar(nuevoClienteHumano());
        assertCantidadDeClientes(1);
    }

    Cliente nuevoClienteHumano() {
        return new ClienteHumano(null, null, null , 0, 0.0);
    }

    @DisplayName("Cliente presenta una solicitud de crédito")
    @Test
    void testAgregarSolicitudDeCredito() {
        assertCantidadDeSolicitudes(0);
        banco.agregarSolicitudDeCredito(nuevaSolicitudDeCreditoHipotecario());
        assertCantidadDeSolicitudes(1);
    }

    void assertCantidadDeSolicitudes(int esperada) {
        assertEquals(esperada, bancoSolicitudes.solicitudes.size(), "banco deberia tener "+esperada+" solicitud");
    }

    Solicitud nuevaSolicitudDeCreditoHipotecario(){
        return new SolicitudDeCreditoHipotecario(null, 0, 0, null);
    }

    @DisplayName("Cuando no hay solicitudes de credito el monto total a desembolsar es cero")
    @Test
    void testEvaluarSolicitudes() {
        assertCantidadDeSolicitudes(0);
        assertEquals(0, banco.montoTotalEnSolicitudesAceptables());
    }
    
    @DisplayName("Cuando hay una solicitud de crédito aceptable y por 1000 el monto total es 1000")
    @Test
    void testmontoTotalEnSolicitudes() {
        Cliente cliente = new ClienteHumano(null, null, null , 20, 11000.0);
        SolicitudDeCredito solicitud = new SolicitudDeCreditoHipotecario(cliente, 1000.0, 1, new Propiedad(null, null, 100000));
        banco.agregarSolicitudDeCredito(solicitud);
        assertEquals(1000, banco.montoTotalEnSolicitudesAceptables());
    }

    
    @DisplayName("Cuando hay ingresada una solicitud de credito y no es aceptable el monto total es 0")
    @Test
    void testSeEvaluaUnaSolicitudNoAceptable() {
        Cliente cliente = new ClienteHumano(null, null, null , 20, 11000.0);
        SolicitudDeCredito solicitud = new SolicitudDeCreditoHipotecario(cliente, 70000.1, 1, new Propiedad(null, null, 100000));
        banco.agregarSolicitudDeCredito(solicitud);
        assertEquals(0, banco.montoTotalEnSolicitudesAceptables());
    }
}
