package ar.edu.unq.po2.tp6.banco;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

public class BancoSolicitudesTest {

    BancoSolicitudes bancoSolicitudes;


    @BeforeEach
    void setUp() {
        bancoSolicitudes = new BancoSolicitudes();
    }


    @DisplayName("Hay BancoSolicitudes")
    @Test
    void testConstructor() {
        assertNotNull(new BancoSolicitudes());
    }

    @DisplayName("Agregar solicitud de Credito")
    @Test
    void testAgregarSolicitud() {
        assertNotNull(bancoSolicitudes.solicitudes);
    }
}
