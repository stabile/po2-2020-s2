package ar.edu.unq.po2.stock;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import ar.edu.unq.po2.producto.*;
import ar.edu.unq.po2.cobrable.ICobrable;
import java.util.List;
import java.util.ArrayList;

public class StockTest {

    Stock stock;
    List<Producto> productos;
    Producto leche;
    Producto garbanzos;
    Producto tomates;

    @BeforeEach
    void setUp() {
        productos = new ArrayList<Producto>(); 
        stock = new Stock(productos);
        leche = new ProductoDeEmpresa(55.0, stock);
        garbanzos = new ProductoDeEmpresa(100.0, stock);
        tomates = new ProductoDeCooperativa(100.0, stock);
    }

    @DisplayName("Stock existe")
    @Test
    void test1() {
        assertNotNull(stock);
    }

//    @DisplayName("Decrementar stock de un producto en un stock null levanta null exception")
//    @Test
//    void test4() {
//        stock = new Stock((List<Producto>)null);
//        assertThrows(NullPointerException.class,
//                () -> { stock.decrementar(leche); }
//                );
//    }

    @DisplayName("Decrementa el stock de un producto")
    @Test
    void test2() {
        assertEquals(3, productos.size());
        stock.decrementar(leche);
        assertEquals(2, productos.size());
    }

    @DisplayName("Decrementa un producto dos veces con tres en stock")
    @Test
    void testDecrementarProductoConDosEnStock() {
        assertEquals(3, productos.size());
        stock.decrementar(leche);
        assertEquals(2, productos.size());
        stock.decrementar(leche);
        assertEquals(2, productos.size());
    }
}
