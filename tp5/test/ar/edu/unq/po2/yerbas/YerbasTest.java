package ar.edu.unq.po2.yerbas;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import java.util.Collection;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

public class YerbasTest {

    @DisplayName("hay ColeccionadorDeObjetos ")
    @Test
    void test() {
        assertNotNull(new ColeccionadorDeObjetos());
    }

    @DisplayName("se obtiene el primero de una List")
    @Test
    void test1() {
        List list = new LinkedList<Integer>();
        list.add(1);
        list.add(17);
        ColeccionadorDeObjetos coleccionador = new ColeccionadorDeObjetos();
        assertEquals(1, coleccionador.getFirstFrom(list));
    }

    @DisplayName("addLast on Collection")
    @Test
    void test2() {
        Collection coleccion = new ArrayList<Integer>();
        coleccion.add(1);
        coleccion.add(17);
        ColeccionadorDeObjetos coleccionador = new ColeccionadorDeObjetos();
        coleccionador.addLast(42, coleccion);
        assertTrue(coleccion.contains(42));
        // wow .get() is not in the Collection protocol
        // but as ArrayLisy got .get()  on its protocol
        // we can cast and .get() the 42 from the 
        // "Collection" property and test if 
        // ColeccionadorDeObjetos has 42 at last place
        assertEquals(42, ((List)coleccion).get(2));
    }

    @DisplayName("subCollection contains")
    @Test
    void test3() {
        List vacia = new LinkedList<>();
        vacia.add(1);
        vacia.add(17);
        vacia.add(42);
        vacia.add(82);
        ColeccionadorDeObjetos cosoDeObjetos = new ColeccionadorDeObjetos();
        List subCollection = (List)cosoDeObjetos.getSubCollection(vacia, 1,3);
        assertEquals(subCollection.size(), 2);
        assertEquals(17, subCollection.get(0));
        assertEquals(42, subCollection.get(1));
        assertTrue(cosoDeObjetos.containsElement(subCollection, 42));
        assertTrue(cosoDeObjetos.containsElement(subCollection, 17));
        assertTrue(cosoDeObjetos.containsElement(vacia, 82));
    }
}
