package ar.edu.unq.po2.recaudadora;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import ar.edu.unq.po2.agencia.Agencia;
import ar.edu.unq.po2.factura.*;

public class RecaudadoraTest {

    Agencia recaudadora = new Recaudadora();
    Factura factura = new FacturaDeImpuesto(10, recaudadora);

    @DisplayName("hay recaudadora")
    @Test
    void test1() {
        assertNotNull(recaudadora);
    }

    @DisplayName("total recaudado es cero")
    @Test
    void test2() {
        assertEquals(0, ((Recaudadora)recaudadora).totalRecaudado());
    }

    @DisplayName("registra una factura y el total recaudado esta bien")
    @Test
    void test3() {
        recaudadora.registrarPago(factura);

        assertEquals(10, ((Recaudadora)recaudadora).totalRecaudado());
    }
}
