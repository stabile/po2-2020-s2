package ar.edu.unq.po2.caja;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import ar.edu.unq.po2.stock.Stock;
import ar.edu.unq.po2.producto.*;
import ar.edu.unq.po2.cobrable.ICobrable;
import ar.edu.unq.po2.agencia.Agencia;
import ar.edu.unq.po2.recaudadora.Recaudadora;
import ar.edu.unq.po2.factura.*;

public class CajaTest {

    Stock stock;
    ArrayList<Producto> productos;
    Caja caja; 
    Producto leche1;
    Producto leche2;
    Producto garbanzos;
    Producto tomates;
    Factura impuesto;
    Factura servicio;
    Agencia agencia;

    @BeforeEach
    void setUp() {
        productos = new ArrayList<Producto>();
        stock = new Stock(productos);
        agencia = new Recaudadora();
        
        leche1 = new ProductoDeEmpresa(55.0, stock);
        leche2 = new ProductoDeEmpresa(55.0, stock);
        garbanzos = new ProductoDeEmpresa(100.0, stock);
        tomates = new ProductoDeCooperativa(100.0, stock);
        impuesto = new FacturaDeImpuesto(56.78, agencia);
        servicio = new FacturaDeServicio(25.5, 5, agencia);

        caja  = new Caja(stock);
    }

    @DisplayName("hay caja")
    @Test
    void testHayUnaCaja() {
        assertNotNull(caja);
    }

    @DisplayName("un producto pasa por la caja")
    @Test
    void testUnProductoPasaPorLaCaja() {
        assertEquals(4, productos.size());
        assertEquals(0.0, caja.montoTotal());
        caja.facturarUnidad(leche1);
        assertEquals(3, productos.size());
        assertEquals(55.0, caja.montoTotal());
    }


    @DisplayName("tres productos pasan por la caja")
    @Test
    void testTresProductosPasanPorLaCaja() {
        assertEquals(4, productos.size());
        assertEquals(0.0, caja.montoTotal());
        caja.facturarUnidad(leche1);
        assertEquals(3, productos.size());
        assertEquals(55.0, caja.montoTotal());
        caja.facturarUnidad(leche2);
        assertEquals(2, productos.size());
        assertEquals(110.0, caja.montoTotal());
        caja.facturarUnidad(tomates);
        assertEquals(1, productos.size());
        assertEquals(200.0, caja.montoTotal());
    }

    
    @DisplayName("Se adquiere una lista de productos")
    @Test
    void testAdquirirLista() {
        List<ICobrable> lista = Arrays.asList(leche1, garbanzos, tomates);
        assertEquals(245.0, caja.facturar(lista));
    }
    
    @DisplayName("Se pasan un servicio y un impuesto por caja")
    @Test
    void testServicioEImpuesto() {
        List<ICobrable> lista = Arrays.asList(impuesto, servicio);
        assertEquals(184.28, caja.facturar(lista));
        assertEquals(184.28, ((Recaudadora)agencia).totalRecaudado());
    }

    @DisplayName("Se pasan facturas y productos")
    @Test
    void testFacturasYProductos() {
        List<ICobrable> lista = Arrays.asList(leche1, garbanzos, tomates, impuesto, servicio);
        assertEquals(245.0 + 184.28, caja.facturar(lista));
        assertEquals(184.28, ((Recaudadora)agencia).totalRecaudado());
    }

}
