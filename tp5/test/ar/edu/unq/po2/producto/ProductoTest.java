package ar.edu.unq.po2.producto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

import ar.edu.unq.po2.stock.Stock;
import java.util.List;
import java.util.ArrayList;

public class ProductoTest {


    Producto producto0;
    Producto producto1;
    Producto producto2;
    Stock stock;
    List<Producto> productos;

    @BeforeEach
    void setUp(){
        productos = new ArrayList<Producto>();
        stock     = new Stock(productos);
        producto0 = new ProductoDeEmpresa(0.0, stock);
        producto1 = new ProductoDeEmpresa(100.0, stock);
        producto2 = new ProductoDeCooperativa(100.0, stock);
    }

    @DisplayName("hay Producto")
    @Test
    void test1() {
        assertNotNull(producto0);
    }


    @DisplayName("hay Producto de Empresa con precio 100.0")
    @Test
    void test2() {
        assertEquals(100.0, producto1.precio());
    }

    @DisplayName("hay Producto de Cooperativa con precio 90.0")
    @Test
    void test3() {
        assertEquals(90.0, producto2.precio());
    }

    @DisplayName("Producto ocupa un lugar en el stock")
    @Test
    void testCheckStockExistencia() {
        assertEquals(3, productos.size());
    }
}
