package ar.edu.unq.po2.smalltalk2java;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;

public class Smalltalk2JavaTest {


    Persona juan;
    Persona gladys;
    Mascota neko;
    Mascota noir;

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));

        neko = new Mascota("Neko", "Gata");
        noir = new Mascota("Noir", "Gato");
        juan   = new Persona("Juan",   LocalDate.of(1963, 9, 19));
        gladys = new Persona("Gladys", LocalDate.of(1936, 4, 9));
    }

    @AfterEach
    void tearDown() {
        System.setOut(standardOut);
    }

    @DisplayName("Collecion de mascotas y personas tienen todos nombre")
    @Test
    void testColeccion() {
        ArrayList<Nombrable> coleccion = new ArrayList<Nombrable>();
        coleccion.add(gladys);
        coleccion.add(neko);
        coleccion.add(juan);
        coleccion.add(noir);
        for(Nombrable elemento : coleccion){ System.out.print(elemento.nombre()); }
        assertEquals("GladysNekoJuanNoir", outputStreamCaptor.toString());
    }

    @DisplayName("Mascota tiene nombre")
    @Test
    void testNombreMascota() {
        assertEquals("Neko", neko.nombre());
    }
    
    @DisplayName("Mascota tiene raza")
    @Test
    void testRaza() {
        assertEquals("Gata", neko.raza());
    }

    @DisplayName("Construir Mascota")
    @Test
    void testMascota() {
        assertNotNull(neko);
    }

    @DisplayName("Contruir Persona")
    @Test
    void test0() {
        assertNotNull(juan);
    }

    @DisplayName("Persona tiene nombre")
    @Test
    void test1() {
        assertEquals("Juan", juan.nombre());
    }
    
    @DisplayName("Persona tiene fechaDeNacimiento")
    @Test
    void testFechaDeNacimiiento() {
        LocalDate fecha = LocalDate.of(1963, 9, 19);
        assertEquals(fecha, juan.fechaDeNacimiento());
    }
    
    @DisplayName("Persona tiene edad")
    @Test
    void testEdad() {
        assertEquals(57, juan.edad());
    }

    @DisplayName("Una persona es menor que otra")
    @Test
    void testMenor() {
        assertTrue(juan.esMenorQue(gladys));
        assertFalse(gladys.esMenorQue(juan));
    }
}
