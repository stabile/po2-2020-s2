package ar.edu.unq.po2.cliente;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import ar.edu.unq.po2.caja.Caja;
import ar.edu.unq.po2.stock.Stock;
import ar.edu.unq.po2.producto.*;
import ar.edu.unq.po2.cobrable.ICobrable;


public class ClienteTest {
    
    Caja caja;
    Stock stock;
    List<Producto> listadoDeProductos;
    List<ICobrable> compra; 
    Producto leche;

    @BeforeEach
    void setUp(){
        listadoDeProductos = new ArrayList<Producto>();
        listadoDeProductos.add(leche);
        stock = new Stock(listadoDeProductos);
        caja = new Caja(stock);
        leche = new ProductoDeEmpresa(34.0, stock);
        compra = Arrays.asList(leche);
    }

    @DisplayName("hay Cliente")
    @Test
    void testExisteConstructor() {
        assertNotNull(new Cliente());
    }


    @DisplayName("Hace una compra y obtiene el total de la compra")
    @Test
    void testCompra() {
        Cliente cliente = new Cliente();
        cliente.hacerCompra(caja, compra);
        assertEquals(34.0, cliente.montoAPagar());
    }
}
