package ar.edu.unq.po2.factura;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

public class FacturaTest {

    Factura servicio;
    Factura impuesto;

    @BeforeEach
    void setUp() {
        ar.edu.unq.po2.agencia.Agencia recaudadora = new ar.edu.unq.po2.recaudadora.Recaudadora();
        servicio = new FacturaDeServicio(3.5, 4, recaudadora);
        impuesto = new FacturaDeImpuesto(11.27, recaudadora);
    }

    @DisplayName("Hay Factura")
    @Test
    void testHayFactura() {
        assertNotNull(servicio);
        assertNotNull(impuesto);
    }

    @DisplayName("Precio de la factura de servicio")
    @Test
    void testFacturaDeServicio() {
        assertEquals(14, servicio.precio());
    }
    
    @DisplayName("Precio del impuesto")
    @Test
    void testFacturaDeImpuesto() {
        assertEquals(11.27, impuesto.precio());
    }
}
