package ar.edu.unq.po2.cobrable;

public interface ICobrable {

    public double precio();

    public void registrarEn(Object agente);

}
