package ar.edu.unq.po2.yerbas;

import java.util.Collection;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

public class ColeccionadorDeObjetos {

    // XXX deberia implementar List o LinkedList
    // (Collection no contempla el msg get.)
    public Object getFirstFrom(List collection){ 
        return collection.get(0);
    }

    //YYY debe implementar Collection ( cualquiera de los estudiados)
    public void addLast (Object element, Collection collection) { 
        collection.add(element);
    }

    // ZZZ debe implementar List
    public Collection getSubCollection(List collection, int from, int to) {
        return collection.subList(from, to);
    }

    // WWW debe implementar Collection
    public Boolean containsElement(Collection collection, Object element) {
        return collection.contains(element);
    }
}

