package ar.edu.unq.po2.caja;

import ar.edu.unq.po2.stock.Stock;
import ar.edu.unq.po2.cobrable.ICobrable;
import java.util.ArrayList;
import java.util.List;

public class Caja {

    private Stock stock;
    private ArrayList<ICobrable> cobrables;

    public Caja(Stock stock) {
        this.setStock(stock);
        cobrables = new ArrayList<ICobrable>();
    }

    private void setStock(Stock stock) {
        this.stock = stock;
    }

    public double montoTotal() { return
        cobrables.stream()
            .reduce(0d, (z, p) -> p.precio() + z , Double::sum);
    }

    public double facturar(List<ICobrable> listaDeCobrables) {
        for(ICobrable unidad : listaDeCobrables) {
            facturarUnidad(unidad);
        }
        return montoTotal();
    }

    protected void facturarUnidad(ICobrable facturable){
        facturable.registrarEn(stock);
        cobrables.add(facturable);
    }
}
