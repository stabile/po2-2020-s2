package ar.edu.unq.po2.agencia;

import ar.edu.unq.po2.factura.Factura;

public interface Agencia {
    public void registrarPago(Factura f);
}
