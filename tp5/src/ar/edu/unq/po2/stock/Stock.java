package ar.edu.unq.po2.stock;

import java.util.ArrayList;
import java.util.List;
import ar.edu.unq.po2.producto.Producto;

public class Stock {

    List<Producto> productos;

    public Stock(List<Producto> productos) {
        this.setProductos(productos);
    }

    private void setProductos(List<Producto> productos) { 
        this.productos = productos;
    }

    public void decrementar(Producto producto) {
        productos.remove(producto);
    }

    public void agregar(Producto producto) {
        productos.add(producto);
    }
}
