package ar.edu.unq.po2.recaudadora;

import ar.edu.unq.po2.agencia.Agencia;
import ar.edu.unq.po2.factura.Factura;
import java.util.List;
import java.util.ArrayList;

public class Recaudadora implements Agencia {

    List<Factura> registro;

    public Recaudadora() {
        registro = new ArrayList<Factura>();
    }

    public double totalRecaudado(){
        return registro.stream().reduce(0d, (z, f) -> f.precio() + z , Double::sum);
//        return registro.stream().mapToDouble(Factura::monto).sum();
//        return registro.stream().mapToDouble(Factura::monto).reduce(0d, (z, m) -> m + z );
    }

    public void registrarPago(Factura factura){
        registro.add(factura);
    }
}
