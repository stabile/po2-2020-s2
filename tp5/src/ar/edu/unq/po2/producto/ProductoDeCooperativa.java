package ar.edu.unq.po2.producto;

import ar.edu.unq.po2.stock.Stock;

public class ProductoDeCooperativa extends Producto {

    public ProductoDeCooperativa(double precio, Stock stock) {
        super(precio, stock);
    }

    @Override
    public double precio() { return precioCon10x100DeDescuento(); }

    private double precioCon10x100DeDescuento() {
        return super.precio() * (1 - 0.10);
    }
}


