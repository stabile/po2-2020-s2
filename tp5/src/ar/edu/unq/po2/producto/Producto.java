package ar.edu.unq.po2.producto;

import ar.edu.unq.po2.cobrable.ICobrable;
import ar.edu.unq.po2.stock.Stock;

public abstract class Producto implements ICobrable {

    private double precio;

    public Producto(double precio, Stock stock) {
        this.setPrecio(precio);
        stock.agregar(this);
    }

    public double precio(){ return this.precio; }

    private void setPrecio(double precio) { this.precio = precio; }

    public void registrarEn(Object stock) {
        ((Stock)stock).decrementar(this);
    }
}

