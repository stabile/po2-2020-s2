package ar.edu.unq.po2.producto;

import ar.edu.unq.po2.stock.Stock;

public class ProductoDeEmpresa extends Producto{

    public ProductoDeEmpresa(double precio, Stock stock){
        super(precio, stock);
    }
}

