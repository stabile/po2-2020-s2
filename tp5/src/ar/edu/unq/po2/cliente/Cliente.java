package ar.edu.unq.po2.cliente;

import java.util.List;
import ar.edu.unq.po2.caja.Caja;
import ar.edu.unq.po2.cobrable.ICobrable;

public class Cliente {
    
    private double montoAPagar;

    public Cliente() { }

    public void hacerCompra(Caja caja, List<ICobrable> carrito) {
        setMontoAPagar(caja.facturar(carrito));
    }

    public double montoAPagar() { return this.montoAPagar; }

    private void setMontoAPagar(double monto) {
        this.montoAPagar = monto;
    }


}
