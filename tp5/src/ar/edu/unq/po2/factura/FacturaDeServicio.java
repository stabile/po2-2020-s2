package ar.edu.unq.po2.factura;

import ar.edu.unq.po2.agencia.Agencia;

public class FacturaDeServicio extends Factura{

    private double costoUnitario;
    private int unidadesConsumidas;

    public FacturaDeServicio(double costoUnitario, int unidadesConsumidas, Agencia agencia) {
        super(agencia);
        this.costoUnitario = costoUnitario;
        this.unidadesConsumidas = unidadesConsumidas;
    }

    public double precio() { return costoUnitario * unidadesConsumidas; }

}
