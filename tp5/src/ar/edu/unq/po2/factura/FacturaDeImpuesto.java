package ar.edu.unq.po2.factura;

import ar.edu.unq.po2.agencia.Agencia;

public class FacturaDeImpuesto extends Factura {

    private double tasa;

    public FacturaDeImpuesto(double tasa, Agencia agencia) {
        super(agencia);
        setTasa(tasa);
    }

    public double precio() { return this.tasa; }

    private void setTasa(double tasa){
        this.tasa = tasa;
    }
}
