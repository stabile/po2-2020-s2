package ar.edu.unq.po2.factura;

import ar.edu.unq.po2.cobrable.ICobrable;
import ar.edu.unq.po2.agencia.Agencia;

public abstract class Factura implements ICobrable {

    Agencia agencia;

    public Factura(Agencia agencia) {
        setAgencia(agencia);
    }

    //public double monto() { return 0; }
    
    public void registrarEn(Object nada) {
        this.agencia.registrarPago(this);
    }

    private void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }
       
}
