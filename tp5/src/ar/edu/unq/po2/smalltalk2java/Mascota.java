package ar.edu.unq.po2.smalltalk2java;

public class Mascota implements Nombrable {

    private String nombre;
    private String raza;

    public Mascota(String nombre, String raza) {
        super();
        setNombre(nombre);
        setRaza(raza);
    }

    public String nombre() { return this.nombre; }

    public String raza() { return this.raza; }

    private void setNombre(String aString) { this.nombre = aString; }

    private void setRaza(String aString) { this.raza = aString; }
}
