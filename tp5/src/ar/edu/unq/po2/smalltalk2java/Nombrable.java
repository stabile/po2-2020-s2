package ar.edu.unq.po2.smalltalk2java;

public interface Nombrable {
    public String nombre();
}
