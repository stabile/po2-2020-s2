package ar.edu.unq.po2.smalltalk2java;

import java.time.LocalDate;
import java.time.Period;

public class Persona implements Nombrable{

    private String nombre;
    private LocalDate fechaDeNacimiento;

    public Persona() {
        super();
        setNombre("nada");
        setFechaDeNacimiento(LocalDate.now());
    }

    public Persona(String nombre, LocalDate fechaDeNacimiento){
        this();
        setNombre(nombre);
        setFechaDeNacimiento(fechaDeNacimiento);
    }

    public boolean esMenorQue(Persona persona){ return this.edad() < persona.edad(); }

    private void setFechaDeNacimiento(LocalDate aDate) {
        this.fechaDeNacimiento = aDate;
    }

    public LocalDate fechaDeNacimiento() {
        return this.fechaDeNacimiento;
    }

    public String nombre() { return this.nombre; }

    public void setNombre(String aString) { 
        this.nombre = aString;
    }

    public int edad() { 
        return Period.between(this.fechaDeNacimiento, LocalDate.now()).getYears();
    }
}

