package videojuego;

public class Jugando extends EstadoDeLaMaquina {
    public Jugando(MaquinaDeVideoJuego maquina) {
        super(maquina);
    }

    @Override
    public void finDelJuego() {
        this.maquina().cambiarElEstado(new EsperandoIngresoDeFicha(this.maquina()));
    }
}
