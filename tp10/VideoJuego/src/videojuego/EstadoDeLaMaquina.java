package videojuego;

public abstract class EstadoDeLaMaquina {
    private MaquinaDeVideoJuego maquina;

    public EstadoDeLaMaquina(MaquinaDeVideoJuego maquina) {

        this.maquina = maquina;
    }

    public void fichaIngresada() { }

    public void inicioPresionado() { }

    public void finDelJuego() { }

    protected MaquinaDeVideoJuego maquina() {return this.maquina;}
}
