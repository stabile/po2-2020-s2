package videojuego;

public class EsperandoIngresoDeFicha extends EstadoDeLaMaquina {

    public EsperandoIngresoDeFicha(MaquinaDeVideoJuego maquina) {
        super(maquina);
    }

    @Override
    public void inicioPresionado() {
        System.out.print("Ingrese una ficha");
    }

    @Override
    public void fichaIngresada() {
        this.maquina().cambiarElEstado(new ListoParaUnJugador(this.maquina()));
    }
}
