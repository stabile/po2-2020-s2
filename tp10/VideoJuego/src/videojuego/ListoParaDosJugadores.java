package videojuego;

public class ListoParaDosJugadores extends EstadoDeLaMaquina {
    public ListoParaDosJugadores(MaquinaDeVideoJuego maquina) {
        super(maquina);
    }

    @Override
    public void inicioPresionado() {
        this.maquina().cambiarElEstado(new Jugando(this.maquina()));
    }
}
