package videojuego;

public class ListoParaUnJugador extends EstadoDeLaMaquina {
    public ListoParaUnJugador(MaquinaDeVideoJuego maquina) {
        super(maquina);
    }

    @Override
    public void fichaIngresada() {
        this.maquina().cambiarElEstado(new ListoParaDosJugadores(this.maquina()));
    }

    @Override
    public void inicioPresionado() {
        this.maquina().cambiarElEstado(new Jugando(this.maquina()));
    }
}
