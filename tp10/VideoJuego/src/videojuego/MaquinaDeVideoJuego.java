package videojuego;

public class MaquinaDeVideoJuego {
    private EstadoDeLaMaquina estado;

    public MaquinaDeVideoJuego(EstadoDeLaMaquina estadoDeLaMaquina) {
        this.estado = estadoDeLaMaquina;
    }

    public void fichaIngresada() {
        this.estado.fichaIngresada();
    }

    public void inicioPresionado() {
       this.estado.inicioPresionado();
    }

    public void finDelJuego() {
        this.estado.finDelJuego();
    }

    public void cambiarElEstado(EstadoDeLaMaquina nuevoEstado) {
        this.estado = nuevoEstado;
    }
}
