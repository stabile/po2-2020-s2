package videojuego;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ListoParaUnJugadorTest {
    MaquinaDeVideoJuego maquina;
    EstadoDeLaMaquina listoParaUno;

    @BeforeEach
    void setUp() {
        maquina = mock(MaquinaDeVideoJuego.class);
        listoParaUno = new ListoParaUnJugador(maquina);
    }

    @DisplayName("Existe ListoPorUno")
    @Test
    void testHayListoParaUno() {
        assertNotNull(maquina);
    }

    @DisplayName("Cuando se ingresa una ficha se cambia al ListoParaDos")
    @Test
    void testFichaIngresada() {
        listoParaUno.fichaIngresada();
        verify(maquina).cambiarElEstado(isA(ListoParaDosJugadores.class));
    }

    @DisplayName("Cuando se presiona el botón de inicio se cambia al estado Jugando")
    @Test
    void testBotonDeInicio() {
        listoParaUno.inicioPresionado();
        verify(maquina).cambiarElEstado(isA(Jugando.class));
    }
}
