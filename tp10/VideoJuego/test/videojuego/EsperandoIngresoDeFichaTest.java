package videojuego;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class EsperandoIngresoDeFichaTest {
    MaquinaDeVideoJuego maquina;
    EstadoDeLaMaquina ingreseFicha;
    final ByteArrayOutputStream mensajeEnPantalla = new ByteArrayOutputStream();
    final PrintStream originalOut =  System.out;

    @BeforeEach
    void setUp() {
        maquina = mock(MaquinaDeVideoJuego.class);
        ingreseFicha = new EsperandoIngresoDeFicha(maquina);
        System.setOut(new PrintStream(mensajeEnPantalla));
    }

    @AfterEach
    void tearDown() {
        System.setOut(originalOut);
    }

    @DisplayName("Cuando se presiona el botón de inicio se pide que se ingrese una ficha")
    @Test
    void testBotonInicio() {
        ingreseFicha.inicioPresionado();
        assertEquals("Ingrese una ficha",mensajeEnPantalla.toString());
    }

    @DisplayName("Existe IngreseFicha")
    @Test
    void hayIngreseFicha() {
        assertNotNull(ingreseFicha);
    }

    @DisplayName("Cuando se ingresa una ficha se pasa al estado ListoParaUno")
    @Test
    void testFichaIngresada() {
        ingreseFicha.fichaIngresada();
        verify(maquina).cambiarElEstado(isA(ListoParaUnJugador.class));
    }
}