package videojuego;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class JugandoTest {
    MaquinaDeVideoJuego maquina;
    EstadoDeLaMaquina jugando;

    @BeforeEach
    void setUp() {
        maquina = mock(MaquinaDeVideoJuego.class);
        jugando = new Jugando(maquina);
    }

    @DisplayName("Existe Jugando")
    @Test
    void hayJugando() {
        assertNotNull(maquina);
    }

    @DisplayName("Cuando termina el juego se pasa a IngreseFicha")
    @Test
    void testFinDelJuego() {
        jugando.finDelJuego();
        verify(maquina).cambiarElEstado(isA(EsperandoIngresoDeFicha.class));
    }
}
