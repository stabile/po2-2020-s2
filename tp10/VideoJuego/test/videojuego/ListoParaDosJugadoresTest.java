package videojuego;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ListoParaDosJugadoresTest {
    MaquinaDeVideoJuego maquina;
    EstadoDeLaMaquina listoParaDos;

    @BeforeEach
    void setUp() {
        maquina = mock(MaquinaDeVideoJuego.class);
        listoParaDos = new ListoParaDosJugadores(maquina);
    }

    @DisplayName("Existe ListoParaDos")
    @Test
    void testHayListoParaDos() {
        assertNotNull(listoParaDos);
    }

    @DisplayName("Cuando se presiona el botón de inicio se pasa a Jugando")
    @Test
    void testInicioPresionado() {
        listoParaDos.inicioPresionado();
        verify(maquina).cambiarElEstado(isA(Jugando.class));
    }
}
