package videojuego;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class MaquinaDeVideoJuegoTest {
    MaquinaDeVideoJuego maquina;
    EstadoDeLaMaquina estadoDeLaMaquina;
    @BeforeEach
    void setUp() {
        estadoDeLaMaquina = mock(EstadoDeLaMaquina.class);
        maquina = new MaquinaDeVideoJuego(estadoDeLaMaquina);
    }


    @DisplayName("Cuando se ingresa una ficha se delega el msg en el estado")
    @Test
    void testIngresoDeFicha() {
        maquina.fichaIngresada();
        verify(estadoDeLaMaquina).fichaIngresada();
    }

    @DisplayName("Existe Maquina")
    @Test
    void testMaquina() {
        assertNotNull(maquina);
    }

    @DisplayName("Cuando se pide cambiarElEstado este cambia")
    @Test
    void testCambioDeEstado() {
        EstadoDeLaMaquina nuevoEstado = mock(EstadoDeLaMaquina.class);
        maquina.cambiarElEstado(nuevoEstado);
        maquina.finDelJuego(); // verifico que se delega este msg en el nuevo estado
        verify(nuevoEstado).finDelJuego();
    }

    @DisplayName("Cuando termina el juego se delega el msg en el estado")
    @Test
    void testFinDelJuego() {
        maquina.finDelJuego();
        verify(estadoDeLaMaquina).finDelJuego();
    }

    @DisplayName("Cuando se presiona el boton de inicio se delega el msg en el estado")
    @Test
    void testBotonDeInicio() {
        maquina.inicioPresionado();
        verify(estadoDeLaMaquina).inicioPresionado();
    }
}
