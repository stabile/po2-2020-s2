package encriptacion;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;


public class EncriptadorNaiveTest {
    EncriptadorNaive encriptadorNaive;
    FormaDeEncriptacion orden;
    FormaDeEncriptacion proximaVocal;
    FormaDeEncriptacion numeroEquivalente;
    List<FormaDeEncriptacion> formas;

    @BeforeEach
    void setUp() {
        orden = new Orden();
        proximaVocal = new ProximaVocal();
        numeroEquivalente = new NumeroEquivalente();

        encriptadorNaive = new EncriptadorNaive(orden);

        formas = Arrays.asList(orden, numeroEquivalente, proximaVocal);
    }

    @DisplayName("")
    @Test
    void testHayEncriptadorNaive() {
        assertNotNull(encriptadorNaive);
    }

    @DisplayName("Encripta la cadena '' a ''")
    @Test
    void testEncriptarVacia() {
        for(FormaDeEncriptacion fe : formas) {
            encriptadorNaive = new EncriptadorNaive(fe);
            assertEquals("", encriptadorNaive.encriptar(""));
        }
    }

    @DisplayName("Desencripta la cadena '' a ''")
    @Test
    void testDesencriptarVacia() {
        for(FormaDeEncriptacion fe : formas) {
            encriptadorNaive = new EncriptadorNaive(fe);
            assertEquals("", encriptadorNaive.desencriptar(""));
        }
    }

    @DisplayName("Cuando encripto 'a b c' con Orden obtengo 'c b a'")
    @Test
    void testEncConOrden() {
        encriptadorNaive.setFormaDeEncriptacion(orden);
        assertEquals("c b a", encriptadorNaive.encriptar("a b c"));
    }

    @DisplayName("Cuando desenripto 'c b a' con Orden obtengo 'a b c'")
    @Test
    void testDedEncConOrden() {
        encriptadorNaive.setFormaDeEncriptacion(orden);
        assertEquals("1 2 3", encriptadorNaive.desencriptar("3 2 1"));
    }

    @DisplayName("Cuando encripto 'aun encripto' con Orden obtengo 'ean incroptu'")
    @Test
    void testEncProximaVocal() {
        encriptadorNaive.setFormaDeEncriptacion(proximaVocal);
        assertEquals("ean incroptu", encriptadorNaive.encriptar("aun encripto"));
    }

    @DisplayName("Cuando desencripto 'aun encripto' con Orden obtengo 'uon ancrepti'")
    @Test
    void testDesEncProximaVocal() {
        encriptadorNaive.setFormaDeEncriptacion(proximaVocal);
        assertEquals("uon ancrepti", encriptadorNaive.desencriptar("aun encripto"));
    }

    @DisplayName("Cuando encripto 'Diego' con numeroEquivalente obtengo '4,9,5,7,15'")
    @Test
    void testNumeroEquivalente() {
        encriptadorNaive.setFormaDeEncriptacion(numeroEquivalente);
        assertEquals("4,9,5,7,15", encriptadorNaive.encriptar("Diego"));
    }

    @DisplayName("Cuando desencripto '4,9,5,7,15'm obtengo 'diego'")
    @Test
    void testDesEncNumeroEquivalente() {
        encriptadorNaive.setFormaDeEncriptacion(numeroEquivalente);
        assertEquals("diego", encriptadorNaive.desencriptar("4,9,5,7,15"));
    }
}
