package encriptacion;

public class EncriptadorNaive {

    private FormaDeEncriptacion formaDeEncriptacion;

    public EncriptadorNaive(FormaDeEncriptacion formaDeEncriptacion) {
        this.setFormaDeEncriptacion(formaDeEncriptacion);
    }

    public String encriptar(String s) {
        return this.formaDeEncriptacion.encriptar(s);
    }

    public String desencriptar(String s) {
        return this.formaDeEncriptacion.desencriptar(s);
    }

    public void setFormaDeEncriptacion(FormaDeEncriptacion formaDeEncriptacion) {
        this.formaDeEncriptacion = formaDeEncriptacion;
    }
}
