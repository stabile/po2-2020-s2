package encriptacion;

import java.util.Arrays;
import java.util.regex.Pattern;

public class Orden implements FormaDeEncriptacion {
    @Override
    public String encriptar(String s){
        return !s.contains(" ") ? s : reverse(s);
    }

    private String reverse(String s) {
        String[] work = Pattern.compile("\\s").split(s);
        String reverse = "";

        for (String st : Arrays.copyOfRange(work, 1, work.length))
            reverse = st + " " + reverse;

        return reverse + work[0];
    }

    @Override
    public String desencriptar(String s) {
        return encriptar(s);
    }
}
