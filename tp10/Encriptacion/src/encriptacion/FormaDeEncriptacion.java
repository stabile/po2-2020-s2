package encriptacion;

public interface FormaDeEncriptacion {
    String encriptar(String s);

    String desencriptar(String s);
}
