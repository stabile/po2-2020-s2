package encriptacion;

import java.util.regex.Pattern;

public class NumeroEquivalente implements FormaDeEncriptacion {
    @Override
    public String encriptar(String s) {
        String r = s.equals("") ? "" : numeroPara(s.charAt(0));

        for(int i = 1; i < s.length() ; i++)
            r += "," + numeroPara(s.charAt(i));

        return r;
    }


    @Override
    public String desencriptar(String s) {
        String[] numeros  = Pattern.compile(",").split(s);
        String retorno = "";

        for(String n : numeros)
            retorno += caracterPara(n);

        return s.equals("") ? "" : retorno;
    }

    Character caracterPara(String n){
        return switch (n) {
            case "0" -> ' ';
            case "1" -> 'a';
            case "2" -> 'b';
            case "3" -> 'c';
            case "4" -> 'd';
            case "5" -> 'e';
            case "6" -> 'f';
            case "7" -> 'g';
            case "8" -> 'h';
            case "9" -> 'i';
            case "10" -> 'j';
            case "11" -> 'k';
            case "12" -> 'l';
            case "13" -> 'm';
            case "14" -> 'n';
            case "15" -> 'o';
            case "16" -> 'p';
            case "17" -> 'q';
            case "18" -> 'r';
            case "19" -> 's';
            case "20" -> 't';
            case "21" -> 'u';
            case "22" -> 'v';
            case "23" -> 'w';
            case "24" -> 'x';
            case "25" -> 'y';
            case "26" -> 'z';
            default -> ' ';
        };
    }

    String numeroPara(Character c){
        return switch (c) {
            case ' ' -> "0";
            case 'a', 'A' -> "1";
            case 'b', 'B' -> "2";
            case 'c', 'C' -> "3";
            case 'd', 'D' -> "4";
            case 'e', 'E' -> "5";
            case 'f', 'F' -> "6";
            case 'g', 'G' -> "7";
            case 'h', 'H' -> "8";
            case 'i', 'I' -> "9";
            case 'j', 'J' -> "10";
            case 'k', 'K' -> "11";
            case 'l', 'L' -> "12";
            case 'm', 'M' -> "13";
            case 'n', 'N' -> "14";
            case 'o', 'O' -> "15";
            case 'p', 'P' -> "16";
            case 'q', 'Q' -> "17";
            case 'r', 'R' -> "18";
            case 's', 'S' -> "19";
            case 't', 'T' -> "20";
            case 'u', 'U' -> "21";
            case 'v', 'V' -> "22";
            case 'w', 'W' -> "23";
            case 'x', 'X' -> "24";
            case 'y', 'Y' -> "25";
            case 'z', 'Z' -> "26";
            default -> "";
        };
    }
}
