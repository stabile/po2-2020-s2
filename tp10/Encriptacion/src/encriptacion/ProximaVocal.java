package encriptacion;

public class ProximaVocal implements FormaDeEncriptacion {
    @Override
    public String encriptar(String s) {
        String r = "";
        for(int i = 0; i < s.length() ; i++)
            r += proximaVocal(s.charAt(i));

        return r;
    }

    @Override
    public String desencriptar(String s) {
        String r = "";
        for(int i = 0; i < s.length() ; i++)
            r += anteriorVocal(s.charAt(i));

        return r;
    }

    Character proximaVocal(Character c) {
        return switch (c) {
            case 'a' -> 'e';
            case 'e' -> 'i';
            case 'i' -> 'o';
            case 'o' -> 'u';
            case 'u' -> 'a';
            default -> c;
        };
    }

    Character anteriorVocal(Character c) {
        return switch (c) {
            case 'e' -> 'a';
            case 'i' -> 'e';
            case 'o' -> 'i';
            case 'u' -> 'o';
            case 'a' -> 'u';
            default -> c;
        };
    }

}
