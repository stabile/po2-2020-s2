package reproductor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ReproductorTest {

    Reproductor reproductor;
    EstadoDelReproductor seleccion;
    EstadoDelReproductor pausa;
    EstadoDelReproductor reproduccion;
    Song song;

    @BeforeEach
    void setUp() {
        seleccion    = new Seleccion();
        pausa        =  new Pausa();
        reproduccion = new Reproduccion();
        song = mock(Song.class);
        reproductor  = new Reproductor(seleccion, song);
    }

    private void verificarEstadoDelReproductor(Object clase) {
        assertEquals(clase.getClass(), reproductor.getEstado().getClass());
    }

    @DisplayName("Cambia el estado del reproductor")
    @Test
    void testCambiarEstado() {
        EstadoDelReproductor[] estados = {seleccion, pausa, reproduccion};
        for(var estado : estados) {
            reproductor.cambiarEstado(estado);
            assertEquals(estado, reproductor.getEstado());
        }
    }
    
    /*
    Play: Reproduce la canción seleccionada, si es que el reproductor esta en modo de selección de canciones. Caso
    contrario produce un error.
     */

    @DisplayName("Cuando en modo selección, play reproduce la canción" )
    @Test
    void testPlayEnSeleccion() throws AccionIncorrectaException{
        reproductor.cambiarEstado(seleccion);

        reproductor.play();

        verificarEstadoDelReproductor(reproduccion);
        verify(song).play();
    }

    @DisplayName("Cuando en modo pausa o reproduccion, play genera error ")
    @Test
    void testPlayEnPausaOReproduccion() {
        EstadoDelReproductor[] estados = {pausa, reproduccion};
        for(var estado : estados)
            assertThrows(AccionIncorrectaException.class,
                         () -> { reproductor.cambiarEstado(estado);
                                 reproductor.play();
                               }) ;
    }

    @DisplayName("Cuando en pausada retoma la reproducción")
    @Test
    void testPauseEnPausa() throws AccionIncorrectaException {
        reproductor.cambiarEstado(pausa);

        reproductor.pause();
        
        verificarEstadoDelReproductor(reproduccion);
        verify(song).play();
    }

    @DisplayName("Cuando en selección, pausa genera error")
    @Test
    void testPausaGeneraErrorEnPausa() {
        reproductor.cambiarEstado(seleccion);
        assertThrows(AccionIncorrectaException.class,
                     () -> reproductor.pause());
    }

    /*
            Pause: Pausa la canción que se está reproduciendo.
                   Si la canción está en pausada retoma la reproducción,
                   caso contrario genera un error.
            */
    @DisplayName("Cuando en reproducción, pausa pone la canción en pausa")
    @Test
    void testPausaEnReproduccion() throws AccionIncorrectaException {
        reproductor.cambiarEstado(reproduccion);

        reproductor.pause();

        verificarEstadoDelReproductor(pausa);
        verify(song).pause();
    }

    /*
    Stop: Para la reproducción o la pausa, y pasa a selección de canciones.
          Caso contrario no hace nada.
     */

    @DisplayName("Cuando en seleccion, stop no hace nada")
    @Test
    void testStopOnSeleccion() {
        reproductor.cambiarEstado(seleccion);

        reproductor.stop();

        verificarEstadoDelReproductor(seleccion);
        verifyZeroInteractions(song);
    }

    @DisplayName("Cuando en reproduccion o pausado, stop pasa a selección")
    @Test
    void testStopEnPausaORepr() {
        EstadoDelReproductor[] estados = {reproduccion, pausa};
        for (var estado : estados) {
            reproductor.cambiarEstado(estado);

            reproductor.stop();

            verificarEstadoDelReproductor(seleccion);
        }
        verify(song, times(estados.length)).stop(); // once for each estado

    }
}
