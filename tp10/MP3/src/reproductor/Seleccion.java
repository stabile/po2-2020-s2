package reproductor;

public class Seleccion extends EstadoDelReproductor {
    @Override
    public void play(Song song, Reproductor reproductor) {
        song.play();
        reproductor.cambiarEstado(new Reproduccion());
    }

    @Override
    public void stop(Song song, Reproductor reproductor) {
       // hace nada
    }
}
