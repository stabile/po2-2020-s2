package reproductor;

public class Reproductor {
    private EstadoDelReproductor estado;
    private Song song;

    public Reproductor(EstadoDelReproductor estado, Song song) {
        this.song = song;
        cambiarEstado(estado);
    }

    public void cambiarEstado(EstadoDelReproductor estado) {
        this.estado = estado;
    }

    public EstadoDelReproductor getEstado() {
        return this.estado;
    }

    public void play() throws AccionIncorrectaException {
        estado.play(song, this);
    }

    public void pause() throws AccionIncorrectaException {
        estado.pause(song, this);
    }

    public void stop() {
        estado.stop(song, this);
    }
}
