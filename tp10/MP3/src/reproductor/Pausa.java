package reproductor;

public class Pausa extends EstadoDelReproductor {
    @Override
    public void pause(Song song, Reproductor reproductor) {
       song.play();
       reproductor.cambiarEstado(new Reproduccion());
    }
}
