package reproductor;

public class EstadoDelReproductor {
    public void play(Song song, Reproductor reproductor) throws AccionIncorrectaException {
        throw new AccionIncorrectaException();
    }

    public void pause(Song song, Reproductor reproductor) throws AccionIncorrectaException {
        throw new AccionIncorrectaException();
    }


    public void stop(Song song, Reproductor reproductor) {
        song.stop();
        reproductor.cambiarEstado(new Seleccion());
    }
}
