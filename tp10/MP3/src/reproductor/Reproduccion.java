package reproductor;

public class Reproduccion extends EstadoDelReproductor {
    @Override
    public void pause(Song song, Reproductor reproductor) {
        song.pause();
        reproductor.cambiarEstado(new Pausa());
    }

}
