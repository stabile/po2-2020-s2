package supermercado;

import java.util.ArrayList;
import java.util.List;

public class Supermercado {

    private List<Producto> productos = new ArrayList<Producto>();

    public Supermercado(String nombre, String direccion) {
    }

    public int getCantidadDeProductos() { return this.productos.size(); }

    public void agregarProducto(Producto producto) { 
        this.productos.add(producto);
    }

    public Double getPrecioTotal() { return productos.stream().reduce(0d, (d, p) -> d + p.getPrecio(), Double::sum); }

}
