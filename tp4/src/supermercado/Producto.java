package supermercado;

import java.math.BigDecimal;

public class Producto {

    private String nombre;
    private BigDecimal precio;
    private boolean esPrecioCuidado;

    public Producto(String nombre, Double precio, boolean esPrecioCuidado) {
        super();
        setNombre(nombre);
        setPrecio(BigDecimal.valueOf(precio));
        setPrecioCuidado(esPrecioCuidado);
    }

    public Producto(String nombre, Double precio) {
        this(nombre, precio, false);
    }

    public String getNombre() { return this.nombre; }

    private void setNombre(String nombre) { this.nombre = nombre; }

    public Double getPrecio() { return this.precio.doubleValue(); }

    private void setPrecio(BigDecimal precio) { this.precio = precio; }

    private void setPrecioCuidado(boolean esPrecioCuidado) { this.esPrecioCuidado = esPrecioCuidado; }

    public boolean esPrecioCuidado() { return this.esPrecioCuidado; }

    public void aumentarPrecio(Double proporcion) { 
        setPrecio(this.precio.multiply(BigDecimal.valueOf(proporcion)));
    }

}
