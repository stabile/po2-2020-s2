package supermercado;

import java.math.BigDecimal;

public class ProductoPrimeraNecesidad extends Producto{

    private BigDecimal descuento;

    public ProductoPrimeraNecesidad(String producto, Double precio, boolean preciocuidado, Double descuento) {
        super(producto, precio, preciocuidado);
        setDescuento(BigDecimal.valueOf(descuento));
    }

    @Override
    public Double getPrecio(){ return precioConDescuento().doubleValue(); }

    private BigDecimal precioConDescuento() { return precioBigDecimal().multiply(factorDeDescuento()); }
    
    private BigDecimal precioBigDecimal() { return BigDecimal.valueOf(super.getPrecio()); }

    private BigDecimal factorDeDescuento() { return BigDecimal.valueOf(1).subtract(descuento); }
    
    private void setDescuento(BigDecimal descuento) { this.descuento = descuento; }
}
