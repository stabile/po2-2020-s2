package impuestoaltrabajador;

public class IngresoPorHorasExtras extends Ingreso {

    public IngresoPorHorasExtras(String mes, String concepto, double monto, int cantidad) {
        super(mes, concepto, monto);
    }

    @Override
    public double montoImponible() { return 0d; }
}
