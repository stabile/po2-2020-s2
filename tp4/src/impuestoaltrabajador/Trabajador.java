package impuestoaltrabajador;

import java.util.List;

public class Trabajador {

    List<Ingreso> ingresos;

    public Trabajador(List<Ingreso> ingresos) {
        super();
        this.ingresos = ingresos;
    }

    public double getTotalPercibido() { return ingresos.stream().reduce(0d, (zero, ingreso) -> ingreso.monto() + zero, Double::sum); }

    public double getMontoImponible() { return ingresos.stream().reduce(0d, (zero, ingreso) -> ingreso.montoImponible() + zero, Double::sum); }

    public double getImpuestoAPagar() { return getMontoImponible() * 0.02d ; }
}
