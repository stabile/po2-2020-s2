package impuestoaltrabajador;

public class Ingreso {

    double monto;

    public Ingreso(String mesDePercepcion, String concepto, double monto) {
        super();
        setMonto(monto);
    }

    public double monto() { return this.monto; }

    public double montoImponible() { return monto(); }

    private void setMonto(double monto) { this.monto = monto; }
}
