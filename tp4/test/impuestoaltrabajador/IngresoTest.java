package impuestoaltrabajador;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

public class IngresoTest {

    Ingreso ingreso1;

    @BeforeEach
    void setUp(){
        ingreso1 = new Ingreso("Diciembre", "Sueldo", 1731.0d);
    }

    @DisplayName("Constructor Ingreso")
    @Test
    void hayUnIngreso() {
        assertNotNull(ingreso1);
    }

    @DisplayName("Monto imponible")
    @Test
    void montoImponible() {
        assertEquals(1731.0d, ingreso1.montoImponible());
    }
}
