package impuestoaltrabajador;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import java.util.List;
import java.util.Arrays;

public class TrabajadorTest {
    Trabajador trabajador1 ; 

    @BeforeEach
    void setUp(){
        Ingreso ingreso1 = new IngresoPorHorasExtras("Enero", "Extras", 200.0d, 4);
        Ingreso ingreso2 = new Ingreso("Enero", "Sueldo", 1200.0d);
        Ingreso ingreso3 = new Ingreso("Enero", "Viatico", 100.0d);
        List<Ingreso> ingresos = Arrays.asList(ingreso1, ingreso2, ingreso3);
        trabajador1 = new Trabajador(ingresos);
    }

    @DisplayName("hay un Trabajador")
    @Test
    void trabajador() {
        assertNotNull(trabajador1);
    }

    @DisplayName("Total Percibido")
    @Test
    void totalPercibido() {
        assertEquals(1500.0d, trabajador1.getTotalPercibido());
    }

    @DisplayName("Monto imponible")
    @Test
    void montoImponible() {
        assertEquals(1300.0d, trabajador1.getMontoImponible());
    }

    @DisplayName("Impuesto a pagar")
    @Test
    void impuestoAPagar() {
        assertEquals(26.0d, trabajador1.getImpuestoAPagar());
    }
}
