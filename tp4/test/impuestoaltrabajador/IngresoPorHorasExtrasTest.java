package impuestoaltrabajador;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

public class IngresoPorHorasExtrasTest {

    Ingreso ingresoExtra1;

    @BeforeEach
    void setUp(){
        ingresoExtra1 = new IngresoPorHorasExtras("Junio", "Extras", 100.d, 2);
    }

    @DisplayName("Constructor Ingreso horas Extra")
    @Test
    void constructor() {
        assertNotNull(ingresoExtra1);
    }


    @DisplayName("Monto imponible")
    @Test
    void montoImponibleZero() {
        assertEquals(0d, ingresoExtra1.montoImponible());
    }
}
