package concurso;

import java.util.List;

public class Jugador implements IJugador {
    private IConcurso concurso;
    private List<String> preguntasAContestar;
    private EstadoDelJugador estado;
    private IJugador ultimoEnResponder;

    public Jugador(IConcurso concurso, List<String> preguntasAContestar) {
        this.concurso = concurso;
        this.preguntasAContestar = preguntasAContestar;
        this.cambioDeEstado(new WaitQ(this));
        concurso.sumar(this);
    }

    @Override
    public void preguntas(List<String> preguntas) {
        for(String pregunta : preguntas)
            this.preguntasAContestar.add(pregunta);
        this.estado.preguntas();
    }

    public void contestarPregunta() {
        concurso.acreditar("Respuesta", preguntasAContestar.get(0), this);
    }

    @Override
    public void preguntaOk(IJugador jugador, String pregunta) {
        ultimoEnResponder = jugador;
    }

    @Override
    public void respuestaCorrecta() {
        this.estado.respuestaCorrecta();
    }

    public void removerPregunta(){
        preguntasAContestar.remove(0);
    }

    public boolean hayMasPreguntasPorContestar() {
        return preguntasAContestar.size() > 0;
    }

    @Override
    public void respuestaIncorrecta() {
        this.estado.respuestaIncorrecta();
    }

    @Override
    public void finDePartida(IJugador jugador) {
        this.estado.finDePartida();
    }


    public void cambioDeEstado(EstadoDelJugador nuevoEstado) {
        this.estado = nuevoEstado;
    }

    public IJugador getUltimoEnResponder() {
        return ultimoEnResponder;
    }
}
