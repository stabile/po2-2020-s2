package concurso;

public abstract class EstadoDelJuego {

    public void seSumaJugador() {
        System.out.print("La inscripción esta cerrada");
    }

    public void seAcreditaPregunta() {
        System.out.print("El Juego no esta en curso");
    }

    public void preguntasEnviadas(Concurso concurso){
        concurso.cambiarDeEstado(new Preguntando());
    }

    public void finDePartidaEnviada(Concurso concurso){
        concurso.cambiarDeEstado(new Terminado());
    }

    public void inscribiendo(Concurso concurso) {
        concurso.cambiarDeEstado(new Inscribiendo());
    }
}
