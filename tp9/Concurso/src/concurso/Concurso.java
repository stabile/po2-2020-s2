package concurso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Concurso implements IConcurso {

    private final Map<String, String> preguntasRespuestas;
    private final List<String> preguntas;
    private final Map<IJugador, Integer> jugadores;
    private EstadoDelJuego estado;

    public Concurso(Map<String, String> preguntasRespuestas,
                    Map<IJugador, Integer> jugadores,
                    EstadoDelJuego estadoInicial) {
        this.preguntasRespuestas = preguntasRespuestas;
        this.preguntas = preguntasEn(preguntasRespuestas);
        this.jugadores = jugadores;
        this.cambiarDeEstado(estadoInicial);
        this.estado.inscribiendo(this);
    }

    public void cambiarDeEstado(EstadoDelJuego nuevoEstado) {
        this.estado = nuevoEstado;
    }

    private List<String> preguntasEn(Map<String, String> preguntasRespuestas) {
        List<String> preguntas = new ArrayList<>();
        for(Map.Entry<String,String> pregunta : preguntasRespuestas.entrySet())
            preguntas.add(pregunta.getKey());
        return preguntas;
    }

    @Override
    public void sumar(IJugador jugador) {
        this.estado.seSumaJugador();
        if (this.jugadores.size() < 5)
            this.jugadores.put(jugador, 0);
        if (this.jugadores.size() == 5)
            enviarPreguntas();
    }

    private void enviarPreguntas() {
        for(IJugador j : this.jugadores.keySet()) j.preguntas(this.preguntas);
        estado.preguntasEnviadas(this);
    }

    @Override
    public void acreditar(String respuesta, String pregunta, IJugador jugador) {
        estado.seAcreditaPregunta();
        if (esRespuestaCorrecta(respuesta, pregunta)) {
            for (IJugador j : jugadores.keySet()) j.preguntaOk(jugador, pregunta);
            jugador.respuestaCorrecta();
            acreditarRespuestaCorrecta(jugador);
        } else {
            jugador.respuestaIncorrecta();
        }
    }

    private void acreditarRespuestaCorrecta(IJugador jugador) {
        jugadores.put(jugador, jugadores.get(jugador) + 1);
        verificarGanador(jugador);
    }

    private void verificarGanador(IJugador jugador) {
        if (jugadores.get(jugador) == 5) {
            enviarFinDePartida(jugador);
        }
    }

    boolean esRespuestaCorrecta(String respuesta, String pregunta) {
        return preguntasRespuestas.containsValue(respuesta) &&
               preguntasRespuestas.containsKey(pregunta) &&
               preguntasRespuestas.get(pregunta).equals(respuesta);
    }

    private void enviarFinDePartida(IJugador jugador) {
        for (IJugador j : jugadores.keySet()) j.finDePartida(jugador);
        estado.finDePartidaEnviada(this);
    }
}
