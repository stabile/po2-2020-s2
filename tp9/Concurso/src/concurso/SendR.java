package concurso;

public class SendR extends EstadoDelJugador {
    public SendR(Jugador jugador) {
        super(jugador);
        jugador.contestarPregunta();
    }

    @Override
    public void respuestaCorrecta() {
        jugador.removerPregunta();
        if (jugador.hayMasPreguntasPorContestar())
            pasarAProximaPregunta();
        else
            finDelQuestionario();
    }

    private void pasarAProximaPregunta() {
        nuevoEstado(new SendR(jugador));
    }

    private void finDelQuestionario() {
        nuevoEstado(new GameOver(jugador));
    }

    @Override
    public void respuestaIncorrecta() {
        nuevoEstado(new SendR(jugador));
    }

    @Override
    public void finDePartida() {
        nuevoEstado(new GameOver(jugador));
    }
}
