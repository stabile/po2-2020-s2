package concurso;

public abstract class EstadoDelJugador {
    protected Jugador jugador;

    public EstadoDelJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public void preguntas() {}

    public void respuestaIncorrecta() {}

    public void respuestaCorrecta() {}

    public void finDePartida() {
        nuevoEstado(new GameOver(jugador));
    }

    protected void nuevoEstado(EstadoDelJugador nuevoEstado) {
        this.jugador.cambioDeEstado(nuevoEstado);
    }
}
