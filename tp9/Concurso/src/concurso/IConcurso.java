package concurso;

public interface IConcurso {
    void sumar(IJugador jugador);

    void acreditar(String respuesta, String pregunta, IJugador jugador);
}
