package concurso;

import java.util.List;

public interface IJugador {
    void preguntas(List<String> preguntas);

    void preguntaOk(IJugador jugador, String pregunta);

    void respuestaCorrecta();

    void respuestaIncorrecta();

    void finDePartida(IJugador jugador);

}
