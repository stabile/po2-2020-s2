package concurso;

public class WaitQ extends EstadoDelJugador {
    public WaitQ(Jugador jugador) {
        super(jugador);
    }

    public void preguntas() {
        this.nuevoEstado(new SendR(jugador));  }
}
