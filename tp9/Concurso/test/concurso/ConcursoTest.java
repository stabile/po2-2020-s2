package concurso;


import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ConcursoTest {
    Map<IJugador, Integer> jugadores;
    Map<String, String> preguntas;
    IConcurso concurso;
    EstadoDelJuego estadoDelJuego;
    IJugador jugador1;
    IJugador jugador2;
    IJugador jugador3;
    IJugador jugador4;
    IJugador jugador5;
    IJugador jugador6;
    List<IJugador> seisJugadores;
    List<IJugador> cincoJugadores;

    @Nested
    class Concurso_Test{
        @BeforeEach
        void setUp() {
            jugadores = new HashMap<>();
            preguntas = Map.of(
                    "Pregunta 1", "Respuesta 1",
                    "Pregunta 2", "Respuesta 2",
                    "Pregunta 3", "Respuesta 3",
                    "Pregunta 4", "Respuesta 4",
                    "Pregunta 5", "Respuesta 5"
            );
            estadoDelJuego = mock(EstadoDelJuego.class);
            concurso = new Concurso(preguntas, jugadores, estadoDelJuego);
            jugador1 = mock(IJugador.class);
            jugador2 = mock(IJugador.class);
            jugador3 = mock(IJugador.class);
            jugador4 = mock(IJugador.class);
            jugador5 = mock(IJugador.class);
            jugador6 = mock(IJugador.class);
            seisJugadores = Arrays.asList(jugador1, jugador2, jugador3, jugador4, jugador5, jugador6);
            cincoJugadores = Arrays.asList(jugador1, jugador2, jugador3, jugador4, jugador5);
        }

        private void sumarCincoJugadores() {
            for (IJugador jugador : cincoJugadores) concurso.sumar(jugador);
        }

        private void assertRespuestasCorrectas(IJugador jugador, Integer x) {
            assertEquals(x, jugadores.get(jugador));
        }

        @DisplayName("Hay Concurso")
        @Test
        void hayJuego() {
            assertNotNull(concurso);
            verify(estadoDelJuego).inscribiendo((Concurso) concurso);
        }

        @DisplayName("Cuando un Jugador se suma , si no es el quinto, se sigue " +
                "esperando")
        @Test
        void testSumar() {
            assertEquals(0, jugadores.size());
            concurso.sumar(jugador1);
            assertEquals(1, jugadores.size());
            for (IJugador jugador : cincoJugadores) verifyZeroInteractions(jugador);
            verify(estadoDelJuego).seSumaJugador();
        }

        @DisplayName("Cuando el quinto jugador se suma se envian a todos las preguntas")
        @Test
        void testQuintoEnviarPreguntas() {
            assertEquals(0, jugadores.size());
            sumarCincoJugadores();
            assertEquals(5, jugadores.size());
            for (IJugador jugador : cincoJugadores)
                verify(jugador).preguntas(anyListOf(String.class));
            for (IJugador jugador : cincoJugadores)
                verifyNoMoreInteractions(jugador);
            verify(estadoDelJuego, times(5)).seSumaJugador();
            verify(estadoDelJuego).preguntasEnviadas((Concurso) concurso);
        }

        @DisplayName("Un jugador acredita una respuesta correcta, todos informados")
        @Test
        void testRespuestaCorrecta() {
            sumarCincoJugadores();
            assertRespuestasCorrectas(jugador3, 0);

            concurso.acreditar("Respuesta 1", "Pregunta 1", jugador3);

            assertRespuestasCorrectas(jugador3, 1);
            for (IJugador jugador : cincoJugadores)
                verify(jugador).preguntaOk(jugador3, "Pregunta 1");
            verify(jugador3).respuestaCorrecta();
            verify(estadoDelJuego).seAcreditaPregunta();
        }

        @DisplayName("Cuando un jugador acredita una respuesta incorrecta, se responde incorrecta")
        @Test
        void testRespuestaIncorrecta() {
            sumarCincoJugadores();
            assertRespuestasCorrectas(jugador4, 0);

            concurso.acreditar("Respuesta Incorrecta", "Pregunta 2", jugador4);

            assertRespuestasCorrectas(jugador4, 0);
            verify(jugador4).respuestaIncorrecta();
            for (IJugador jugador : cincoJugadores)
                verify(jugador).preguntas(anyListOf(String.class));
            for (IJugador jugador : cincoJugadores)
                verifyNoMoreInteractions(jugador);
            verify(estadoDelJuego).seAcreditaPregunta();
        }

        @DisplayName("Cuando un jugador contesta bien la ultima, gana")
        @Test
        void testJugadorGanaLaPartida() {
            sumarCincoJugadores();

            concurso.acreditar("Respuesta 1", "Pregunta 1", jugador3);
            concurso.acreditar("Respuesta 2", "Pregunta 2", jugador3);
            concurso.acreditar("Respuesta 3", "Pregunta 3", jugador3);
            concurso.acreditar("Respuesta 4", "Pregunta 4", jugador3);
            concurso.acreditar("Respuesta 5", "Pregunta 5", jugador3);

            for (IJugador jugador : cincoJugadores)
                verify(jugador).finDePartida(jugador3);
            verify(estadoDelJuego).finDePartidaEnviada((Concurso) concurso);
        }
    }
    
    @Nested
    class Estado_Juego_Test {
        final ByteArrayOutputStream mensajeEnConsola = new ByteArrayOutputStream();
        final PrintStream originalOut =  System.out;

        @BeforeEach
        void setUp() {
            jugadores = new HashMap<>();
            preguntas = Map.of(
                    "Pregunta 1", "Respuesta 1",
                    "Pregunta 2", "Respuesta 2",
                    "Pregunta 3", "Respuesta 3",
                    "Pregunta 4", "Respuesta 4",
                    "Pregunta 5", "Respuesta 5"
            );
            jugador1 = mock(IJugador.class);

            estadoDelJuego = new Inscribiendo();

            concurso = new Concurso(preguntas,
                    jugadores,
                    estadoDelJuego);
            System.setOut(new PrintStream(mensajeEnConsola));
        }

        @AfterEach
        void tearDown() {
            System.setOut(originalOut);
        }

        @DisplayName("En estado de inscripción se acredita una pregunta, error")
        @Test
        void testInscribiendo() {
            estadoDelJuego.inscribiendo((Concurso)concurso);
            concurso.acreditar("Respuesta", "Pregunta", jugador1);
            assertEquals("El Juego no esta en curso",
                    mensajeEnConsola.toString());
        }

        @DisplayName("Con el Juego terminado se acredita una respuesta, error")
        @Test
        void testJuegoTerminado() {
            estadoDelJuego.finDePartidaEnviada((Concurso)concurso);
            concurso.acreditar("Respuesta", "Pregunta", jugador1);
            assertEquals("El Juego no esta en curso",
                    mensajeEnConsola.toString());
        }

        @DisplayName("En medio del juego un jugador intenta sumarse, error")
        @Test
        void testLaInscripcionEstaCerrada() {
            estadoDelJuego.preguntasEnviadas((Concurso)concurso);
            concurso.sumar(jugador1);
            assertEquals("La inscripción esta cerrada",
                    mensajeEnConsola.toString());
        }

        @DisplayName("Con el juego terminado intentan sumarse, error")
        @Test
        void testLaPartidaTerminoEstaCerrada() {
            estadoDelJuego.finDePartidaEnviada((Concurso)concurso);
            concurso.sumar(jugador1);
            assertEquals("La inscripción esta cerrada",
                    mensajeEnConsola.toString());
        }

        @DisplayName("Jugando se acredita pregunta, correcto")
        @Test
        void testJugandoSeAcreditaPregunta() {
            estadoDelJuego.preguntasEnviadas((Concurso)concurso);
            concurso.acreditar("Respuesta", "Pregunta", jugador1);
            assertEquals("",
                    mensajeEnConsola.toString());
        }

        @DisplayName("Durante la inscripción se suma un jugador, correcto")
        @Test
        void testInscribiendoSeSuma() {
            estadoDelJuego.inscribiendo((Concurso)concurso);
            concurso.sumar(jugador1);
            assertEquals("",
                    mensajeEnConsola.toString());

        }
    }
}
