package concurso;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class JugadorTest {

    public class JugadorSpy extends Jugador {
        public int nCallsPreguntaOK = 0;
        public int nCallsRespuestaCorrecta = 0;
        public int nCallsRespuestaIncorrecta = 0;
        public int nCallsCambioDeEstado = 0;
        public EstadoDelJugador estadoActualSpy;
        public IJugador ultimoEnResponder;

        JugadorSpy(IConcurso concurso,
                   List<String> preguntasAContestar) {
            super(concurso, preguntasAContestar);
            estadoActualSpy = estadoDelJugador;
        }

        @Override
        public void cambioDeEstado(EstadoDelJugador nuevoEstado) {
           super.cambioDeEstado(nuevoEstado);
           nCallsCambioDeEstado+=1;
           this.estadoActualSpy = nuevoEstado;
        }

        @Override
        public void preguntaOk(IJugador j, String s){
            nCallsPreguntaOK++;
            ultimoEnResponder = j;
            super.preguntaOk(j, s);
        }
        @Override
        public void respuestaCorrecta() {
            nCallsRespuestaCorrecta++;
            super.respuestaCorrecta();
        }

        @Override
        public void respuestaIncorrecta() {
            nCallsRespuestaIncorrecta++;
            super.respuestaIncorrecta();
        }
    }

    IJugador jugador;
    IJugador otroJugador;
    List<String> preguntasAContestar;
    List<String> preguntasPropuestas;
    JugadorSpy jugadorSpy;
    EstadoDelJugador estadoDelJugador;

    Concurso concurso;

    @BeforeEach
    void setUp() {
        estadoDelJugador = mock(EstadoDelJugador.class);
        concurso = mock(Concurso.class);
        preguntasAContestar = new ArrayList<>();
        preguntasPropuestas = Arrays.asList(
                "Pregunta1",
                "Pregunta2",
                "Pregunta3",
                "Pregunta4",
                "Pregunta5"
        );
        jugador = new Jugador(concurso, preguntasAContestar);
        jugadorSpy = new JugadorSpy(concurso, preguntasAContestar);

    }

    @DisplayName("Existe Jugador")
    @Test
    void testHayJugador() {
        JugadorSpy jugadorSpy = new JugadorSpy(concurso, preguntasAContestar);
        assertNotNull(jugadorSpy);
        verify(concurso).sumar(jugadorSpy);
    }

    @DisplayName("Un jugador recibe las preguntas")
    @Test
    void testPreguntas() {
        assertEquals(0, preguntasAContestar.size());

        jugador.preguntas(preguntasPropuestas);

        assertEquals(5, preguntasAContestar.size());
        assertEquals("Pregunta1", preguntasAContestar.get(0));
        verify(concurso).acreditar(eq("Respuesta"),
                                   eq("Pregunta1"),
                                   eq(jugador));
    }

    @DisplayName("Alguien contesto ok la pregunta 3")
    @Test
    void testPreguntaOK() {
        jugador.preguntaOk(otroJugador, "Pregunta3");
        assertEquals(otroJugador, ((Jugador)jugador).getUltimoEnResponder());
    }

    @DisplayName("Respuesta Incorrecta")
    @Test
    void testRespuestaIncorrecta() {
        jugador.preguntas(preguntasPropuestas); // lanza la primer respuesta

        jugador.respuestaIncorrecta(); // la segunda

        verify(concurso, times(2)).acreditar(eq("Respuesta"), any(),
                eq(jugador));

    }

    @DisplayName("Respuesta Correcta")
    @Test
    void testRespuestaCorrecta() {
        jugador.preguntas(preguntasPropuestas); // lanza la primeryy respuesta

        jugador.respuestaCorrecta(); // lanza la segunda respuesta

        verify(concurso, times(2)).acreditar(eq("Respuesta"), any(),
                eq(jugador));
    }

    @DisplayName("Cinco respuestas Correctas")
    @Test
    void cincoRespuestasCorrectas() {
        jugador.preguntas(preguntasPropuestas);

        jugador.respuestaCorrecta();
        jugador.respuestaCorrecta();
        jugador.respuestaCorrecta();
        jugador.respuestaCorrecta();
        jugador.respuestaCorrecta();

        assertEquals(0, preguntasAContestar.size());
    }

    @DisplayName("Fin de partida")
    @Test
    void testFinDePartida() {
        ((Jugador)jugador).cambioDeEstado(estadoDelJugador);
        jugador.finDePartida(otroJugador);
        verify(estadoDelJugador).finDePartida();
    }
}
