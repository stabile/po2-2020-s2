package publicaciones;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PublicacionesTest {
    IPublicaciones publicaciones;
    Articulo articulo;
    ISuscriptor suscriptor1;
    ISuscriptor suscriptor2;
    List<ISuscriptor> suscriptorList = new ArrayList<>();
    List<Articulo> articuloList = new ArrayList<>();
    String descripcionArticulo = "Descripcion completa del articulo";

    @BeforeEach
    void setUp() {
        publicaciones = new Publicaciones(suscriptorList, articuloList);
        articulo = mock(Articulo.class);

        when(articulo.getDescripcionCompleta()).thenReturn(descripcionArticulo);
        suscriptor1 = mock(ISuscriptor.class);
        suscriptor2 = mock(ISuscriptor.class);
    }

    @DisplayName("Hay publicaciones")
    @Test
    void testHayPublicaciones() {
        assertNotNull(publicaciones);
    }

    @DisplayName("Cuando Registro un suscriptor este esta en suscriptores")
    @Test
    void testAgregarSuscriptor() {
        assertEquals(0, suscriptorList.size());
        publicaciones.registrar(suscriptor1);
        assertEquals(1, suscriptorList.size());
        assertTrue(suscriptorList.contains(suscriptor1));
    }

    @DisplayName("Cuando agrego un articulo a publicaciones se notifica a los suscriptores")
    @Test
    void testAgregarPublicacion() {
        publicaciones.registrar(suscriptor1);
        publicaciones.registrar(suscriptor2);
        publicaciones.agregarArticulo(articulo);
        verify(suscriptor1).update(descripcionArticulo);
        verify(suscriptor2).update(descripcionArticulo);
    }

}
