package publicaciones;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ArticuloTest {

    Articulo articulo;
    String descripcion;

    @BeforeEach
    void setUp() {
        descripcion = "Descripción (csv)";
        articulo = new Articulo(descripcion);

    }

    /**
     * En este sistema es posible cargar artículos científicos indicando el
     * titulo,
     * los autores,
     * las filiaciones de cada uno (Universidad, * Laboratorio de Investigación, etc),
     * el tipo de artículo,
     * el lugar donde fue publicado y
     * las palabras claves.
     */
    @DisplayName("Hay un articulo con ")
    @Test
    void testHayArticulo() {
        assertNotNull(articulo);
    }

    @DisplayName("Cuando pido la descripcion completa del articulo la obtengo")
    @Test
    void testGetDescripcion() {
        assertEquals(descripcion, articulo.getDescripcionCompleta());
    }
}
