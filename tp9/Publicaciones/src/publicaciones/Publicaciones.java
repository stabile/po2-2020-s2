package publicaciones;

import java.util.List;

public class Publicaciones implements IPublicaciones {
    private final List<ISuscriptor> suscriptores;
    private final List<Articulo> articulos;

    public Publicaciones(List<ISuscriptor> suscriptores, List<Articulo> articulos) {
        this.suscriptores = suscriptores;
        this.articulos = articulos;
    }

    @Override
    public void registrar(ISuscriptor s) {
        this.suscriptores.add(s);
    }

    @Override
    public void agregarArticulo(Articulo a) {
        this.articulos.add(a);
        this.notificar(a.getDescripcionCompleta());
    }

    private void notificar(String descripcion) {
        for(ISuscriptor suscriptor : suscriptores)
            suscriptor.update(descripcion);
    }
}
