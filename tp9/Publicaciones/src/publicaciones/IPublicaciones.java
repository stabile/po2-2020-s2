package publicaciones;

public interface IPublicaciones {
    void registrar(ISuscriptor s);
    void agregarArticulo(Articulo a);
}
