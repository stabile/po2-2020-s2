package publicaciones;

public class Articulo {

    private String descripcionCompleta;

    public Articulo(String descripcionCompleta) {
        this.descripcionCompleta = descripcionCompleta;
    }

    public String getDescripcionCompleta() {
        return this.descripcionCompleta;
    }
}
