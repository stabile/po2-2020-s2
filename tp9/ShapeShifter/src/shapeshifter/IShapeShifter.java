package shapeshifter;

import java.util.Collection;
import java.util.List;

public interface IShapeShifter {
    IShapeShifter compose(IShapeShifter ss);
    int deepest();
    IShapeShifter flat();
    List<Integer> values();
}
