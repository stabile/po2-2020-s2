package shapeshifter;

import java.util.List;

public interface IShapeShifterAux {
    String format();
    List<IShapeShifter> flat_();
}
