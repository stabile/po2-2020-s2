package shapeshifter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SSLeaf implements IShapeShifter, IShapeShifterAux {
    private final Integer valor;
    public SSLeaf(int valor) {
        this.valor = valor;
    }

    @Override
    public IShapeShifter compose(IShapeShifter ss) {
        return (new SSComposite()).add(this).add(ss);
    }

    @Override
    public int deepest() { return 0; }

    @Override
    public IShapeShifter flat() {
        System.out.print(".L. ");
        return this;
    }

    @Override
    public List<IShapeShifter> flat_() {
        return Collections.singletonList(this.flat());
    }

    @Override
    public List<Integer> values() {
        return Arrays.asList(this.valor);
    }

    @Override
    public String format() {
        return "("+this.valor+")" ;
    }

}
