package shapeshifter;

import java.util.ArrayList;
import java.util.List;

public class SSComposite implements IShapeShifter, IShapeShifterAux {

    List<IShapeShifter> components;

    SSComposite() {
        this.components = new ArrayList<>();
    }

    @Override
    public IShapeShifter compose(IShapeShifter ss) {
        return (new SSComposite()).add(this).add(ss);
    }

    protected SSComposite add(IShapeShifter shapeShifter) {
        this.components.add(shapeShifter);
        return this;
    }

    @Override
    public int deepest() {
        return 1 + maximoDeep(this.components);
    }

    private int maximoDeep(List<IShapeShifter> components) {
        return components.stream().mapToInt(c -> c.deepest()).max().orElse(0);
    }

    @Override
    public IShapeShifter flat() {
        return (new SSComposite()).addAll(this.flat_());
    }

    @Override
    public List<IShapeShifter> flat_() {
        List<IShapeShifter> flatten = new ArrayList<>();
        for(IShapeShifter comp : components){
            flatten.addAll(((IShapeShifterAux)comp).flat_());
        }
        return flatten;
    }

    private IShapeShifter addAll(List<IShapeShifter> sss) {
        this.components.addAll(sss);
        return this;
    }

    @Override
    public List<Integer> values() {
        List<Integer> values = new ArrayList<>();
        for(IShapeShifter ss : this.components){
            values.addAll(ss.values());
        }
        return values;
    }

    @Override
    public String format() {
        String out = "";
        out +="[";
        for(IShapeShifter component : this.components)
            out += ((IShapeShifterAux)component).format() + " ";

        return out + "]";
    }

}
