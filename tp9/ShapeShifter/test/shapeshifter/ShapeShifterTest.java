package shapeshifter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ShapeShifterTest {
    @DisplayName("values de un Leaf es la lista del entero con que se creo")
    @Test
    void testValuesLeaf() {
        IShapeShifter ssLeaf = new SSLeaf(1);
        assertEquals(Arrays.asList(1), ssLeaf.values());
    }
    /**
     * recibe un IShapeShifter y retorna un nuevo IShapeShifter que es el
     * resultado de componer al receptor con el que es enviado
     */
    /**
     * a.compose(b) es igual a c (done)
     * a = (1)
     * b = (2)
     * c = [(1) (2)]
     * d.compose(c) es igual a d
     * d = (3)
     * c = [(1) (2)]
     * d = [(3) [(1) (2)]]
     * d.compose(e) es igual a f
     * e = [(5) (6)]
     * f = [ [(3) [(1) (2)]]  [(5) (6)]]
     */
    @DisplayName("Un Composite.compose recibe un Composite y retorna un Composite")
    @Test
    void testCompose2Composite() {
        IShapeShifter a = new SSLeaf(1);
        IShapeShifter b = new SSLeaf(2);
        IShapeShifter c = a.compose(b);
        IShapeShifter d = new SSLeaf(3);
        IShapeShifter e1 = new SSLeaf(5);
        IShapeShifter e2 = new SSLeaf(6);
        IShapeShifter e = e1.compose(e2);

        d = d.compose(c);

        IShapeShifter f = d.compose(e);

        assertListEquals(Arrays.asList(1, 2, 3, 5, 6), f.values());
    }

    @DisplayName("Un Leaf.compose recibe un Composite y retorna un Composite")
    @Test
    void testCompose2Leaf2() {
        IShapeShifter a = new SSLeaf(1);
        IShapeShifter b = new SSLeaf(2);
        IShapeShifter c = a.compose(b);
        IShapeShifter d = new SSLeaf(3);

        d = d.compose(c);

        assertListEquals(Arrays.asList(1, 2, 3), d.values());
    }

    @DisplayName("Un Leaf.compose recibe un Leaf y retorna un Composite")
    @Test
    void testCompose2Leaf() {
        IShapeShifter ssLeaf1 = new SSLeaf(1);
        IShapeShifter ssLeaf2 = new SSLeaf(2);

        IShapeShifter ssResult = ssLeaf1.compose(ssLeaf2);

        assertListEquals(Arrays.asList(1, 2), ssResult.values());
    }

    private void assertListEquals(List<Integer> expected, List<Integer> values) {
        assertTrue("values deberia contener all expected " + values + " " + expected,
                values.containsAll(expected));
        assertTrue("expected deberia contener all v" + expected + " " + values,
                expected.containsAll(values));
        assertTrue("Deberian tener igual tamaño" + expected.size() + " == " + values.size(),
                expected.size() == values.size());
    }

    @Nested
    class DeepestTest {
        @DisplayName("Un Leaf tiene deepest cero")
        @Test
        void testDeepestLeaf() {
            IShapeShifter ssLeaf = new SSLeaf(42);
            assertEquals(0, ssLeaf.deepest());
        }

        @DisplayName("Un Composite con dos leaf tiene deepest uno")
        @Test
        void testDeepestComposite() {
            IShapeShifter a = new SSLeaf(1);
            IShapeShifter b = new SSLeaf(2);
            IShapeShifter c = a.compose(b);
            assertEquals(1, c.deepest());
        }

        @DisplayName("Un Composite con un Composite de uno y un Leaf tiene dos de deepest")
        @Test
        void testDeepestDos() {

            IShapeShifter a = new SSLeaf(1);
            IShapeShifter b = new SSLeaf(2);
            IShapeShifter c = a.compose(b);
            IShapeShifter d0 = new SSLeaf(3);
            IShapeShifter d = d0.compose(c);

            assertEquals(2, d.deepest());

        }

        @DisplayName("UnComposite tiene deepest 3")
        @Test
        void testDeepestTres() {
            IShapeShifter a = new SSLeaf(1); //0
            IShapeShifter b = new SSLeaf(2); //0
            IShapeShifter c = a.compose(b); // 1
            IShapeShifter d0 = new SSLeaf(3); // 0
            IShapeShifter e1 = new SSLeaf(5); // 0
            IShapeShifter e2 = new SSLeaf(6); // 0
            IShapeShifter e = e1.compose(e2); //1
            IShapeShifter d = d0.compose(c); // 2
            IShapeShifter f = d.compose(e);

//            ((IShapeShifterPrint)e).print();
            assertEquals(1, e.deepest());
            assertEquals(1, c.deepest());
            assertEquals(0, d0.deepest());
            assertEquals(2, d.deepest());
//            ((IShapeShifterPrint)f).print();
            assertEquals(3, f.deepest());
        }
    }
    
    @Nested
    class FlatTest {
        @DisplayName("Flat de Leaf es el Leaf")
        @Test
        void testFlatLeaf() {
            IShapeShifter a = new SSLeaf(1);
            assertEquals(a, a.flat());
            assertEquals("(1)" , ((IShapeShifterAux)a).format());
        }

        @DisplayName("Flat de Composite es un Composite deeper uno")
        @Test
        void testFlatCompositeUno() {
            IShapeShifter a = new SSLeaf(1); //0
            IShapeShifter b = new SSLeaf(2); //0
            IShapeShifter c = a.compose(b); // 1
            IShapeShifter cFlat = c.flat();

            assertEquals("[(1) (2) ]", ((IShapeShifterAux)cFlat).format());
        }

        @DisplayName("Flat de Composite deeper tres")
        @Test
        void testCompositeTres() {
            IShapeShifter a = new SSLeaf(1); //0
            IShapeShifter b = new SSLeaf(2); //0
            IShapeShifter c = a.compose(b); // 1
            IShapeShifter d0 = new SSLeaf(3); // 0
            IShapeShifter e1 = new SSLeaf(5); // 0
            IShapeShifter e2 = new SSLeaf(6); // 0
            IShapeShifter e = e1.compose(e2); //1
            IShapeShifter d = d0.compose(c); // 2
            IShapeShifter f = d.compose(e);

            IShapeShifter g = f.flat(); // Excercise

            assertEquals("[(3) (1) (2) (5) (6) ]", ((IShapeShifterAux)g).format());

        }
    }



}

