package cultivo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CultivoTest {
    Soja soja;
    Trigo trigo;
    Mixta mixta;
    Region region;

    @BeforeEach
    void setUp(){
        soja = new Soja();
        trigo = new Trigo();
        mixta = new Mixta(trigo, trigo, soja, soja);
//        region = new Region();
    }


    @Nested
    class SojaTest{
        @DisplayName("Hay Soja")
        @Test
        void haySoja() {
            assertEquals(Soja.class, soja.getClass());
            assertTrue(soja instanceof Region);
            assertTrue(soja instanceof Soja);
        }

        @DisplayName("Region de Soja rinde 300")
        @Test
        void testRindeSoja() {
            assertEquals(500.0, soja.ganancia(), 0.001);
        }
    }

    @Nested
    class TrigoTest {
        @DisplayName("Hay Trigo")
        @Test
        void hayTrigo() {
            assertEquals(Trigo.class, trigo.getClass() );
            assertTrue(trigo instanceof Region);
            assertTrue(trigo instanceof Trigo);
        }

        @DisplayName("Region de Trigo rinde 300")
        @Test
        void testRindeTrigo() {
            assertEquals(300.0, trigo.ganancia(), 0.001);
        }
    }

    @Nested
    class MixtaTest{
        @DisplayName("Hay Mixta (Composite)")
        @Test
        void hayMixta() {
            assertEquals(Mixta.class, mixta.getClass());
            assertTrue(mixta instanceof Region);
            assertTrue(mixta instanceof Mixta);
        }

        @DisplayName("Mixta tiene una coleccion de parcelas")
        @Test
        void hayColeccionDeParcelasDefault() {
            List<Region> regions = new ArrayList<>();
            assertEquals(4 , mixta.parcelas().size());
        }

        @DisplayName("Mixta tiene una ganancia sobre una hectarea")
        @Test
        void testGananciaTotalDefault() {
            assertEquals(400.0d, mixta.gananciaTotal(1),
                    0.001);
        }

        @DisplayName("Mixta tiene una ganancia 500: igual a la suma de sus cuatro regiones")
        @Test
        void testGananciaTotal() {
            Region soja = new Soja();
            Region mixta = new Mixta(soja, soja, soja, soja);
            assertEquals(500.0d, mixta.gananciaTotal(1),
                          0.001);
        }

    }

}
