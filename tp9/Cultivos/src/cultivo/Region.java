package cultivo;

/**
 * La interface Region cumple el rol de Component.
 * No hay add ni remove.
 * Mixta debe mantener una lista de cuatro Components.
 * El constructor de Mixta oficia de add.
 */
interface Region {
    default double gananciaTotal(double hectareaParaEstaRegion) {
        return this.ganancia() * hectareaParaEstaRegion;
    }

    double ganancia();
}
