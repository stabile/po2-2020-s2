package cultivo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Mixta cumple el rol de Composite
 */
public class Mixta implements Region{
    private List<Region> regiones;

    public Mixta(Region region1, Region region2, Region region3, Region region4) {
        this.regiones = Arrays.asList(region1, region2, region3, region4);
    }

    public List<Region> parcelas() {
        return this.regiones;
    }

    public double gananciaTotal(double hectareaParaEstaRegion) {
        return regiones.stream()
                .mapToDouble(region -> region.gananciaTotal(hectareaParaEstaRegion / 4.0))
                .sum();
    }

    @Override
    public double ganancia() {
        return 0;
    }
}
