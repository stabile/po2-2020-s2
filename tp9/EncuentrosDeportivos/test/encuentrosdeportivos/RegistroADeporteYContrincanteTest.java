package encuentrosdeportivos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class RegistroADeporteYContrincanteTest {
    RegistroAEncuentrosDeportivos registroADeporteYContrincante;
    ISuscriptor suscriptor;
    List<String> contrincantesConContrincanteRegistrado;
    List<String> contrincantesSinContrincanteRegistrado;

    @BeforeEach
    void setUp() {
        suscriptor = mock(ISuscriptor.class);
        registroADeporteYContrincante = new RegistroADeporteYContrincante("Deporte", "Contrincante", suscriptor);
        contrincantesConContrincanteRegistrado = Arrays.asList("Otro", "Contrincante", "Otro");
        contrincantesSinContrincanteRegistrado = Arrays.asList("Otro", "Otro", "Otro");
    }

    @DisplayName("Hay Registro a deporte y contrincante")
    @Test
    void testHAyRegistroADeporteYContrincante() {
        assertNotNull(registroADeporteYContrincante);
    }

    @DisplayName("Cuando notificar y es del deporte y el contrincante de actualiza al suscriptor")
    @Test
    void testCoincideDeporteYContrincante() {
        registroADeporteYContrincante.notificar("Deporte",
                contrincantesConContrincanteRegistrado,
                "Resultado");
        verify(suscriptor).update("Deporte", contrincantesConContrincanteRegistrado, "Resultado");
    }

    @DisplayName("Cuando notificar y el deporte no es el registrado No se actualiza al suscriptor")
    @Test
    void testElDeporteNoEstaRegistrado() {
        registroADeporteYContrincante.notificar("DeporteNoRegistrado",
                contrincantesConContrincanteRegistrado,
                "Resultado");
        verify(suscriptor, never()).update(any(), any(), any());
    }

    @DisplayName("Cuando notificar y el contrincante registrado no esta incluido NO se actualiza al suscriptor")
    @Test
    void testElContrincanteNoEstaRegistrado() {
        registroADeporteYContrincante.notificar("Deporte",
                contrincantesSinContrincanteRegistrado,
                "Resultado");
        verify(suscriptor, never()).update(any(), any(), any());
    }
}