package encuentrosdeportivos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class RegistroAContrincanteTest {

    RegistroAEncuentrosDeportivos registroAContrincante;
    ISuscriptor suscriptor;
    List<String> contrincantesConContrincanteRegistrado;
    List<String> contrincantesSinContrincanteRegistrado;
    @BeforeEach
    void setUp() {
        suscriptor = mock(ISuscriptor.class);
        registroAContrincante = new RegistroAContrincante("Contrincante", suscriptor);
        contrincantesConContrincanteRegistrado = Arrays.asList("Otro", "Contrincante", "Otro");
        contrincantesSinContrincanteRegistrado = Arrays.asList("Otro", "Aquel", "Otro");
    }

    @DisplayName("Existe un registro a contrincante")
    @Test
    void hayRegistro() {
        assertNotNull(registroAContrincante);
    }

    @DisplayName("Cuando notificar y es el contrincante, actualiza al suscritor")
    @Test
    void testNotificar() {
        registroAContrincante.notificar("Deporte",
                contrincantesConContrincanteRegistrado,
                "Resultado");
        verify(suscriptor).update("Deporte",
                contrincantesConContrincanteRegistrado,
                "Resultado" );
    }

    @DisplayName("Cuando notificar y NO se incluye al contrincante NO se actualiza al suscriptor")
    @Test
    void testNoNotificar() {
        registroAContrincante.notificar("Deporte",
                contrincantesSinContrincanteRegistrado,
                "Resultado");
        verify(suscriptor, never()).update(any(), any(), any());

    }
}
