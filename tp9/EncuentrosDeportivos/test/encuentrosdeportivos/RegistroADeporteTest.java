package encuentrosdeportivos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class RegistroADeporteTest {
    RegistroAEncuentrosDeportivos registroADeporte;
    ISuscriptor suscriptor;
    List<String> contrincantes;

    @BeforeEach
    void setUp() {
        suscriptor = mock(ISuscriptor.class);
        registroADeporte = new RegistroADeporte("Futbol", suscriptor);
        contrincantes = Arrays.asList("Contendiente1", "Contendiente2");
    }

    @DisplayName("Existe un registro a deporte")
    @Test
    void hayRegistroADeporte() {
        assertNotNull(registroADeporte);
    }

    @DisplayName("Cuando notificar y es para el deporte actualiza al suscriptor")
    @Test
    void testNotificacion() {
        registroADeporte.notificar("Futbol", contrincantes, "Empate" );
        verify(suscriptor).update("Futbol", contrincantes, "Empate");
    }

    @DisplayName("Cuando notificar y no es el deporte No actualiza el suscriptor")
    @Test
    void testNoHayNotificacion() {
        registroADeporte.notificar("Basket", contrincantes, "Empate");
        verify(suscriptor, never()).update(any(), any(), any());
    }
}
