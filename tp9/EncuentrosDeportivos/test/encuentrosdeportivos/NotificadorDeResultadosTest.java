package encuentrosdeportivos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

public class NotificadorDeResultadosTest {
    INotificadorDeResultados notificadorDeResultados;
    List<RegistroAEncuentrosDeportivos> registros;
    ISuscriptor suscriptor;
    String deporte;
    String contrincante;

    @BeforeEach
    void setUp() {
        deporte = "Deporte";
        contrincante = "Contrincante";
        suscriptor = mock(ISuscriptor.class);
        registros = new ArrayList<>();
        notificadorDeResultados = new NotificadorDeResultados(registros);
    }

    @DisplayName("Existe Notificador")
    @Test
    void hatNotificador() {
        assertNotNull(notificadorDeResultados);
    }

    @DisplayName("Cuando registro una suscripción completa al Notificador este crea y registra un Registro a deporte y contrincante")
    @Test
    void testRegistroCompleto() {
        RegistroAEncuentrosDeportivos registro;
        assertEquals(0, registros.size());
        notificadorDeResultados.registrarA(deporte, contrincante, suscriptor);
        assertEquals(1, registros.size());
        registro = registros.get(0);
        assertEquals(RegistroADeporteYContrincante.class, registro.getClass());
        assertEquals(registro.contrincante(), contrincante);
        assertEquals(registro.deporte(), deporte);
    }

    @DisplayName("Cuando registro una suscripción a deporte 0l Notificador este crea y almancena un Registro a deporte")
    @Test
    void testRegistroDeporte() {
        RegistroAEncuentrosDeportivos registro;
        assertEquals(0, registros.size());
        notificadorDeResultados.registrarADeporte("DeporteEjemplo", suscriptor);
        assertEquals(1, registros.size());
        registro = registros.get(0);
        assertEquals(RegistroADeporte.class, registro.getClass());
        assertEquals(registro.deporte(), "DeporteEjemplo");
    }

    @DisplayName("Cuando registro una suscripción a contrincante al Notificador este crea y almacena un Registro a contrincante")
    @Test
    void testRegistroContrincante() {
        RegistroAEncuentrosDeportivos registro;
        assertEquals(0, registros.size());
        notificadorDeResultados.registrarAContrincante("ContrincanteEjemplo", suscriptor);
        assertEquals(1, registros.size());
        registro = registros.get(0);
        assertEquals(RegistroAContrincante.class, registro.getClass());
        assertEquals(registro.contrincante(), "ContrincanteEjemplo");
    }


}
