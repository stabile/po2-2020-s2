package encuentrosdeportivos;

import java.util.List;

public interface ISuscriptor {
    void update(String deporte, List<String> contrincantes, String resultado);
}
