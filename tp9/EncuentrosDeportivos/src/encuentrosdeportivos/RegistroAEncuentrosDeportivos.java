package encuentrosdeportivos;

import java.util.List;

public abstract class RegistroAEncuentrosDeportivos {
    private String deporte;
    private String contrincante;
    private ISuscriptor suscriptor;

    public RegistroAEncuentrosDeportivos(String deporte, String contrincante,  ISuscriptor suscriptor) {
        this.deporte = deporte;
        this.contrincante = contrincante;
        this.suscriptor = suscriptor;
    }

    public abstract void notificar(String futbol, List<String> contrincantes, String empate);

    protected String deporte() { return this.deporte;}

    protected ISuscriptor suscriptor() { return this.suscriptor;}

    protected String contrincante() { return this.contrincante;}
}
