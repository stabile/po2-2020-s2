package encuentrosdeportivos;

import java.util.List;

public class RegistroADeporteYContrincante extends RegistroAEncuentrosDeportivos {
    public RegistroADeporteYContrincante(String deporte, String contrincante, ISuscriptor suscriptor) {
        super(deporte, contrincante, suscriptor);
    }

    @Override
    public void notificar(String deporte, List<String> contrincantes, String resultado) {
        if(this.deporte().equals(deporte) && contrincantes.contains(this.contrincante()) )
            this.suscriptor().update(deporte, contrincantes, resultado);
    }
}
