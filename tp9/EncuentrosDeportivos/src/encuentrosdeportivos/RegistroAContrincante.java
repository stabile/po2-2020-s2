package encuentrosdeportivos;

import java.util.List;

public class RegistroAContrincante extends RegistroAEncuentrosDeportivos {

    public RegistroAContrincante(String contrincante, ISuscriptor suscriptor) {
        super(null, contrincante, suscriptor);
    }

    @Override
    public void notificar(String deporte, List<String> contrincantes, String resultado) {
        if (contrincantes.contains(this.contrincante()))
            this.suscriptor().update(deporte, contrincantes, resultado);
    }
}
