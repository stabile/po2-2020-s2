package encuentrosdeportivos;

import java.util.Arrays;
import java.util.List;

public class RegistroADeporte extends RegistroAEncuentrosDeportivos {
    public RegistroADeporte(String deporte, ISuscriptor suscriptor) {
        super(deporte, null, suscriptor);
    }

    @Override
    public void notificar(String deporte, List<String> contrincantes, String resultado) {
        if(this.deporte().equals(deporte))
            this.suscriptor().update(deporte, contrincantes, resultado);
    }
}
