package encuentrosdeportivos;

public interface INotificadorDeResultados {
    void registrarA(String deporte, String contrincante, ISuscriptor suscriptor);

    void registrarADeporte(String deporte, ISuscriptor suscriptor);

    void registrarAContrincante(String contrincanteEjemplo, ISuscriptor suscriptor);
}
