package encuentrosdeportivos;


import java.util.List;

public class NotificadorDeResultados implements INotificadorDeResultados {
    private List<RegistroAEncuentrosDeportivos> registros;

    public NotificadorDeResultados(List<RegistroAEncuentrosDeportivos> registros) {
        this.registros = registros;
    }

    @Override
    public void registrarA(String deporte, String contrincante, ISuscriptor suscriptor) {
        this.registros.add(new RegistroADeporteYContrincante(deporte, contrincante, suscriptor));
    }

    @Override
    public void registrarADeporte(String deporte, ISuscriptor suscriptor) {
        this.registros.add(new RegistroADeporte(deporte, suscriptor));
    }

    @Override
    public void registrarAContrincante(String contrincante, ISuscriptor suscriptor) {
        this.registros.add(new RegistroAContrincante(contrincante, suscriptor));
    }
}
