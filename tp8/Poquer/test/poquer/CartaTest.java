package poquer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


import static org.junit.jupiter.api.Assertions.*;

public class CartaTest {

    Carta carta;

    @BeforeEach
    void setUp() {
        String valor = "1";
        String palo  = "P";
       carta = new Carta(valor, palo);
    }

    @DisplayName("Existe un Carta")
    @Test
    void testHayCarta() {
        assertEquals(Carta.class, carta.getClass());
    }

    @DisplayName("El Valor de una carta es el dado")
    @Test
    void testCartaValor() {
        assertEquals("1", carta.valor());
    }

    @DisplayName("El palo de una carta es el dado")
    @Test
    void testCartaPalo() {
        assertEquals("P", carta.palo());
    }




    @DisplayName("Cuando una carta tiene un valor mayor que otra, carta es mayor")
    @ParameterizedTest
    @CsvSource({
            "AP,KP",
            "AP,2P",
            "KP,JC",
            "KD,QC",
            "QT,JC",
            "QC,10C",
            "JD,10T",
            "JT,9T",
            "10T,9T",
            "10T,2T",
            "10T,1T",
            "9T,8T",
            "9T,1T",
            "2T,1T",
    })
    void testMayorCartaVerdad(String sc1, String sc2) {
        assertTrue(carta(sc1).mayorA(carta(sc2)));
    }

    String valor(String carta) {
        return carta.substring(0, carta.length() - 1);
    }

    String palo(String carta) {
        return carta.substring(carta.length() - 1);
    }

    ICarta carta(String scarta) {
        return new Carta(valor(scarta), palo(scarta));
    }

    @DisplayName("Cuando una carta no es mayor que otra....")
    @ParameterizedTest
    @CsvSource({
            "1P,AC","JD,JT",
            "2C,KD","7P,KT",
            "10P,10C","3D,9T",
            "5P,7P", "QC,QD",
            "KP,KP",
            "3D,10T", "9D,10C", "9C,KC", "9T,QD", "9P,JT"
    })
    void testMayorFalso(String sc1, String sc2) {
        assertFalse(carta(sc1).mayorA(carta(sc2)));
    }

    @DisplayName("Dos cartas tienen el mismo palo")
    @Test
    void testCartaTieneIgualPalo() {
        assertTrue(carta.mismoPalo(new Carta("2", "P")), "Deberian tener el mismo palo");
    }
}
