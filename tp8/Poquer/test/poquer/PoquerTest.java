package poquer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;


public class PoquerTest {

    PoquerStatus ps;

    @BeforeEach
    void setUp() { // <<-- SetUp
        ps = new PoquerStatus();
    }

    @DisplayName("Existe PoquerStatus")
    @Test
    void hayPoquerStatus() {
        assertEquals(PoquerStatus.class, ps.getClass()); // <<-- Verify
    }

    String valor(String scarta) {
        return scarta.substring(0, scarta.length() - 1);
    }

    String palo(String scarta) {
        return scarta.substring(scarta.length() - 1);
    }

    Carta carta(String scarta){
        return new Carta(valor(scarta), palo(scarta));
    }

    @DisplayName("Cuando hay Nada se verifica")
    @ParameterizedTest
    @CsvSource({
            "2P,2C,1D,1T,7P",
            "4P,4C,3D,7P,3T",
            "5P,5C,7P,4D,4T",
            "KP,7P,JC,QD,QT",
            "10P,9P,8C,7D,6T",
    }) // <<-- Setup
    void testHayNada(String c1, String c2, String c3 ,String c4, String c5) {
        Jugada jugada = ps.verificar(carta(c1), carta(c2), carta(c3), carta(c4), carta(c5)); // <<-- Excercise
        assertTrue( jugada.esNada());//<<-- Verify
        // Teardown
    }

    @Nested
    class TestPoquer{
        @DisplayName("Cuando hay Poquer en cualquiera de los cinco ordenes de cartas se verifica")
        @ParameterizedTest
        @CsvSource({
                "1P,1C,1D,1T,7P",
                "3P,3C,3D,7P,3T",
                "4P,4C,7P,4D,4T",
                "QP,7P,QC,QD,QT",
                "7P,6P,6C,6D,6T",
        }) // <<-- Setup
        void testVerificarPoquer(String c1, String c2, String c3 ,String c4, String c5) {
            Jugada jugada = ps.verificar(carta(c1), carta(c2), carta(c3), carta(c4), carta(c5)); // <<-- Excercise
            assertTrue( jugada.esPoquer());//<<-- Verify
            // Teardown
        }

        @DisplayName("Cuando no hay Poquer, no se verifica")
        @ParameterizedTest
        @CsvSource({
                "1P,JC,JD,JT,7P",
                "KP,2C,KD,7P,KT",
                "10P,10C,7P,3D,10T",
                "5P,7P,5C,5D,4T",
                "7P,5P,6C,6D,6T",
        }) // <<-- Setup
        void testNoVerificarPoquer(String c1, String c2, String c3 ,String c4, String c5) {
            Jugada jugada = ps.verificar(carta(c1), carta(c2), carta(c3), carta(c4), carta(c5)); // <<-- Excercise
            assertFalse( jugada.esPoquer());//<<-- Verify
            // Teardown
        }

    }

    @Nested
    class TestTrio{
        @DisplayName("Cuando hay Trio se verifica")
        @ParameterizedTest
        @CsvSource({
                "1P,JC,JD,JT,7P",
                "KP,2C,KD,7P,KT",
                "10P,10C,7P,3D,10T",
                "5P,7P,5C,5D,4T",
                "7P,5P,6C,6D,6T",
        }) // <<-- Setup
        void testVerificarTrio(String c1, String c2, String c3 ,String c4, String c5) {
            Jugada jugada = ps.verificar(carta(c1), carta(c2), carta(c3), carta(c4), carta(c5)); // <<-- Excercise
            assertTrue( jugada.esTrio());// <<-- Verify
            // Teardown
        }

        @DisplayName("Cuando no hay Trio no se verifica")
        @ParameterizedTest
        @CsvSource({
                "1P,JC,KD,JT,7P",
                "KP,2C,QD,7P,KT",
                "10P,1C,7P,3D,10T",
                "5P,7P,6C,5D,4T",
                "7P,5P,7C,6D,6T",
        }) // <<-- Setup
        void testNoVerificarTrio(String c1, String c2, String c3 ,String c4, String c5) {
            Jugada jugada = ps.verificar(carta(c1), carta(c2), carta(c3), carta(c4), carta(c5)); // <<-- Excercise
            assertFalse( jugada.esTrio());// <<-- Verify
            // Teardown
        }

    }

    @Nested
    class TestColor {
        @DisplayName("Cuando hay Color se verifica")
        @ParameterizedTest
        @CsvSource({
                "1P,JP,KP,QP,7P",
                "KC,2C,QC,7C,3C",
                "10T,1T,7T,3T,9T",
                "5D,7D,6D,QD,4D",
        }) // <<-- Setup
        void testVerificarColor(String c1, String c2, String c3 ,String c4, String c5) {
            Jugada jugada = ps.verificar(carta(c1), carta(c2), carta(c3), carta(c4), carta(c5)); // <<-- Excercise
            assertTrue( jugada.esColor());//<<-- Verify
            // Teardown
        }

        @DisplayName("Cuando no hay Color no se verifica")
        @ParameterizedTest
        @CsvSource({
                "1P,JP,KP,QP,7C",
                "KC,2C,QC,7C,3P",
                "10T,1T,7T,3T,9D",
                "5D,7D,6D,QD,4T",
        }) // <<-- Setup
        void testNoVerificarColor(String c1, String c2, String c3 ,String c4, String c5) {
            Jugada jugada = ps.verificar(carta(c1), carta(c2), carta(c3), carta(c4), carta(c5)); // <<-- Excercise
            assertFalse( jugada.esColor());//<<-- Verify
            // Teardown
        }
    }


    @Test
    void testPoquerGanaATrio() {
        Jugada trio = ps.verificar(carta("7P"), carta("7D"), carta("7C"), carta("KC"), carta("QD")); // <<-- Excercise
        Jugada poquer = ps.verificar(carta("AP"), carta("2D"), carta("2T"), carta("2P"), carta("2C")); // <<-- Excercise
        assertTrue( trio.esTrio());//<<-- Verify
        assertTrue( poquer.esPoquer());//<<-- Verify
        assertEquals(poquer, poquer.ganaA(trio));//<<-- Verify
        assertEquals(poquer, trio.ganaA(poquer));//<<-- Verify
    }

    @Test
    void testPoquerDeAcesGanaAPoquerDeReyes() {
        Jugada poquerDeAces = ps.verificar(carta("AP"), carta("AD"), carta("AC"), carta("AT"), carta("QD")); // <<-- Excercise
        Jugada poquerDeKings = ps.verificar(carta("KP"), carta("KD"), carta("KC"), carta("KT"), carta("QD")); // <<-- Excercise
        assertTrue( poquerDeAces.esPoquer());//<<-- Verify
        assertTrue( poquerDeKings.esPoquer());//<<-- Verify
        assertEquals(poquerDeAces, poquerDeKings.ganaA(poquerDeAces));
    }
}
