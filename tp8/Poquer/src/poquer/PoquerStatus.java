package poquer;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PoquerStatus {
    public Jugada verificar(ICarta carta1, ICarta carta2,
                            ICarta carta3, ICarta carta4, ICarta carta5) {

        return  Jugada.instancia(Arrays.asList(carta1, carta2, carta3, carta4, carta5));
    }

}
