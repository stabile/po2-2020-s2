package poquer;

import java.util.List;

public class Poquer extends Jugada {
    public Poquer(List<ICarta> cartas) {
        super(cartas);
    }

    @Override
    public boolean esPoquer() {
        return true;
    }

    public ICarta cartaDeReferencia() {
        return !valores(this.cartas()).get(0).equals(valores(this.cartas()).get(1))
                ? this.cartas().get(2) : this.cartas().get(1);
    }

    public Jugada ganaA(Jugada jugada) {
        return jugada.ganaAPoquer(this.cartaDeReferencia()) ? jugada : this;
    }
    @Override
    public boolean ganaAPoquer(ICarta cartaDelOtroJuego) {
        return this.cartaDeReferencia().mayorA(cartaDelOtroJuego);
    }

    @Override
    protected boolean ganaATrio(ICarta cartaDelPoquer) {
        return true;
    }

    @Override
    protected boolean ganaAColor(ICarta cartaDelPoquer) {
        return false;
    }

    @Override
    protected boolean ganaANada(ICarta cartaDeReferencia) {
        return false;
    }


}
