package poquer;

import java.util.List;

public class Nada extends Jugada {
    public Nada(List<ICarta> cartas) {
        super(cartas);
    }

    @Override
    public boolean esNada() {
        return true;
    }

    public ICarta cartaDeReferencia() {
        return this.cartas().get(1);
    }

    @Override
    public Jugada ganaA(Jugada jugada) {
        return jugada.ganaANada(this.cartaDeReferencia()) ? jugada : this;
    }

    @Override
    public boolean ganaAPoquer(ICarta carta) { return false; }

    @Override
    public boolean ganaATrio(ICarta carta) { return false; }

    @Override
    public boolean ganaAColor(ICarta carta) { return false; }

    @Override
    public boolean ganaANada(ICarta mejorCartaDelOtroJuego) {
        return this.cartaDeReferencia().mayorA(mejorCartaDelOtroJuego);
    }
}
