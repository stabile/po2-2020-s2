package poquer;

public interface ICarta {
    String valor();
    String palo();
    boolean mayorA(ICarta carta2);
}
