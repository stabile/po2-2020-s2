package poquer;

public class Carta implements ICarta {
    private final String valor;
    private final String palo;

    public Carta(String valor, String palo) {
        this.valor = valor;
        this.palo = palo;
    }

    public String valor() {
        return this.valor;
    }

    public String palo() {
        return this.palo;
    }

    public boolean mayorA(ICarta carta) {
        return this.esAs()   && !((Carta)carta).esAs()
            || this.esKing()   && !((Carta)carta).esKing()
            || this.esQueen()  && !((Carta)carta).esQueen()
            || this.esJack()   && !((Carta)carta).esJack()
            || this.esDiez()   && !((Carta)carta).esDiez()
            || this.valor().compareTo(carta.valor()) > 0 && !((Carta)carta).esDiez();
    }

    public boolean esDiez() {
        return this.valor().equals("10");
    }

    public boolean esAs() {
        return this.valor().equals("A") || this.valor().equals("1");
    }

    public boolean esKing() {
        return this.valor().equals("K");
    }

    public boolean esQueen() {
        return this.valor().equals("Q");
    }

    public boolean esJack() {
        return this.valor().equals("J");
    }

    public boolean mismoPalo(Carta carta) {
        return this.palo().equals(carta.palo());
    }
}
