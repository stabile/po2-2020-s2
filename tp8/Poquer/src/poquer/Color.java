package poquer;

import java.util.List;

public class Color extends Jugada {
    public Color(List<ICarta> cartas) {
        super(cartas);
    }
    @Override
    public boolean esColor() {
        return true;
    }

    public ICarta cartaDeReferencia() {
        return this.cartas().get(1);
    }

    @Override
    public Jugada ganaA(Jugada jugada) {
        return jugada.ganaAColor(this.cartaDeReferencia()) ? jugada : this;
    }

    @Override
    public boolean ganaAPoquer(ICarta carta) { return false; }

    @Override
    public boolean ganaATrio(ICarta carta) { return false; }

    @Override
    public boolean ganaAColor(ICarta mejorCartaDelOtroJuego) {
        return this.cartaDeReferencia().mayorA(mejorCartaDelOtroJuego);
    }

    @Override
    public boolean ganaANada(ICarta carta) { return true; }
}
