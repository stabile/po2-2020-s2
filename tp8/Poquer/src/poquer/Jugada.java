package poquer;

import java.util.List;
import java.util.stream.Collectors;

public abstract class Jugada {
    
    private final List<String> valoresDeLaMano;
    private final List<String> palosDeLaMano;

    private List<ICarta> cartas;

    Jugada(List<ICarta> cartas){
        this.cartas = cartas;
        this.valoresDeLaMano = cartas
                .stream()
                .map(ICarta::valor)
                .collect(Collectors.toList());
        this.palosDeLaMano = cartas
                .stream()
                .map(ICarta::palo)
                .collect(Collectors.toList());
    }

    protected List<ICarta> cartas() { return this.cartas;}

    public static List<String> palos(List<ICarta> cartas) {
        return cartas.stream().map(ICarta::palo).collect(Collectors.toList());
    }

    public static List<String> valores(List<ICarta> cartas) {
        return cartas.stream().map(ICarta::valor).collect(Collectors.toList());
    }

    public static boolean esPoquerS(List<ICarta> cartas) {
        List<String> valoresDeLaMano = valores(cartas);
        return sn_IgualesEn_AElementoEnPosicion_(4, valoresDeLaMano, 0) ||
               sn_IgualesEn_AElementoEnPosicion_(4, valoresDeLaMano, 1);
    }

    public static boolean esTrioS(List<ICarta> cartas)  {
        List<String> valoresDeLaMano = valores(cartas);
        return sn_IgualesEn_AElementoEnPosicion_(3, valoresDeLaMano, 0) ||
                sn_IgualesEn_AElementoEnPosicion_(3, valoresDeLaMano, 1) ||
                sn_IgualesEn_AElementoEnPosicion_(3, valoresDeLaMano, 2) ;
    }
    public static boolean esColorS(List<ICarta> cartas) {
        List<String> palosDeLaMano = palos(cartas);
        return sn_IgualesEn_AElementoEnPosicion_(5, palosDeLaMano, 0);
    }
    private static boolean sn_IgualesEn_AElementoEnPosicion_(int n, List<String> valoresDeLaMano, int posicion) {
        return valoresDeLaMano.stream()
                .filter(valor -> valoresDeLaMano.get(posicion).equals(valor))
                .count() == n;
    }


    public boolean esNada() {
        return !esPoquer() && !esTrio() && !esColor();
    }

    public boolean esPoquer() {
        return n_IgualesEn_AElementoEnPosicion_(4, valoresDeLaMano, 0) ||
               n_IgualesEn_AElementoEnPosicion_(4, valoresDeLaMano, 1);
    }

    public boolean esTrio() {
        return n_IgualesEn_AElementoEnPosicion_(3, valoresDeLaMano, 0) ||
                n_IgualesEn_AElementoEnPosicion_(3, valoresDeLaMano, 1) ||
                n_IgualesEn_AElementoEnPosicion_(3, valoresDeLaMano, 2) ;
    }

    private boolean n_IgualesEn_AElementoEnPosicion_(int n, List<String> valoresDeLaMano, int posicion) {
        return valoresDeLaMano.stream()
                .filter(valor -> valoresDeLaMano.get(posicion).equals(valor))
                .count() == n;
    }

    public boolean esColor() {
        return n_IgualesEn_AElementoEnPosicion_(5, palosDeLaMano, 0);
    }


    // Factory method
    public static Jugada instancia(List<ICarta> cartas) {
        return esPoquerS(cartas) ? new Poquer(cartas) :
               esTrioS(cartas)   ? new Trio(cartas)   :
               esColorS(cartas)  ? new Color(cartas)  :
                                 new Nada(cartas);
    }

    public abstract Jugada ganaA(Jugada jugada);

    protected abstract boolean ganaAPoquer(ICarta cartaDelPoquer) ;
    protected abstract boolean ganaATrio(ICarta cartaDelPoquer) ;
    protected abstract boolean ganaAColor(ICarta cartaDelPoquer) ;
    protected abstract boolean ganaANada(ICarta cartaDeReferencia);
}
