2.- ¿Cómo se indica en mockito que el objeto mock debe recibir una secuencia de
     mensajes particular en un orden preestablecido? Agregue un ejemplo.

     Primero se crea un objeto inOrder a partir del mock al que se le verificara
     la llegada de mensajes. Luego para verificar el envío de mensajes, enviamos
     una serie de verify(mock).mensaje...() al objeto inOrder en el orden que
     corresponda para verificar su cumplimiento. Adicionalmente se puede
     verificar que no se realicen mas interacciones con el orden dado enviando
     el mensaje verifyNoMoreInteractions al objeto inOrder. Sin embargo si el
     mock recibiera algún otro mensaje ademas de los verificados, este ultimo
     mensaje a inOrden no fallaría. Para verificar esto puede enviarse el
     mensaje de clase verifyNoMoreInteractions(mock) y si se hubieran enviado
     otros mensajes ademas de los verificados al mock, fallara.

     Clase mock = mock(Clase.class);

     InOrder inOrder = inOrder(mock);

     inOrder.verify(mock).primerMensaje();
     inOrder.verify(mock).segundoMensaje();

     inOrder.verifyNoMoreInteractions();


3.- ¿Cómo hacer que un objeto mock este preparado para recibir algunos mensajes
     sin importar el orden o la obligatoriedad de recibirlos? Agregue un ejemplo.

     Para que el mock este preparado para recibir msg sin importar el orden ni
     la obligatoriedad, debe crearse a partir de una interfaz o clase que
     declare estos mensajes.
     Así se puede mockear IInterfaz y preguntarle si existe al mock.

     interface IInterfaz {
        boolean existo();
     }
     ...
     IInterfaz mock = mock(IInterfaz.class);
     doReturn(true).when(mock.existo());

     assertTrue(mock.existo()); // verde!

4.- ¿Es posible anidar el envío de mensajes con mockito? Si es posible, ¿Cómo se
     hace?

     Si:
        interface IHabitacion {
            int cantidadDeCamas();
        }

        interface ICasa {
            int cantidadDeVentanas();
            IHabitacion dormitorio();
        }
        ...
        ICasa mockCasa = mock(ICasa.class);
        IHabitacion mockDormitorio = mock(IHabitacion.class);
        when(mockCasa.dormitorio()).thenReturn(mockDormitorio);
        when(mockCasa.dormitorio().cantidadDeCamas()).thenReturn(2);

        assertEquals(2, mockCasa.dormitorio().cantidadDeCamas());


5.- ¿Como es la forma de verificación con mockito?

        Mockito verifica el comportamiento del SUT espiando las colaboraciones
        que este realiza con los mock inyectados. Si estas fueron realizadas
        como fue previsto, la verificación se considera exitosa.

