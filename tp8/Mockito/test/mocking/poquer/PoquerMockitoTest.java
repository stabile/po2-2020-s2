package mocking.poquer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import poquer.ICarta;
import poquer.PoquerStatus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PoquerMockitoTest {

    PoquerStatus ps;

    @BeforeEach
    void setUp() { // <<-- SetUp
        ps = new PoquerStatus();
    }

    @DisplayName("Existe PoquerStatus")
    @Test
    void hayPoquerStatus() {
        assertEquals(PoquerStatus.class, ps.getClass()); // <<-- Verify
    }

    String valor(String scarta) {
        return scarta.substring(0, scarta.length() - 1);
    }

    String palo(String scarta) {
        return scarta.substring(scarta.length() - 1);
    }

    ICarta crearCarta(String scarta){
//        return new ICarta(valor(scarta), palo(scarta));
        ICarta mockICarta = mock(ICarta.class);
        when(mockICarta.valor()).thenReturn(valor(scarta));
        when(mockICarta.palo()).thenReturn(palo(scarta));
        return mockICarta;
    }

    @DisplayName("Cuando hay Nada se verifica")
    @ParameterizedTest
    @CsvSource({
            "2P,2C,1D,1T,7P",
            "4P,4C,3D,7P,3T",
            "5P,5C,7P,4D,4T",
            "KP,7P,JC,QD,QT",
            "10P,9P,8C,7D,6T",
    }) // <<-- Setup
    void testHayNada(String c1, String c2, String c3 ,String c4, String c5) {
        assertEquals("Nada", ps.verificar(crearCarta(c1), crearCarta(c2), crearCarta(c3), crearCarta(c4), crearCarta(c5))); // <<-- Excercise , Verify
        // Teardown
    }

    @Nested
    class TestPoquer{
        @DisplayName("Cuando hay Poquer en cualquiera de los cinco ordenes de cartas se verifica")
        @ParameterizedTest
        @CsvSource({
                "1P,1C,1D,1T,7P",
                "3P,3C,3D,7P,3T",
                "4P,4C,7P,4D,4T",
                "QP,7P,QC,QD,QT",
                "7P,6P,6C,6D,6T",
        }) // <<-- Setup
        void testVerificarPoquer(String c1, String c2, String c3 ,String c4, String c5) {
            assertEquals("Poquer", ps.verificar(crearCarta(c1), crearCarta(c2), crearCarta(c3), crearCarta(c4), crearCarta(c5))); // <<-- Excercise , Verify
            // Teardown
        }

        @DisplayName("Cuando no hay Poquer, no se verifica")
        @ParameterizedTest
        @CsvSource({
                "1P,JC,JD,JT,7P",
                "KP,2C,KD,7P,KT",
                "10P,10C,7P,3D,10T",
                "5P,7P,5C,5D,4T",
                "7P,5P,6C,6D,6T",
        }) // <<-- Setup
        void testNoVerificarPoquer(String c1, String c2, String c3 ,String c4, String c5) {
            assertNotEquals("Poquer", ps.verificar(crearCarta(c1), crearCarta(c2), crearCarta(c3), crearCarta(c4), crearCarta(c5))); // <<-- Excercise , Verify
            // Teardown
        }

    }

    @Nested
    class TestTrio{
        @DisplayName("Cuando hay Trio se verifica")
        @ParameterizedTest
        @CsvSource({
                "1P,JC,JD,JT,7P",
                "KP,2C,KD,7P,KT",
                "10P,10C,7P,3D,10T",
                "5P,7P,5C,5D,4T",
                "7P,5P,6C,6D,6T",
        }) // <<-- Setup
        void testVerificarTrio(String c1, String c2, String c3 ,String c4, String c5) {
            assertEquals("Trio", ps.verificar(crearCarta(c1), crearCarta(c2), crearCarta(c3), crearCarta(c4), crearCarta(c5))); // <<-- Excercise , Verify
            // Teardown
        }

        @DisplayName("Cuando no hay Trio no se verifica")
        @ParameterizedTest
        @CsvSource({
                "1P,JC,KD,JT,7P",
                "KP,2C,QD,7P,KT",
                "10P,1C,7P,3D,10T",
                "5P,7P,6C,5D,4T",
                "7P,5P,7C,6D,6T",
        }) // <<-- Setup
        void testNoVerificarTrio(String c1, String c2, String c3 ,String c4, String c5) {
            assertNotEquals("Trio", ps.verificar(crearCarta(c1), crearCarta(c2), crearCarta(c3), crearCarta(c4), crearCarta(c5))); // <<-- Excercise , Verify
            // Teardown
        }

    }

    @Nested
    class TestColor {
        @DisplayName("Cuando hay Color se verifica")
        @ParameterizedTest
        @CsvSource({
                "1P,JP,KP,QP,7P",
                "KC,2C,QC,7C,3C",
                "10T,1T,7T,3T,9T",
                "5D,7D,6D,QD,4D",
        }) // <<-- Setup
        void testVerificarColor(String c1, String c2, String c3 ,String c4, String c5) {
            assertEquals("Color", ps.verificar(crearCarta(c1), crearCarta(c2), crearCarta(c3), crearCarta(c4), crearCarta(c5))); // <<-- Excercise , Verify
            // Teardown
        }

        @DisplayName("Cuando no hay Color no se verifica")
        @ParameterizedTest
        @CsvSource({
                "1P,JP,KP,QP,7C",
                "KC,2C,QC,7C,3P",
                "10T,1T,7T,3T,9D",
                "5D,7D,6D,QD,4T",
        }) // <<-- Setup
        void testNoVerificarColor(String c1, String c2, String c3 ,String c4, String c5) {
            assertNotEquals("Color", ps.verificar(crearCarta(c1), crearCarta(c2), crearCarta(c3), crearCarta(c4), crearCarta(c5))); // <<-- Excercise , Verify
            // Teardown
        }
    }

}
