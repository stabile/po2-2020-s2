package mocking;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class MockitoTest {

    interface IHabitacion {
        int cantidadDeCamas();
    }

    interface ICasa {
        int cantidadDeVentanas();
        IHabitacion dormitorio();
        void cerrar();
        void limpiar();
        void abrir();
    }

    class Manager {
        ICasa casa;
        Manager(ICasa casa) {
            this.casa = casa;
        }

        void higienizar() {
            casa.abrir();
            casa.limpiar();
            casa.cerrar();
        }
    }

    @Test
    void testWhenThenReturn() {
        ICasa mock = mock(ICasa.class);
        when(mock.cantidadDeVentanas()).thenReturn(4);
        assertEquals(4, mock.cantidadDeVentanas());
    }

    @DisplayName("Mensajes anidados a mock resuelto con RETURNS_DEEP_STUBS")
    @Test
    void testNestedCalls() {
        ICasa mock = mock(ICasa.class, RETURNS_DEEP_STUBS);
        when(mock.dormitorio().cantidadDeCamas()).thenReturn(2);

        assertEquals(2, mock.dormitorio().cantidadDeCamas());
    }

    @DisplayName("Mensaje anidado a mock resuelto con paulatinos when")
    @Test
    void testNestedCallsOldSchool() {
        ICasa mockCasa = mock(ICasa.class);
        IHabitacion mockDormitorio = mock(IHabitacion.class);
        when(mockCasa.dormitorio()).thenReturn(mockDormitorio);
        when(mockCasa.dormitorio().cantidadDeCamas()).thenReturn(2);

        assertEquals(2, mockCasa.dormitorio().cantidadDeCamas());
    }

    @DisplayName("Verifica que los msg al mock van en orden, solo dos de ellos")
    @Test
    void testInOrderExtricto() {
        ICasa mockCasa = mock(ICasa.class);
        Manager gestor = new Manager(mockCasa);
        InOrder inOrder = inOrder(mockCasa);

        gestor.higienizar();

        inOrder.verify(mockCasa).abrir();
        inOrder.verify(mockCasa).limpiar();
        inOrder.verify(mockCasa).cerrar();
        inOrder.verifyNoMoreInteractions();
        verifyNoMoreInteractions(mockCasa);
    }

    @DisplayName("Los msg al mock van en orden, no verifica otros llamados")
    @Test
    void testInOrderAbierto() {
        ICasa mockCasa = mock(ICasa.class);
        Manager gestor = new Manager(mockCasa);
        InOrder inOrder = inOrder(mockCasa);

        gestor.higienizar();

        inOrder.verify(mockCasa).limpiar();
        inOrder.verify(mockCasa).cerrar();
        inOrder.verifyNoMoreInteractions();
    }

    interface IInterfaz {
        boolean existo();
    }

    @DisplayName("Un mock puede recibir msg cuando el msg esta declarado.")
    @Test
    void testMockPreparado() {
        IInterfaz mock = mock(IInterfaz.class);
        doReturn(true).when(mock).existo();

        assertTrue(mock.existo());
    }


}
