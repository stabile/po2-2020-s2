Ejercicio 3

1.- Los tipos de datos primitivos son tipos especiales de datos construidos dentro del lenguaje. Esto significa que no se instancian como cualquier otra clase. 

2.- Un Integer es un objeto que envuelve al tipo de dato primitivo int. Dando de esta forma las ventajas inherentes a la manipulación de objetos a los datos primitivos. Adicionando perdida de performance. Esto ultimo debido a que en cada operación debe envolver y desenvolver (boxing, unboxing) el valor.

Ejercicio 5

├── model
│   ├── gui
│   │   └── Gui.class
│   ├── Model.class
│   └── stack
│       └── Stack.class

1.- ¿Como están organizadas en el sistema de archivos?
Las clases creadas estan organizadas jerarquicamente. 
2.- ¿Encuentra alguna relación entre el nombre del paquete y la ubicación de los archivos fuentes de las clases (.java) y los archivos compilados (.class)?
El nombre de los paquetes guarda relacion directa con el path en el file system hacia cada uno de ambos archivos, fuentes y compilados.

Ejercicio 8

Si un objeto cualquiera le pide la edad a una Persona: ¿Conoce cómo esta calcula u obtiene tal valor? ¿Cómo se llama el mecanismo de abstracción  que permite esto?

El objeto que pide la edad de una Persona no conoce como esta calcula u obtiene este valor. Este mecanismo se llama encapsulación.
