package unq;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

public class Ejercicio3Test {

    int intSinInicializar;
    Integer integerSinInicializar;

    @DisplayName("un int como var de instancia sin inicializar es cero (0)")
    @Test
    void test1() {
        assertEquals(0, intSinInicializar);
    }

    @DisplayName("un Integer como var de instancia sin inicializar es null")
    @Test
    void test2() {
        assertNull(integerSinInicializar);
    }


    @DisplayName("un int como var de metodo sin inicializar da error de compilación")
    @Test
    void test3() {
//        int intLocalSinInicializar;
//        assertEquals(0, intLocalSinInicializar); 
    }

    @DisplayName("un Integer como var de metodo sin inicializar da error de compilación")
    @Test
    void test4() {
//        Integer integerLocalSinInicializar;
//        assertNull(integerLocalSinInicializar);
    }

    @DisplayName("array de int se inicializa con new.....")
    @Test
    void test5() {
        int[] arregloDeEnteros;
        arregloDeEnteros = new int[5];
        arregloDeEnteros[4] = 19;
        assertEquals(19, arregloDeEnteros[4]);
        assertEquals(0, arregloDeEnteros[3]);
    }

}
