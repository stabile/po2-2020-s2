package unq;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

public class CounterTest {

    private Counter counter;

    /**
     * @throws Exception
     */
    @BeforeEach
    public void setUp() throws Exception {
        // Se crea el contador
        counter = new Counter();

        // se agregan los numeros. Un solo par y nueve impares
        counter.addNumber(1);
        counter.addNumber(3);
        counter.addNumber(2);
        counter.addNumber(5); 
        counter.addNumber(7); 
        counter.addNumber(9); 
        counter.addNumber(6);
        counter.addNumber(1);
        counter.addNumber(1);
        counter.addNumber(4);
    }

    @Test
    void counterTieneCeroCantidadDeImpares() {
        assertEquals(0l, new Counter().cantidadDeImpares());
    }

    @Test
    void counterTieneCeroCantidadDePares() {
        assertEquals(0l, new Counter().cantidadDePares());
    }

    @Test
    void counterTieneCeroMultiplosDeSite() {
        assertEquals(0l, new Counter().cantidadDeMultiplosDe(7));
    }

    @Test
    void testCantidadDePares() {
        assertEquals(3l, counter.cantidadDePares());
    }

    @Test
    void testCantidadDeImpares() {
        assertEquals(7l, counter.cantidadDeImpares());
    }

    @Test
    void hayUnMultiploDeSiete() {
        assertEquals(1l, counter.cantidadDeMultiplosDe(7));
    }

    @Test
    void hayTresMultiplosDeTres() {
        assertEquals(3l, counter.cantidadDeMultiplosDe(3));
    }

}
