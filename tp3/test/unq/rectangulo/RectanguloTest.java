package unq.rectangulo;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

public class RectanguloTest {

    IPoint orig;

    @BeforeEach
    void setUp(){
        orig = mock(IPoint.class);
        when(orig.x()).thenReturn(3);
        when(orig.y()).thenReturn(7);
    }

    Rectangulo newRect(IPoint orig, int h, int l){
        return new Rectangulo(orig, h, l);
    }

    @DisplayName("Warm up IPoint")
    @Test
    void test1() {
       IPoint p = mock(IPoint.class);
       when(p.x()).thenReturn(2);
       assertEquals(2, p.x());
    }

    @DisplayName("Hay rectangulo")
    @Test
    void hayRectangulo() {
        Rectangulo rect = new Rectangulo(null, 0, 0);
        assertNotNull(rect);
    }
    
    @DisplayName("Creo un rectangulo con origen en un mock")
    @Test
    void testN() {
        Rectangulo rect = new Rectangulo(orig, 0, 0);
        assertNotNull(rect);
    }

    @DisplayName("Creo un rectangulo de 3 x 7 con area 21")
    @Test
    void rectanguloDe3x7conArea21() {
        IPoint orig = mock(IPoint.class);
        Rectangulo rect = new Rectangulo(orig, 3, 7);
        assertEquals(21, rect.area());
    }

    @DisplayName("Varios rectangulos verificando sus areas")
    @Test
    void rectangulosAreas() {
        assertAll("rectangulos",
                () -> assertEquals(8 , newRect(orig,2,4).area()),
                () -> assertEquals(9 , newRect(orig,3,3).area()),
                () -> assertEquals(12, newRect(orig,3,4).area()),
                () -> assertEquals(42, newRect(orig,6,7).area())
                );
    }

    @DisplayName("Creo un Rectangulo 2x3 con perimetro 10")
    @Test
    void rectanguloDe3x2ConPerim10(){
        IPoint orig = mock(IPoint.class);
        Rectangulo rect = new Rectangulo(orig, 2, 3);
        assertEquals(10, rect.perimetro());
    }

    @DisplayName("Varios rectangulos con sus perimetros")
    @Test
    void rectangulosPerimetros() {
        assertAll("rectangulos Perimetros",
        () -> assertEquals(0 , newRect(orig, 0, 0).perimetro()),
        () -> assertEquals(12, newRect(orig, 3, 3).perimetro()),
        () -> assertEquals(20, newRect(orig, 10, 0).perimetro()),
        () -> assertEquals(22, newRect(orig, 1, 10).perimetro())
        );
    }

    @DisplayName("Un rectangulo de 3x2 es horizontal")
    @Test
    void rectanguloHorizontal() {
        Rectangulo rect = newRect(orig, 3,2);
        assertTrue(rect.horizontal());
        assertFalse(rect.vertical());
    }

    @DisplayName("Un rectangule de 2x3 es vertical")
    @Test
    void rectanguloVertical() {
        Rectangulo rect = newRect(orig, 2, 3);
        assertTrue(rect.vertical());
        assertFalse(rect.horizontal());
    }
    
    @DisplayName("Varios rectangulos Horizontales y verticales")
    @Test
    void rectangulosVerticalesHorizontales() {
        assertAll("rectangulos ",
        () -> assertFalse(newRect(orig, 0, 0).horizontal()),
        () -> assertFalse(newRect(orig, 0, 0).vertical()),
        
        () -> assertFalse(newRect(orig, 3, 3).horizontal()),
        () -> assertFalse(newRect(orig, 3, 3).vertical()),

        () -> assertTrue(newRect(orig, 10, 0).horizontal()),
        () -> assertFalse(newRect(orig, 10, 0).vertical()),

        () -> assertFalse(newRect(orig, 1, 10).horizontal()),
        () -> assertTrue(newRect(orig, 1, 10).vertical())
        );
    }

}
