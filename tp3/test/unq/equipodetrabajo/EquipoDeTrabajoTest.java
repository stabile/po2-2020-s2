package unq.equipodetrabajo;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import java.math.BigDecimal;
import java.util.ArrayList;
//import java.util.Arrays;

public class EquipoDeTrabajoTest {

    IPersona persona1;
    IPersona persona2;
    IPersona persona3;
    IPersona persona4;
    IPersona persona5;
    ArrayList<IPersona> personas;

    @BeforeEach
    void setUp(){
        persona1 = mock(IPersona.class);
        persona2 = mock(IPersona.class);
        persona3 = mock(IPersona.class);
        persona4 = mock(IPersona.class);
        persona5 = mock(IPersona.class);
        personas = new ArrayList<IPersona>();
        personas.add(persona1);
        personas.add(persona2);
        personas.add(persona3);
        personas.add(persona4);
        personas.add(persona5);
    }

    @DisplayName("existe un equipo de trabajo")
    @Test
    void hayEquipoDeTrabajo() {
        EquipoDeTrabajo equipoDeTrabajo = new EquipoDeTrabajo();
        assertNotNull(equipoDeTrabajo);
    }

    @DisplayName("un equipoDeTrabajo tiene nombre")
    @Test
    void tieneNombre() {
        EquipoDeTrabajo equipoDeTrabajo = new EquipoDeTrabajo();
        equipoDeTrabajo.nombre("Nombre");
        assertEquals("Nombre", equipoDeTrabajo.nombre());
    }

    @DisplayName("equipo vacio promedia cero de edad")
    @Test
    void promedioZero() {
        EquipoDeTrabajo equipoDeTrabajo = new EquipoDeTrabajo();
        equipoDeTrabajo.promedio();
        assertEquals(BigDecimal.valueOf(0), equipoDeTrabajo.promedio());
    }

    @DisplayName("equipo promedia 10 de edad")
    @Test
    void promedioDeEdades() {
        EquipoDeTrabajo equipoDeTrabajo = new EquipoDeTrabajo((String)null, (ArrayList<IPersona>) personas);
        when(persona1.edad()).thenReturn(BigDecimal.valueOf(10));
        when(persona2.edad()).thenReturn(BigDecimal.valueOf( 9));
        when(persona3.edad()).thenReturn(BigDecimal.valueOf( 8));
        when(persona4.edad()).thenReturn(BigDecimal.valueOf( 7));
        when(persona5.edad()).thenReturn(BigDecimal.valueOf( 6));
        assertEquals(BigDecimal.valueOf(8), equipoDeTrabajo.promedio());
    }

}
