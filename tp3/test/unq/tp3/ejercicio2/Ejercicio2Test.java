package unq.tp3.ejercicio2;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

public class Ejercicio2Test {

    String a = "abc";
    String s = a;
    String t;

    @DisplayName("a.length() es 3")
    @Test
    void test1() {
        assertEquals(3, a.length());
    }

    @DisplayName("t.length() levanta NullPointerExceptio por no 't' tiene referencia")
    @Test
    void test2() {
    	assertNull(t);
        //assertThrows(NullPointerException.class, () -> { t.length(); });
    }

    @DisplayName("Con a = \"abc\", 1 + a = \"1abc\" por que + atiende tanto a int como a Strings y ambas responden a .toString()")
    @Test
    void test3() {
        assertEquals("1abc" , 1 + a);
    }

    @DisplayName("a.toUpperCase() denota \"ABC\"")
    @Test
    void test4() {
        assertEquals("ABC", a.toUpperCase());
    }

    @DisplayName("\"Libertad\".indexOf(\"r\") es = 4 ya que el indice comienza en 0")
    @Test
    void test5() {
        assertEquals(4, "Libertad".indexOf("r"));
    }

    @DisplayName("7 es =  \"Universidad\".lastIndexOf('i'); por que el index comienza en cero")
    @Test
    void test6() {
        assertEquals(7, "Universidad".lastIndexOf('i')) ;
    }

    @DisplayName("\"Quilmes\".substring(2,4) = \"il\" por que el primer valor del rango es inclusivo y el final es exclusivo")
    @Test
    void test7() {
        assertEquals("il", "Quilmes".substring(2,4));
    }

    @DisplayName("(a.length() + a).startsWith(\"a\"); denota falso ya que el objeto al que se le envia el msg startsWith comienza con \"3\"  ")
    @Test
    void test8() {
        assertFalse((a.length() + a).startsWith("a"));
        assertTrue ((a.length() + a).startsWith("3"));
    }
    
    boolean sonAySiguales(String a, String b) {
    	return b == a;
    }

    @DisplayName("s == a es true, ambos identificadores hacen referencia al mismo objeto")
    @Test
    void test9() {
    	String a = "cadena";
    	String b = a;
        assertEquals( sonAySiguales(a, b), sonAySiguales(a, b)  );
        b = "madera";
        assertEquals( sonAySiguales(a, b), sonAySiguales(a, b)  );

    }

//    @DisplayName("a.substring(1,3).equals(\"bc\") es true.")
//    @Test
//    void test10() {
//        assertTrue(a.substring(1,3).equals("bc"));
//        assertEquals("as", "as");
//        assertTrue("as" == "as");
//        System.out.println("as".getId());
//        System.out.println("as".getId());
//    }

    @DisplayName("Prueba de igualdad con == , .equals() de Object y .equals redefinido")
    @Test
    void pruebaDeIgualdad() {
    	class Crap {}
        class Objeto {
            public int a;
            int b;
            Objeto(int a, int b){
                this.a = a;
                this.b = b;
            }
            public int a() { return a; }
            public int b() { return b; }
            @Override
            public boolean equals(Object y){
                return 
                   y instanceof Objeto	 
                   && a() == ((Objeto)y).a() 
                   && b() == ((Objeto)y).b();
            }
        }

        Objeto p = new Objeto(1,2);
        Objeto q = new Objeto(1,2);
        Crap   c = new Crap();

        assertTrue( p.equals(q)); // diferentes objetos y mismo estado (con equals redefinido)
        p.b = 10; // p cambio
        assertFalse( p.equals(q)); // no son mas iguales
        p.a = 10;
        assertFalse( p.equals(q));
        
        assertFalse(p.equals(c)); // no son el mismo tipo de objeto
        
//        assertFalse( p ==  q);    // diferentes Objetos
//        q = p;
//        assertFalse( p !=  q);    // diferentes Objetos 
        

    }
}
