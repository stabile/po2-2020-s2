package unq.persona;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import java.time.LocalDate;

public class PersonaTest {

    @DisplayName("hay una Persona")
    @Test
    void hayUnaPersona() {
        Persona persona = new Persona();
        assertNotNull(persona);
    }

    @DisplayName("una Persona tiene nombre")
    @Test
    void personaTieneNombre() {
        Persona persona = new Persona("Asio", null);
        assertEquals("Asio", persona.nombre());
    }

    @DisplayName("una Persona tiene fechaDeNacimiento")
    @Test
    void personaTieneFechaDeNacimiento() {
        Persona persona = new Persona(null, LocalDate.of(2000,12,15));
        assertEquals(LocalDate.parse("2000-12-15"), persona.fechaDeNacimiento());
    }

    @DisplayName("una Persona tiene edad")
    @Test
    void laEdadDeUnaPersona() {
        Persona persona = new Persona(null, LocalDate.now().minusYears(42));
        assertEquals(42, persona.edad());
    }
    
    @DisplayName("una Persona es menor que otra")
    @Test
    void unaPersonaEsMenorQue() {
        Persona persona = new Persona(null, LocalDate.parse("2000-01-31"));
        Persona otraPersona = new Persona(null, LocalDate.parse("2000-01-30"));
        assertTrue(persona.menorQue(otraPersona));
    }

    @DisplayName("una persona tiene constructor con parametros")
    @Test
    void constructoConParametros() {
        Persona persona = new Persona(null, null);
        assertNotNull(persona);
    }

    @DisplayName("una persona con nombre apellido y fecha de nacimiento")
    @Test
    void personaConApellido() {
        Persona persona = new Persona((String)null, (String)null, (LocalDate) null);
        assertNotNull(persona);
    }

    @DisplayName("una persona tiene apellido")
    @Test
    void tieneiApellido() {
        Persona persona = new Persona(null, "Apellido", null);
        assertEquals("Apellido", persona.apellido());
    }


}
