package unq.point;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

public class PointTest {

    Point p;

    @DisplayName("hay un punto en 0,0")
    @Test
    void test1() {
        p = new Point();
        assertNotNull(p);
        assertEquals(0, p.x(), "x no es = 0");
        assertEquals(0, p.y(), "y no es = 0");
        //assumingThat( p.x() == 0 , () -> assertEquals(0, p.y(), "y no es = 0"));
    }

    @DisplayName("hay un punto en 3,7")
    @Test
    void test2() {
        p = new Point(3,7);
        assertAll("punto",
            () -> assertEquals(3, p.x(), "x no es 3"),
            () -> assertEquals(7, p.y(), "y no es 7")
            );
    }

    @DisplayName("Un punto en 0,0 se mueve a 11,17")
    @Test
    void test3() {
        Point p = new Point();
        p.moveTo(11,17);
        assertAll("move point", 
                () -> assertEquals(11,p.x()),
                () -> assertEquals(17,p.y())
                );
    }

    @DisplayName("Suma de puntos")
    @Test
    void test4() {
        Point p = new Point(7,13);
        Point q = new Point(4,4);
        Point r = p.add(q);
        assertAll("add point",
                () -> assertEquals(11, r.x()),
                () -> assertEquals(17, r.y())
                );
    }
}
