package unq;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

public class Ejercicio4Test {

    Multioperador array;

    @BeforeEach
    void setUp() {
    }

    @DisplayName("Existe Multioperador")
    @Test
    void test1() {
        int[] arreglo = {};
        array = new Multioperador();
        assertNotNull(array);
    }

    @DisplayName("Suma del array = 0")
    @Test
    void test2() {
        array = new Multioperador();
        assertEquals(0, array.suma());
    }

    @DisplayName("Suma del array = 17")
    @Test
    void test3() {
        array = new Multioperador(new int[]{2,3,5,7});
        assertEquals(17, array.suma());
    }

    @DisplayName("Resta del array = -29")
    @Test
    void test4() {
        array = new Multioperador(new int[]{17,7,5});
        assertEquals((-29), array.resta());
    }

    @DisplayName("Multiplicacion del array = 120")
    @Test
    void test5() {
        array = new Multioperador(new int[]{1,2,3,4,5});
        assertEquals(120, array.mult());
    }
}
