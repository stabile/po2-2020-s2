package ar.edu.utn.frba.tadp.holamundo;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;

//import ar.edu.utn.frba.tadp.entes.Nombrable;
//import ar.edu.utn.frba.tadp.entes.Persona;
import ar.edu.utn.frba.tadp.entes.*;

public class HolaMundoTest {

    private static ByteArrayOutputStream outContent;
    private static ByteArrayOutputStream errContent;
    private static final PrintStream originalOut = System.out;
    private static final PrintStream originalErr = System.err;

    @BeforeEach
    void setUpStream() {
        outContent = new ByteArrayOutputStream();
        errContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent, true));
        System.setErr(new PrintStream(errContent, true));
    }
    
    @AfterEach
    void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }
    

    @Test
    void recepcionistaFormalDiceHola() {
        new RecepcionistaFormal().saludar(new Mundo());
        assertEquals("Buen día estimado Mundo\n", outContent.toString());
    }

    @Test
    void recepcionistaClasicoDiceHola() {
        new RecepcionistaClasico().saludar(new Mundo());
        assertEquals("hola Mundo\n", outContent.toString());
    }

    @Test
    void personaNombrableEsSaludadaPorRecepcionistaClasico() {
        Recepcionista recepcionista = new RecepcionistaClasico();
        Nombrable nombrable = new Persona("Jalio", null);
        recepcionista.saludar(nombrable);
        assertEquals("hola Jalio\n", outContent.toString());
    }

    @Test
    void mundoNombrableEsSaludadoPorRecepcionistaClasico() {
        Recepcionista recepcionista = new RecepcionistaClasico();
        Nombrable nombrable = new Mundo();
        recepcionista.saludar(nombrable);
        assertEquals("hola Mundo\n", outContent.toString());
    }

    @Test
    void personaNombrableEsSaludadaPorRecepcionistaFormal() {
        Recepcionista recepcionista = new RecepcionistaFormal();
        Nombrable nombrable = new Persona("Polimorfo", null);
        recepcionista.saludar(nombrable);
        assertEquals("Buen día estimado Polimorfo\n", outContent.toString());
    }

    @Test
    void mundoNombrableEsSaludadaPorRecepcionistaFormal() {
        Recepcionista recepcionista = new RecepcionistaFormal();
        Nombrable nombrable = new Mundo();
        recepcionista.saludar(nombrable);
        assertEquals("Buen día estimado Mundo\n", outContent.toString());
    }

    @Test
    void saludandoUnaCollectionDeNombrables() {
        Collection<Nombrable> nombrables = new ArrayList<Nombrable>();
        nombrables.add(new Mundo());
        nombrables.add(new Persona("Juan", null));
        nombrables.add(new Persona("Maria", null));
        for(Nombrable nombrable : nombrables) {
            new RecepcionistaClasico().saludar(nombrable);
        }
        assertEquals("hola Mundo\nhola Juan\nhola Maria\n", outContent.toString());
    }

    @Test
    void toStringDeDireccion() {
        Direccion direccion = new Direccion("Pasteur", 76);
        assertEquals("Pasteur 76", direccion.toString());
    }

    @Test
    void marcoViveEnMoreno57() {
        Direccion direccion1 = new Direccion("Alvear", 57);
        Direccion direccion2 = new Direccion("Moreno", 58);
        Direccion direccion3 = new Direccion("Moreno", 57);
        Direccion dirAnonym  = new Direccion (null, null) { };
        Persona persona = new Persona("Marco", direccion3);
        assertFalse(persona.viveEn(dirAnonym));
        assertFalse(persona.viveEn(direccion1));
        assertFalse(persona.viveEn(direccion2));
        assertTrue(persona.viveEn(direccion3));
    }

    @Test
    void dosSiSonIgualesTienenElMismoHashCode() {
        Direccion direccion1 = new Direccion("Alvear", 57);
        Direccion direccion2 = new Direccion("Alvear", 56);
        Direccion direccion3 = new Direccion("AlveaR", 57);
        assertNotEquals(direccion2.hashCode(), direccion1.hashCode());
        assertNotEquals(direccion2.hashCode(), direccion3.hashCode());
        assertNotEquals(direccion1.hashCode(), direccion3.hashCode());
        assertEquals(direccion1.hashCode(), direccion1.hashCode());
        assertEquals(direccion2.hashCode(), direccion2.hashCode());
        assertEquals(direccion3.hashCode(), direccion3.hashCode());
    }
}
