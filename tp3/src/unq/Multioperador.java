package unq;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

public class Multioperador {

    List<Integer> array;

    
    Multioperador() {
        array = new ArrayList<Integer>();
    }

    Multioperador(int[] enteros) {
        this();
        for ( int entero : enteros){
            add(entero);
        }
    }

    public Integer suma() { return array.stream()
                                .reduce(0, Integer::sum);
    }

    public Integer resta() { return array.stream()
                                .reduce(0, (r, n) -> (r - n));
    }

    public Integer mult() { return array.stream()
                                .reduce(1, (s, n) -> s * n);
    }

    private void add(Integer e) { array.add(e); }
}

