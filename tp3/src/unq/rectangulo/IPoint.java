package unq.rectangulo;

public interface IPoint {
    public int x();
    public int y();
}
