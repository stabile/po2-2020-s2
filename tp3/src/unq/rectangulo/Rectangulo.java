package unq.rectangulo;

public class Rectangulo {

    int ancho;
    int alto;

    private Rectangulo() {
        super();
    }

    public Rectangulo(IPoint origen, int ancho, int alto) {
        this();
        this.ancho = ancho;
        this.alto = alto;
    }

    private int alto() { return alto; }
    
    private int ancho() { return ancho; }

    public int area(){ return alto() * ancho(); }

    public int perimetro(){ return 2 * (alto() + ancho()) ; }

    public boolean horizontal() { return ancho() > alto(); }

    public boolean vertical() { return ancho() < alto(); }
}
