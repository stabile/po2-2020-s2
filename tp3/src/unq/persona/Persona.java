package unq.persona;

import java.time.LocalDate;
import java.time.Period;

public class Persona {

    private String nombre;
    private String apellido;
    private LocalDate fechaDeNacimiento;
    
    public Persona() {
        super();
    }

    public Persona(String nombre, LocalDate fechaDeNacimiento){
        this();
        nombre(nombre);
        fechaDeNacimiento(fechaDeNacimiento);
    }

    public Persona(String nombre, String apellido, LocalDate fechaDeNacimiento) {
        this(nombre, fechaDeNacimiento);
        apellido(apellido);
    }

    public String nombre() { return this.nombre; }

    private void nombre(String nombre) {
        this.nombre = nombre;
    }

    public String apellido() { return this.apellido; }

    private void apellido(String apellido) {
        this.apellido = apellido;
    }

    public LocalDate fechaDeNacimiento() { return this.fechaDeNacimiento; }

    private void fechaDeNacimiento(LocalDate fecha) {
        this.fechaDeNacimiento = fecha;
    }

    public int edad() {
        return Period.between(fechaDeNacimiento, LocalDate.now()).getYears();
    }

    public boolean menorQue(Persona otraPersona) {
        return fechaDeNacimiento().isAfter(otraPersona.fechaDeNacimiento());
    }
}

