package unq;

import java.util.ArrayList;
import java.util.function.Predicate;

public class Counter {

    ArrayList<Integer> numbers;

    class MultiploDe implements Predicate<Integer> {

        private Integer multiple;

        MultiploDe(Integer multiple){
            this.multiple = multiple;
        }

        @Override
        public boolean test(Integer n) {
            return n % multiple == 0;
        }

    }


    public Counter() {
        this.numbers = new ArrayList<Integer>();
    }

    public Long cantidadDePares() {
        return this.cantidadDeMultiplosDe(2);
    }

    public Long cantidadDeImpares() {
        return numbers().size() - this.cantidadDeMultiplosDe(2);
    }

    public Long cantidadDeMultiplosDe(Integer number) {
        return cantidadQueCumplen(new MultiploDe(number));
    }

    private Long cantidadQueCumplen(Predicate<Integer> predicado) {
        return numbers().stream()
                        .filter(n -> predicado.test(n))
                        .count();
    }

    public void addNumber(Integer number) {
        numbers().add(number);
    }

    ArrayList<Integer> numbers() { return this.numbers; }
}
