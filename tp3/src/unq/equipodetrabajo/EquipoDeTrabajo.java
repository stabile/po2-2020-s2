package unq.equipodetrabajo;

import java.math.BigDecimal;
import java.util.ArrayList;

public class EquipoDeTrabajo {

    private String nombre;
    private ArrayList<IPersona> integrantes;

    public EquipoDeTrabajo() {
        super();
        integrantes(new ArrayList<IPersona>());
    }

    public EquipoDeTrabajo(String nombre, ArrayList<IPersona> integrantes) {
        this();
        nombre(nombre);
        integrantes(integrantes);
    }

    public String nombre() { return this.nombre; }

    public void nombre(String nombre) { this.nombre = nombre; }

    public BigDecimal promedio() { 
        BigDecimal suma     = new BigDecimal("0");
        BigDecimal promedio = new BigDecimal("0");
        int cantidadDeIntegrantes = integrantes.size();

        if (cantidadDeIntegrantes != 0) {
            for (IPersona persona : integrantes){
                suma = persona.edad().add(suma);
            }
            promedio = suma.divide(BigDecimal.valueOf(cantidadDeIntegrantes));
        } 

        return promedio;
    }

    private void integrantes(ArrayList<IPersona> integrantes) {
        this.integrantes = integrantes;
    }
}
