package unq.equipodetrabajo;

import java.math.BigDecimal;

public interface IPersona {
    public BigDecimal edad();
}
