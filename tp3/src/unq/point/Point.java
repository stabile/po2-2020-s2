package unq.point;

public class Point {

    int x;
    int y;

    public Point() {
    }

    public Point(int _x, int _y){
        this();
        moveTo(_x, _y);
    }

    private void x(int _x) { this.x = _x;}

    private void y(int _y) { this.y = _y;}

    public int x() { return this.x; }

    public int y() { return this.y; }

    public void moveTo(int _x, int _y) {
        x(_x);
        y(_y);
    }

    public Point add(Point p) {
        return new Point(x() + p.x(), y() + p.y());
    }

}
