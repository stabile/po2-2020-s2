package ar.edu.utn.frba.tadp.holamundo;

import ar.edu.utn.frba.tadp.entes.Articulo;
import ar.edu.utn.frba.tadp.entes.Nombrable;

@Articulo("un")
public class Mundo implements Nombrable {

    public String nombre() {
        return "Mundo";
    }
}
