package ar.edu.utn.frba.tadp.holamundo;

import ar.edu.utn.frba.tadp.entes.Nombrable;

class RecepcionistaClasico extends Recepcionista {
    
    @Override
    protected String armarSaludo(Nombrable nombrable) {
        return "hola " + nombrable.nombre();
    }
}
