package ar.edu.utn.frba.tadp.holamundo;

import ar.edu.utn.frba.tadp.entes.Nombrable;
import ar.edu.utn.frba.tadp.entes.Articulo;

public abstract class Recepcionista {

    public void saludar(Nombrable nombrable) {
        //this.log(nombrable);
        System.out.println(this.armarSaludo(nombrable));
        log(nombrable);
    }

    private void log(Nombrable nombrable) {
        System.err.println("saludando a " + articulo(nombrable) + " " +
                nombrable.getClass().getSimpleName());
    }

    private String articulo(Nombrable nombrable) {
        return nombrable.getClass().getAnnotation(Articulo.class).value();
    }

    protected abstract String armarSaludo(Nombrable nombrable);
}
