package ar.edu.utn.frba.tadp.entes;

public class Direccion {
    
    private String calle;
    private Integer numero;

    public Direccion(String calle, Integer numero) {
        this.calle = calle;
        this.numero = numero;
    }

    @Override
    public String toString() { 
        // el Integer se convierte solito a String. Maravilloso!!!!! (Ironico)
        // ok ok ok. El operador + "resuelve" el tipo. Es solo eso.
        return this.calle + " " + this.numero;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Direccion) 
            && this.calle.equals(((Direccion) obj).calle)
            && this.numero.equals(((Direccion) obj).numero);
    }

    @Override
    public int hashCode() {
        return this.calle.hashCode() + this.numero;
    }
}
