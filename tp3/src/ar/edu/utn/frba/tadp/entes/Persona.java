package ar.edu.utn.frba.tadp.entes;

@Articulo("una")
public class Persona implements Nombrable {

    private String nombre;
    private Direccion direccion;

    public Persona() {
        super();
    }

    public Persona(String nombre, Direccion direccion) {
        this();
        nombre(nombre);
        direccion(direccion);
    }

    // interface Nombrable
    public String nombre() {
        return this.nombre;
    }

    private void nombre(String nombre) {
        this.nombre = nombre;
    }

    private Direccion direccion(){
        return this.direccion;
    }

    private void direccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public Boolean viveEn(Direccion direccion){
        return direccion().equals(direccion);
    }

}
