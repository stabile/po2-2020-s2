package siete;

import javax.swing.*;

public class ListModelAdapter<E> extends DefaultListModel<E> {

    private final ListaDePalabrasOrdenadas listaDePalabrasOrdenadasAdaptada;

    ListModelAdapter(ListaDePalabrasOrdenadas listaDePalabrasOrdenadas) {
        this.listaDePalabrasOrdenadasAdaptada = listaDePalabrasOrdenadas;
    }

    @Override
    public void addElement(E element) {
        this.listaDePalabrasOrdenadasAdaptada.agregarPalabra((String) element);
    }

    @Override
    public int size() {
        return this.listaDePalabrasOrdenadasAdaptada.cantidadDePalabras();
    }

    @Override
    public E elementAt(int index) {
        return (E) this.listaDePalabrasOrdenadasAdaptada.getPalabrasDePosicion(index);
    }

    @Override
    public int getSize() {
        return this.size();
    }

    @Override
    public E getElementAt(int index) {
        return this.elementAt(index);

    }
}