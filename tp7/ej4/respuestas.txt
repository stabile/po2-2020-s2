Ejercicio 4 - Llamadas telefónicas
A partir de las siguientes líneas de código identifique estos conceptos, si es posible:
● Template Method.
● Operaciones primitivas.
● Operaciones concretas.
● Hook Method.

public abstract class LlamadaTelefonica {
    private int tiempo;
    private int horaDelDia;

    public LlamadaTelefonica(int tiempo, int horaDelDia){
        this.tiempo = tiempo;
        this.horaDelDia = horaDelDia;
    }

    // Operación concreta.
    public int getTiempo(){
        return this.tiempo;
    }

    public int getHorasDelDia(){
        return this.horaDelDia;
    }

    // Operación primitiva
    public abstract boolean esHoraPico();


    // Template Method
    public float costoFinal(){
        if (this.esHoraPico()){
            return this.costoNeto()*1.2f*this.getTiempo();
        }else{
            return this.costoNeto() * this.getTiempo();
        }
    }

    // Hook method (puede ser sobreescrita)
    public float costoNeto(){
        return this.getTiempo()*1;
    }

}

public classLLamadaDescuento extends LlamadaTelefonica{
    public LlamadaTelefonica(int tiempo, int horaDelDia) {
        super(tiempo, horaDelDia()) {
    }

    // Operación primitiva (implementación)
    @Override
    public boolean esHoraPico() {
        return false;
    }

    @Override
    public float costoNeto() {
        return this.getTiempo() * 0.95f;
    }
}

