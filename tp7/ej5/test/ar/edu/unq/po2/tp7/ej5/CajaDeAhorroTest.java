package ar.edu.unq.po2.tp7.ej5;

import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

public class CajaDeAhorroTest {
    CuentaBancaria cda;
    
    @BeforeEach
    void setUp() {
        cda = new CajaDeAhorro("De Caja De Ahorro", 200.0);
    }


    @DisplayName("Hay caja de ahorro")
    @Test
    void testHayCajaDeAhorro() {
        assertEquals(CajaDeAhorro.class, cda.getClass());
        assertTrue(cda instanceof CuentaBancaria);
    }

    @DisplayName("Tiene limite")
    @Test
    void testLimite() {
        assertEquals(200, ((CajaDeAhorro)cda).getLimite());
    }


    @DisplayName("Puede extraer")
    @Test
    void testExtraer() {
        cda.setSaldo(100.0);
        cda.extraer(51.0);
        assertEquals(49.0, cda.getSaldo());
    }
}