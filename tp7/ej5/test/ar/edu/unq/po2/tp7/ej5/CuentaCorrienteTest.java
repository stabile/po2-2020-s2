package ar.edu.unq.po2.tp7.ej5;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CuentaCorrienteTest {

    CuentaBancaria cc;
    String titular;

    @BeforeEach
    void setUp() {
        titular = "Titular";
        cc = new CuentaCorriente(titular, 100.0);
    }


    @DisplayName("Hay cuenta corriente")
    @Test
    void testHay() {
        assertEquals(CuentaCorriente.class , cc.getClass());
    }

    @DisplayName("Tiene un titular")
    @Test
    void test1() {
        assertEquals("Titular" , cc.getTitular());
    }

    @DisplayName("Tiene un saldo")
    @Test
    void testTieneSaldo() {
        assertEquals(0, cc.getSaldo());
    }

    @DisplayName("Tiene un saldo especifico")
    @Test
    void testSaldoEspecifico() {
        cc.setSaldo(102.97);
        assertEquals(102.97, cc.getSaldo());
    }


    @DisplayName("Puede guardar un movimiento - lineas de codigo sin justificacion")
    @Test
    void testGuardaMovimiento() {
        List<String> movimientos = new ArrayList<String>();
        CuentaBancaria ccs = new CuentaCorriente(movimientos);
        ccs.agregarMovimiento("Movimiento");
        assertEquals("Movimiento", movimientos.get(movimientos.size()-1));
    }

    @DisplayName("Puedo extraer")
    @Test
    void testExtraer() {
        cc.setSaldo(192.0);
        cc.extraer(92.0);

        assertEquals(100.0, cc.getSaldo());
    }

    


    


}

