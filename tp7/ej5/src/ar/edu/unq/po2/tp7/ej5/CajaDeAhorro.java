package ar.edu.unq.po2.tp7.ej5;

public class CajaDeAhorro extends CuentaBancaria{

    private double limite;

    public CajaDeAhorro(String titular, double limite) {
        super(titular);
        this.limite = limite;
    }

    boolean esPosibleExtraer(double monto) {
        return (this.getSaldo() >= monto && this.getLimite() >= monto);
    }

    public double getLimite() { return this.limite; }
}
