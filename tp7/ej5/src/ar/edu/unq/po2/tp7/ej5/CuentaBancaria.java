package ar.edu.unq.po2.tp7.ej5;

import java.util.*;

public abstract class CuentaBancaria {

    private String titular;
    private double saldo;
    private List<String> movimientos;

    public CuentaBancaria(String titular) {
        super();
        this.titular = titular;
        this.movimientos = new ArrayList<String>();
    }

    CuentaBancaria(List<String> movimientos) {
        this("Stub");
        this.movimientos = movimientos;
    }

    public String getTitular() { return this.titular; }

    public double getSaldo() { return this.saldo; }

    public void setSaldo(double valor) { 
        this.saldo = valor;
    }

    public void agregarMovimiento(String movimiento) {
        this.movimientos.add(movimiento);
    }

    public void extraer(double monto) {
        if (this.esPosibleExtraer(monto)) {
            this.setSaldo(this.getSaldo() - monto);
            this.agregarMovimiento("Extraccion");
        }
    }


    abstract boolean esPosibleExtraer(double monto); 

}
