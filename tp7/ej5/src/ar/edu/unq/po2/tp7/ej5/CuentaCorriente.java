package ar.edu.unq.po2.tp7.ej5;

import java.util.*;

public class CuentaCorriente extends CuentaBancaria {
    
    private double descubierto;

    public CuentaCorriente(String titular, double descubierto) {
        super(titular);
        this.descubierto = descubierto;
    }

    public CuentaCorriente(List<String> movimientos) {
        super(movimientos);
    }

    public double getDescubierto() {
        return this.descubierto;
    }

    boolean esPosibleExtraer(double monto) {
        return (this.getSaldo() + this.getDescubierto() >= monto);
    }
}
