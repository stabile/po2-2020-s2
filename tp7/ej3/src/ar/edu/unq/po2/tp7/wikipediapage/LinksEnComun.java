package ar.edu.unq.po2.tp7.wikipediapage;

import java.util.List;

public class LinksEnComun extends Filtro {

    public LinksEnComun() {
    }

    public boolean predicado(WikipediaPage w1, WikipediaPage w2) { 
        return w1.getLinks().stream().anyMatch(wpe -> w2.getLinks().contains(wpe)); }
}
