package ar.edu.unq.po2.tp7.wikipediapage;

import java.util.List;
import java.util.Arrays;
import java.util.stream.Collectors;

public abstract class Filtro {

    public Filtro() {
    }

    public abstract boolean predicado(WikipediaPage w1, WikipediaPage w2);

    public List<WikipediaPage> getSimilarPages(WikipediaPage wp, List<WikipediaPage> wps) {
        return wps.stream().filter(wpe -> predicado(wpe, wp)).collect(Collectors.toList());
    }
}
