package ar.edu.unq.po2.tp7.wikipediapage;

public class MismaLetraInicial extends Filtro{

    public MismaLetraInicial() {
    }

    public boolean predicado(WikipediaPage wp1, WikipediaPage wp2) { 
        return wp1.getTitle().charAt(0) == wp2.getTitle().charAt(0);
    }
}
