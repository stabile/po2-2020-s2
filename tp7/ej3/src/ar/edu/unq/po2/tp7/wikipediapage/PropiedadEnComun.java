package ar.edu.unq.po2.tp7.wikipediapage;

public class PropiedadEnComun extends Filtro {

    public PropiedadEnComun() {
    }

    public boolean predicado(WikipediaPage w1, WikipediaPage w2){
        return w1.getInfoBox().keySet().stream().anyMatch(wpe -> w2.getInfoBox().keySet().contains(wpe));
    }
}
