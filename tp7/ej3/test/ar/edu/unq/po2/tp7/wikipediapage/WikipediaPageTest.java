package ar.edu.unq.po2.tp7.wikipediapage;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

interface Imaginaria { 
    public void llamado(); 
    public int cantidad();
    public int saldoDeNeuronas();
}

public class WikipediaPageTest {
    WikipediaPage pagina1 = new WikiPagina();
    WikipediaPage pagina2 = new WikiPagina();
    WikipediaPage pagina3 = new WikiPagina();
    WikipediaPage pagina4 = new WikiPagina();
    WikipediaPage pagina5 = new WikiPagina();
    WikipediaPage pagina6 = new WikiPagina();

    class WikiPagina implements WikipediaPage {
        public String getTitle() { return "Inline Mock"; }
        public List<WikipediaPage> getLinks() { return Arrays.asList(
                pagina1, pagina2, pagina3
                );
        }
        public Map<String, WikipediaPage> getInfoBox() {
            Map<String, WikipediaPage> wpmap1 = new HashMap<String, WikipediaPage>();
            wpmap1.put("1", new WikiPagina());
            wpmap1.put("2", new WikiPagina());
            wpmap1.put("3", new WikiPagina());
            return wpmap1;
        }
    }

    WikipediaPage wpp;
    WikipediaPage wp1;
    WikipediaPage wp2;
    WikipediaPage wp3;
    WikipediaPage wp4;
    WikipediaPage wp5;
    WikipediaPage wp6;
    List<WikipediaPage> wplinks123;
    List<WikipediaPage> wplinks456;
    Map<String, WikipediaPage> wpmap0;
    Map<String, WikipediaPage> wpmap1;
    Map<String, WikipediaPage> wpmap2;
    Filtro filtroMismaLetraInicial ;
    Filtro filtroLinksEnComun;
    Filtro filtroPropiedadEnComun;
    WikipediaPage wpagina;
    WikipediaPage wpSiSimil0;
    WikipediaPage wpSiSimil1;
    WikipediaPage wpSiSimil2;
    WikipediaPage wpNoSimil1;
    WikipediaPage wpNoSimil2;
    WikipediaPage wprara;

    @BeforeEach
    void setUp() {
        wpp = mock(WikipediaPage.class);
        wplinks123 = Arrays.asList(wp1, wp2, wp3);
        wplinks456 = Arrays.asList(wp4, wp5, wp6);

        wpmap0 = makeMap();
        wpmap0.put("About", wp1);
        wpmap1 = makeMap();
        wpmap1.put("11", new WikiPagina());
        wpmap1.put("12", new WikiPagina());
        wpmap1.put("13", new WikiPagina());
        wpmap2 = makeMap();
        wpmap2.put("Libros", wp4);
        wpmap2.put("Referencias", wp5);
        wpmap2.put("About", wp6);

        filtroMismaLetraInicial = new MismaLetraInicial();
        filtroLinksEnComun = new LinksEnComun();
        filtroPropiedadEnComun = spy(PropiedadEnComun.class);
        wpagina    = new WikiPagina();
        wprara     = paginaCon("Pagina"  , wplinks456, wpmap1);
        wpSiSimil0 = paginaCon("Zero"    , wplinks456, wpmap1);
        wpSiSimil1 = paginaCon("I Method", wplinks123, wpmap1);
        wpSiSimil2 = paginaCon("I To"    , wplinks123, wpmap1);
        wpNoSimil1 = paginaCon("@@@@"    , wplinks123, wpmap2);
        wpNoSimil2 = paginaCon("%%%%"    , wplinks123, wpmap2);
    }

    Map<String, WikipediaPage> makeMap() { return new HashMap<String, WikipediaPage>(); }


    WikipediaPage paginaCon(String titulo, List<WikipediaPage> lista, Map<String, WikipediaPage> mapa) { 
        return new WikiPagina() { public String getTitle() { return titulo; }
                                  public List<WikipediaPage> getLinks() { return lista; }
                                  public Map<String, WikipediaPage> getInfoBox() { return mapa; } };
    }

    @DisplayName("Existe un mock WikipediaPage ")
    @Test
    void test1() {
        assertNotNull(wpp);
    }


    @DisplayName("Una wpp tiene el titulo Mock")
    @Test
    void test2() {
        when(wpp.getTitle()).thenReturn("Mock");
        assertEquals("Mock", wpp.getTitle());
    }


    @DisplayName("wpp se conecta con 3 paginas")
    @Test
    void test3() {
        when(wpp.getLinks()).thenReturn(wplinks123);
        assertEquals(3, wplinks123.size());
    }

    @DisplayName("Map containsKey")
    @Test
    void test4() {
        when(wpp.getInfoBox()).thenReturn(wpmap1);
        assertTrue(wpp.getInfoBox().containsKey("12"));
        assertTrue((new WikiPagina()).getInfoBox().containsKey("3"));
    }

    @DisplayName("Dos wp tienen la misma letra inicial")
    @Test
    void test5() {
        when(wpp.getTitle()).thenReturn("Initial Title Mock");
        assertEquals('I' , wpagina.getTitle().charAt(0));
        assertTrue(filtroMismaLetraInicial.predicado(wpp, wpagina));
    }

    @DisplayName("Dos wp no tienen la misma letra inicial")
    @Test
    void testNoTienenLaMismaLetraInicial() {
        when(wpp.getTitle()).thenReturn("Diferente");
        assertEquals('I', wpagina.getTitle().charAt(0));
        assertFalse(filtroMismaLetraInicial.predicado(wpp, wpagina));
    }

// * MismaLetraInicial retorna como páginas similares aquellas que poseen la misma primera letra en el comienzo del título, por ejemplo “La Plata” es similar con “Lucas Art” y “Lobo”.
//
    @DisplayName("Cuando buscamos paginas similares en una lista vacia obtenemos una lista vacía")
    @Test
    void testMismaLetraInicialEnUnaListaVacia() {
        WikipediaPage wpagina = new WikiPagina() { public String getTitle() { return "Inner Inner Method"; } };
        List<WikipediaPage> vacia = Arrays.asList();
        assertEquals(0, filtroMismaLetraInicial.getSimilarPages(wpagina, vacia).size());
    }

    @DisplayName("Cuando buscamos paginas similares en una lista que tiene nuestra pagina, obtenemos una lista con nuestra pagina")
    @Test
    void testMismaLetraInicialEnUnaListaConUnaPaginaConLaMismaLetraInicial() {
        List<WikipediaPage> paginasDeUna = Arrays.asList(wpSiSimil1);
        assertEquals(wpSiSimil1, filtroMismaLetraInicial.getSimilarPages(wpagina, paginasDeUna).get(0));
    }



    @DisplayName("Cuando en una lista de tres paginas hay solo una similar obtenemos esa una")
    @Test
    void testUnaSimilDeTres() {
        List<WikipediaPage> paginasDeTres = Arrays.asList(wpNoSimil1, wpSiSimil1, wpNoSimil2);
        List<WikipediaPage> listaResultado = filtroMismaLetraInicial.getSimilarPages(wpagina, paginasDeTres);
        assertEquals(wpSiSimil1, listaResultado.get(0));
        assertEquals(1, listaResultado.size());
    }

    @DisplayName("Cuando en una lista de cuatro paginas hay dos similares obtenemos esas dos")
    @Test
    void testDosSimilares() {
        List<WikipediaPage> paginasDeCuatro = Arrays.asList(wpNoSimil1, wpSiSimil1, wpNoSimil2, wpSiSimil2);
        List<WikipediaPage> listaResultado = filtroMismaLetraInicial.getSimilarPages(wpagina, paginasDeCuatro);
        assertTrue(listaResultado.contains(wpSiSimil1));
        assertTrue(listaResultado.contains(wpSiSimil2));
        assertEquals(2, listaResultado.size());
    }

    @DisplayName("Cuando se pide links en comun a una lista vacía se obtiene una lista vacía")
    @Test
    void testLinksComunListaVacía() {
        assertEquals(0, filtroLinksEnComun.getSimilarPages(wpagina, Arrays.asList()).size());
    }

    @DisplayName("Cuando se pide links en comun y hay uno en una lista de uno se obtiene este")
    @Test
    void testUnLinkEnSingleton() {
        assertEquals(1, filtroLinksEnComun.getSimilarPages(wpagina, Arrays.asList(wpagina)).size());
    }

    @DisplayName("Cuando se pide links en comun y no hay en una lista de uno se obtiene nada")
    @Test
    void testNadaEnSingleton() {
        assertEquals(0, filtroLinksEnComun.getSimilarPages(wpagina, Arrays.asList(wprara)).size());
    }
   
    @DisplayName("cantidad y llamado de Imaginaria son usados y verificados")
    @Test
    void testLLamadoEsRealizado() {
        Imaginaria imaginacion = spy(Imaginaria.class);
        doReturn(10).when(imaginacion).cantidad();
        imaginacion.llamado();
        verify(imaginacion).llamado();
        assertEquals(10, imaginacion.cantidad());
        when(imaginacion.saldoDeNeuronas()).thenReturn(27);
        assertEquals(27, imaginacion.saldoDeNeuronas());
    }

    @DisplayName("Cuando pag con propi en comun en una pagina con 1 propiedad y un dupleton de pag. se llama a predicado dos veces")
    @Test
    void testPredicadoEsLLamadoDosVeces() {
        List<WikipediaPage> resultado = filtroPropiedadEnComun.getSimilarPages(wpagina, Arrays.asList(wpagina, wprara));
        verify(filtroPropiedadEnComun, times(2)).predicado(any(), any());
    }

    @DisplayName("Cuando se llama al predicado de PropiedadEnComun con dos paginas que no la tienen indica falso")
    @Test
    void testPredicadoDePropiedadEnComunFalso() {
        assertFalse(filtroPropiedadEnComun.predicado(wpSiSimil0, wpNoSimil1));
    }

    @DisplayName("Cuando dos listas contienen propiedades en comun predicado es verdadero")
    @Test
    void testPredicadoDePropiedadEsVerdad() {
        assertTrue(filtroPropiedadEnComun.predicado(wpSiSimil0, wpSiSimil1));
    }

    @DisplayName("Prueba MAP")
    @Test
    void testMap() {
        Map<String, Integer> map = new HashMap<>();
        map.put("B", 10);
        map.put("Z", 42);
        map.put("C", 23);
        assertNull(map.get("A"));
        assertNotNull(map.get("Z"));
        assertEquals(23, map.get("C"));
        assertTrue(map.containsKey("C"));
        assertTrue(map.keySet().contains("C"));
        assertFalse(map.keySet().contains("D"));
    }
}


/* *
 * Es necesario programar un filtro para que a partir de una colección de páginas de
 * Wikipedia retorne aquellas que sean similares a una particular. Para ello es necesario
 * implementar una jerarquía de clases filtros en las que se pueda definir la similitud en
 * función de diferentes heurísticas. Las heurísticas están definidas en función a atributos
 * que poseen las páginas de Wikipedia. Para esto, una página de Wikipedia cuenta con la
 * interfaz WikipediaPage que posee el siguiente protocolo:
     * String getTitle(); /*retorna el título de la página.
     * List<WikipediaPage> getLinks(); //retorna una Lista de las páginas de Wikipedia con las que se conecta.
     * 
     * Map<String, WikipediaPage> getInfobox(); // *retorna un Map con un valor en texto y la pagina que describe ese valor que aparecen en los infobox de la página de Wikipedia.
 * 
 * En esta oportunidad ud debe implementar tres tipos de filtros: MismaLetraInicial,
 * LinkEnComun, PropiedadEnComun. La forma en que funcionan las clases filtros es
 * mediante el método
 * public List<WikipediaPage> getSimilarPages(WikipediaPage page,
 * List<WikipediaPage) wikipedia)
 * La lógica de cada filtro es la siguiente:
 * MismaLetraInicial retorna como páginas similares aquellas que poseen la misma primera letra en el comienzo del título, por ejemplo “La Plata” es similar con “Lucas Art” y “Lobo”.
 * LinkEnComun retorna como páginas similares aquellas que posean al menos un link a una página en común, por ejemplo si la página de “Gimnasia y Esgrima La Plata” tiene un link a la página “La Plata” y la página “Buenos Aires” tiene un link a “La Plata” esas páginas serian similares.
 * PropiedadEnComun , retorna aquellas páginas que poseen alguna propiedad del infobox en común, por ejemplo si la página de una persona tiene la propiedad “birth_place” y otra página posee también la propiedad “birth_place” serian similares mutuamente. En este caso, no importa que el valor de la propiedad sea diferente.
 * 1. Realice un diagrama de clases que modele el problema planteado.
 * 2. Realice un diagrama de secuencia para el caso de usar un filtro de tipo MismaLetraInicial para obtener las paginas similares a “Bernal” asumiendo que Wikipedia posee como paginas “Bernal”, “Quilmes”, “Buenos Aires”
 * 3. Programe el test de unidad utilizando JUnit para las tres clases involucradas.
 * 4. Programa la funcionalidad completa en Java.  
 *  */
