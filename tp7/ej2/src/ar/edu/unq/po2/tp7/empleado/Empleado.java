package ar.edu.unq.po2.tp7.empleado;

public abstract class Empleado {

    private double sueldoBasico = 0;

    private int horasTrabajadas = 0;

    private boolean tieneFamilia = false;
    
    public Empleado() {
    }
    
    private double sueldoBruto() { return sueldoBasico() 
                                  + sueldoPorHorasTrabajadas()
                                  + bonusPorFamilia()
                                  ; }

    public double sueldo() { return sueldoBruto() 
                                  - descuentos()
                                  ; }

    protected double sueldoBasico() { return getSueldoBasico(); } // abstracta o hook? hook. no todos los empleados tienen sueldo basico

    protected int horasTrabajadas() { return getHorasTrabajadas(); } // abstracta o hook? hook. no todos los empleados cobran por horas

    protected double valorPorHoraTrabajada() { return 0; } // abstracta o hook ? hook. no todos los empleados cobran por horas

    protected double bonusPorFamilia() { return 0; } // abstracta o hook? hook. no todos los empleados tienen bonus por familia

    private double descuentos() { return sueldoBruto() * 0.13; }

    protected void setSueldoBasico(double basico) { this.sueldoBasico = basico; }

    protected double getSueldoBasico() { return this.sueldoBasico; }

    protected void setHorasTrabajadas(int horas) { this.horasTrabajadas = horas; }

    protected int getHorasTrabajadas() { return this.horasTrabajadas; }

    private double sueldoPorHorasTrabajadas() { return horasTrabajadas() * valorPorHoraTrabajada() ;}
    
    public Empleado conFamilia() { this.tieneFamilia = true; return this;}
    
    public Empleado sinFamilia() { this.tieneFamilia = false; return this;}

    protected boolean tieneFamilia() { return this.tieneFamilia; }

}
