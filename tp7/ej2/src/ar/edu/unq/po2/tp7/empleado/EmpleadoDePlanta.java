package ar.edu.unq.po2.tp7.empleado;

public class EmpleadoDePlanta extends Empleado {

    private final double SUELDO_BASICO = 3000;
    private final double BONUS_POR_HIJO = 150;

    private int cantidadDeHijos;

    public EmpleadoDePlanta(int cantidadDeHijos) {
        super();
        super.setSueldoBasico(SUELDO_BASICO);
        setCantidadDeHijos(cantidadDeHijos);
    }

    @Override
    protected double bonusPorFamilia() { return BONUS_POR_HIJO * cantidadDeHijos(); }

    private int cantidadDeHijos() { return this.cantidadDeHijos; }

    private void setCantidadDeHijos(int cantidad) { this.cantidadDeHijos = cantidad; }

}
