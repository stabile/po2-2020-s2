package ar.edu.unq.po2.tp7.empleado;

public class EmpleadoPasante extends Empleado{

    private final double VALOR_POR_HORA_TRABAJADA = 40;

    public EmpleadoPasante(int horas) {
        super();
        super.setHorasTrabajadas(horas);
    }

    @Override
    protected double valorPorHoraTrabajada() { return VALOR_POR_HORA_TRABAJADA; }

}
