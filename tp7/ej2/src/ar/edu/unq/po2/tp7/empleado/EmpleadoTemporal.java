package ar.edu.unq.po2.tp7.empleado;

public class EmpleadoTemporal extends Empleado {

    private final double SUELDO_BASICO = 1000;
    private final double BONUS_POR_HIJOS_OY_CONYUGE = 100;
    private final double VALOR_POR_HORA_TRABAJADA = 5;

    public EmpleadoTemporal(int horas) {
        super();
        super.setSueldoBasico(SUELDO_BASICO);
        super.setHorasTrabajadas(horas);
    }

    @Override
    protected double valorPorHoraTrabajada() { return VALOR_POR_HORA_TRABAJADA; }

    @Override
    protected double bonusPorFamilia() { return super.tieneFamilia() ? BONUS_POR_HIJOS_OY_CONYUGE : 0; }

}
