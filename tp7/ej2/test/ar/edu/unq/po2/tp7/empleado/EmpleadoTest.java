package ar.edu.unq.po2.tp7.empleado;

/*
 *
 * Ejercicio 2 - Sueldos recargado
 * 
 * Una empresa realiza periódicamente los pagos de sueldos y cargas de cada uno de sus
 * empleados. En la empresa los empleados están organizados en tres tipos: Temporarios,
 * Pasantes y de Planta. 
 * El sueldo de un empleado Temporario está determinado por el
 * pago de $5 por hora que trabajó más el sueldo básico que es de $1000, además se le
 * paga $100 si posee hijos y/o está casado. 
 *
 * A los Pasantes se les paga $40 las horas trabajadas en el mes. 
 *
 * Por último a los empleados de Planta se les paga un sueldo básico
 * de $ 3000 y un plus por cada hijo que posea de $ 150 cada hijo. 
 *
 * Por otro lado, de cada sueldo se deben descontar en conceptos de aportes y 
 * obra social un 13 % del sueldo.
 * Modele la jerarquía de Empleados de forma tal que cualquier empleado puede
 * responder al mensaje #sueldo. Utilice template method para su solución e indique los
 * roles participantes.
 */

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmpleadoTest {

    @Nested
    @DisplayName("Empleado Temporal")
    class Temporal {
    
        Empleado empleadoTemporal;

        @BeforeEach
        void setUp() {
        }

        @DisplayName("Cuando un empleado Temporal trabaja cero hora cobra 870.0")
        @Test
        void test1000DeBasico() {
            empleadoTemporal = new EmpleadoTemporal(0);
            assertEquals(870.0 , empleadoTemporal.sueldo());
        }

        @DisplayName("Cuando un empleado temporal sinFamilia trabaja 100 horas cobra 1305")
        @Test
        void testTemporalCobra1500() {
            Empleado empleadoTemporal = new EmpleadoTemporal(100).sinFamilia();
            assertEquals(1305, empleadoTemporal.sueldo());
        }

        @DisplayName("Cuando el empleado temporal trabaja cero horas y tiene familia cobra 957")
        @Test
        void testBasicoMasBonusConFamilia(){
            Empleado empleadoTemporal = new EmpleadoTemporal(0).conFamilia();
            assertEquals(957.0, empleadoTemporal.sueldo());
        }

        @DisplayName("Cuando un empleado temporal trabaja 50 horas y tiene familia cobra 1174.5 ")
        @Test
        void test50HorasConFamilia() {
            Empleado empleadoTemporal = new EmpleadoTemporal(50).conFamilia();
            assertEquals(1174.5, empleadoTemporal.sueldo());
        }
    }

    @Nested
    @DisplayName("Pasante")
    class Pasante {

        @DisplayName("Cuando un empleado pasante trabaja cero horas tiene sueldo cero")
        @Test
        void testPasanteSueldoCero() {
            Empleado empleadoPasante = new EmpleadoPasante(0);
            assertEquals(0, empleadoPasante.sueldo());
        }

        @DisplayName("Cuando un empleado pasante trabaja 10 horas en el mes y cobra 348.0")
        @Test
        void testPasante10Horas() {
            assertEquals(348.0, (new EmpleadoPasante(10)).sueldo());
        }
    }

    @Nested
    @DisplayName("De Planta")
    class DePlanta {
        @DisplayName("Cuando empleado de planta no tiene hijos cobra 2610.0")
        @Test
        void testDePlantaCeroHoras() {
            Empleado empleadoDePlanta = new EmpleadoDePlanta(0);
            assertEquals(2610.0, empleadoDePlanta.sueldo());
        }

        @DisplayName("Cuando un empleado de planta tiene 3 hijos cobra 3001.5")
        @Test
        void testDePlantaConTresHijos() {
            Empleado empleadoDePlanta = new EmpleadoDePlanta(3);
            assertEquals(3001.5, empleadoDePlanta.sueldo());
        }
    }
}
