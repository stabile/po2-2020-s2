package cuatro;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.*;


class AdapterTest {

    Adapter adapter;


    @Nested
    class EnumerationAndVector {

        @DisplayName("Test about Vector.this syntax in Vector's elements()")
        @Test
        void testEnumerationInterface() {

            class OutterClass {
                private int local = 123;
                class InnerClass {
                    private int local = 987;
                    public int innerLocalPlusOutterLocal() {
                        return this.local + OutterClass.this.local;
                    }
                }
                public InnerClass innerInstance() {
                    return new InnerClass(){};
                }
            }
            int prueba = new OutterClass().innerInstance().innerLocalPlusOutterLocal();

            assertEquals(123+987, prueba);
        }

        @DisplayName("Enumerate vector elements")
        @Test
        void test2() {
            Vector<Integer> vectorEnteros = new Vector<Integer>();
            vectorEnteros.add(123);
            vectorEnteros.add(456);
            vectorEnteros.add(789);
            vectorEnteros.add(1789);
            vectorEnteros.add(0);
            for (Enumeration<Integer> e = vectorEnteros.elements(); e.hasMoreElements(); ) {
                int a = e.nextElement();
                assertTrue(a < 1999, "" + a + " no es menor que 1000");
            }

        }

        class ArrayListEnum<E> extends ArrayList<E> implements Enumeration<E> {
            private Iterator<E> it;

            public boolean hasMoreElements() {
                                                   return it.hasNext();
                                                                       }

            public E nextElement() {
                                         return it.next();
                                                          }

            public void setItr() {
                                       it = this.iterator();
                                                            }
        }

        @DisplayName("Add to enArray and iterate")
        @Test
        void addEnArray() {
            ArrayListEnum<Integer> enArray = new ArrayListEnum<Integer>();

            enArray.add(42);
            enArray.add(17);
            enArray.setItr();
            assertEquals(42, enArray.nextElement());
            assertEquals(17, enArray.nextElement());

            enArray.setItr();
            assertTrue(enArray.hasMoreElements());
            while (enArray.hasMoreElements()) {
                Integer elemento = enArray.nextElement();
                assertTrue(elemento == 42 || elemento == 17);
            }
        }

    }

    class Ventana {
        public String display = "";

        public void visualizar(List<String> listaDePalabras) {
            for (String palabra : listaDePalabras)
                display += palabra;
        }

        ;
    }

    @DisplayName("hay clase Ventana")
    @Test
    void hayVentana() {
        Ventana ventana = new Ventana();
        assertEquals(Ventana.class, ventana.getClass());
    }

    @DisplayName("la ventana puede vizualizar una lista de palabras")
    @Test
    void visualizaLista() {
        Ventana ventana = new Ventana();
        ventana.visualizar(Arrays.asList("Objeto", "Diseño", "Patron"));

        assertEquals("ObjetoDiseñoPatron", ventana.display);
    }

    class ListaDePalabrasOrdenadas<String> {
        List lista = new ArrayList<String>();

        public void add(String palabra) {
            lista.add(palabra);
            Collections.sort(lista);
        }

        public boolean contains(String palabra) {
            return lista.contains(palabra);
        }
    }

    @DisplayName("hay Lista De Palabras Ordenadas")
    @Test
    void hayListaDePalabrasOrdenadas() {
        ListaDePalabrasOrdenadas<String> ldpo = new ListaDePalabrasOrdenadas<String>();

        assertEquals(ListaDePalabrasOrdenadas.class, ldpo.getClass());
    }

    @DisplayName("agrego palabras a la lista ordenada")
    @Test
    void agregoPalabrasAListaDePalabrasOrdenadas() {
        ListaDePalabrasOrdenadas<String> ldpo = new ListaDePalabrasOrdenadas<String>();
        assertFalse(ldpo.contains("Mockito"));
        ldpo.add("Mockito");
        assertTrue(ldpo.contains("Mockito"));
    }

    @Nested
    class Ejercicio_6_6Test {
        /*
        Ej 6.6. Implemente un Adaptador, que adapte un Iterator a un Enumeration.
        Escribir código que utilice dicha implementación adaptando un ArrayList.
         */

        Enumeration<Integer> enumeration;
        ArrayList<Integer> arrayList;
        Iterator<Integer> iterator;
        List<Integer> l0 = Arrays.asList();
        List<Integer> l1 = Arrays.asList(123);
        List<Integer> l2 = Arrays.asList(123, 987);
        @BeforeEach
        void setUp() {
        }

        void makeEnumeration(List<Integer> list) {
            arrayList =new ArrayList<Integer>(list);
            iterator = arrayList.iterator();
            enumeration = new Adapter(iterator);
        }


        @DisplayName("Hay Adapter")
        @Test
        void testIterator() {
            makeEnumeration(l0);
            assertEquals(Adapter.class, enumeration.getClass());
        }

        @DisplayName("Enumeration dont hasMoreElements")
        @Test
        void testSinElementos() {
            makeEnumeration(l0);
            assertFalse(enumeration.hasMoreElements());
        }

        @DisplayName("Enumeration hasMoreElements: one element")
        @Test
        void testUnElemento() {
            makeEnumeration(l1);
            assertTrue(enumeration.hasMoreElements());
            arrayList.remove(0);
            assertFalse(enumeration.hasMoreElements());
        }

        @DisplayName("Enumeration hasMoreElements: two elements")
        @Test
        void testDosElementos() {
            makeEnumeration(l2);
            assertTrue(enumeration.hasMoreElements());
            assertEquals(2, arrayList.size());
            arrayList.remove(0);
            arrayList.remove(0);
            assertFalse(enumeration.hasMoreElements());
            assertEquals(0, arrayList.size());

        }

        @DisplayName("Hay dos nextElement 123 y 987")
        @Test
        void testHayDosElementos() {
            makeEnumeration(l2);
            assertEquals(123, enumeration.nextElement());
            assertEquals(987, enumeration.nextElement());
            assertFalse(enumeration.hasMoreElements());
        }

        @DisplayName("Hay un nextElement y es el 123")
        @Test
        void testHayElementoYEs123() {
            makeEnumeration(l1);
            assertEquals(1, arrayList.size());
            assertEquals(123, enumeration.nextElement());
            assertFalse(enumeration.hasMoreElements());
        }
    }
}