package cuatro;

import java.util.Enumeration;
import java.util.Iterator;

public class Adapter implements Enumeration {


    private final Iterator<Integer> iterator;

    public Adapter(Iterator<Integer> iterator) {
        this.iterator = iterator;
    }

    @Override
    public boolean hasMoreElements() {
        return iterator.hasNext();
    }

    @Override
    public Object nextElement() {
        return iterator.next();
    }
}
