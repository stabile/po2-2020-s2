Ejercicio 6 – Contestar y justificar las respuestas
1. Existe más de un tipo de adaptadores, mencione y explique cada uno de ellos.

    Object adapter
        El Adaptador compone al Adaptado e implementa, o hereda, la interfaz
        del Target, mediante esta interfaz implementa mensajes que luego
        delegara en el objeto compuesto del Adaptado, adaptando el mensaje.

    Class adapter
        El Adaptado es super clase del Adaptador. Así el Adaptador tiene a
        disposición la interfaz heredada del Adaptado y a la vez hereda de Target.

2. ¿En qué se diferencia un tipo de adaptador del otro?

    El Object adapter compone una instancia de la clase a adaptar mientras que
    el Class adapter hereda de la clase a adaptar.


3. ¿Se pueden utilizar ambas alternativas de implementación del patrón en java?
¿Justifique la respuesta?

   Solo se puede usar el Object adapter. Java carece de herencia múltiple.


4. Ver la interface Enumeration de java y la clase Vector, preste atención a que
   dicha clase contiene un método "elements()". Explique cómo funciona el patrón
   adapter y cuáles son los roles de los participantes entre la interface
   y clase mencionada. Mencione qué tipo de implementación del patrón se utiliza.

   Aquí el método elements() crea una instancia de una subclase
   (anonima ) de Enumeration que oficia de Adapter. Siendo el método elements()
   parte de la implementación de Vector (Vector.this es composición en

    public Enumeration<E> elements() {
        return new Enumeration<E>() {
            int count = 0;

            public boolean hasMoreElements() {
                return this.count < Vector.this.elementCount;
            }

            public E nextElement() {
                synchronized(Vector.this) {
                    if (this.count < Vector.this.elementCount) {
                        return Vector.this.elementData(this.count++);
                    }
                }

                throw new NoSuchElementException("Vector Enumeration");
            }
        };
    }
   )

   En este caso el Target es la subclase anónima de la interface Enumeration,
   el Adapter es una clase anónima (Subclase de Enumeration), y el Adaptee es
   una instancia (la actual, la compuesta internamente) de Vector.

   La implementación del patron utilizada es: Object adapter.

5. Realice el mismo análisis del punto 3, pero con la interface Iterator, la clase
   ArrayList (preste atención al método "iterator()"). Muestre un ejemplo de
   funcionamiento y especifique los roles de cada participante.

/home/a/Objetos2/po2-2020-s2/tp7/ej6/respuesta.drawio

6. Implemente un Adaptador, que adapte un Iterator a un Enumeration. Escribir
código que utilice dicha implementación adaptando un ArrayList.

en tp7/ej6/test/cuatro/AdapterTest.java$Ejercicio6_6Test

public class Adapter implements Enumeration {

    private final Iterator<Integer> iterator;

    public Adapter(Iterator<Integer> iterator) {
        this.iterator = iterator;
    }

    @Override
    public boolean hasMoreElements() {
        return iterator.hasNext();
    }

    @Override
    public Object nextElement() {
        return iterator.next();
    }
}
/home/a/Objetos2/po2-2020-s2/tp7/ej6/respuesta.drawio