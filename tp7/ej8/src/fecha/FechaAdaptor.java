package fecha;

import org.joda.time.LocalDate;

public class FechaAdaptor implements IFecha {
    private LocalDate date;

    FechaAdaptor(int anio, int mes, int dia){
        this.date = new LocalDate(anio, mes, dia);
    }

    @Override
    public IFecha restarDias(int dias) {
        LocalDate dateMenosDias = date.minusDays(dias);
        return  new FechaAdaptor(
            dateMenosDias.year().get(),
            dateMenosDias.monthOfYear().get(),
            dateMenosDias.dayOfMonth().get()
        );
    }

    private LocalDate dtFromIFecha(IFecha fecha) {
        return new LocalDate(fecha.anio(), fecha.mes(), fecha.dia());
    }

    @Override
    public boolean antedDeAhora() {
        return date.isBefore(LocalDate.now());
    }

    @Override
    public boolean antesDe(IFecha fecha) {
        return date.isBefore(dtFromIFecha(fecha));
    }

    @Override
    public boolean despuesDeAhora() {
        return date.isAfter(LocalDate.now());
    }

    @Override
    public boolean despuesDeFecha(IFecha fecha) {
        return date.isAfter(dtFromIFecha(fecha));
    }

    @Override
    public int dia() {
        return date.dayOfMonth().get();
    }

    @Override
    public int mes() {
        return date.monthOfYear().get();
    }

    @Override
    public int anio() {
        return date.year().get();
    }
}
