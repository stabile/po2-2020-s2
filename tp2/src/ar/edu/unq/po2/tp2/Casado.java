package ar.edu.unq.po2.tp2;

public class Casado extends EstadoCivil {
    private Double asignacionPorConyuge = 100.0d;

    public Double asignacionPorConyuge() { return this.asignacionPorConyuge; }
}
