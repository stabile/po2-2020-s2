package ar.edu.unq.po2.tp2;

import java.time.LocalDate;

public class EmpleadoPermanente extends Empleado{
    private Integer cantidadDeHijos;
    private Integer añosDeAntigüedad;

    public EmpleadoPermanente(String nombre, String direccion, EstadoCivil estadoCivil, LocalDate fechaDeNacimiento, Double sueldoBasico,
                              Integer cantidadDeHijos, Integer antigüedad) {
        super(nombre, direccion, estadoCivil, fechaDeNacimiento, sueldoBasico);
        cantidadDeHijos(cantidadDeHijos);
        antigüedad(antigüedad);
    }

    @Override
    public Double sueldoBruto() { return sueldoBasico() + salarioFamiliar() ;}

    private Double salarioFamiliar() { return estadoCivil().asignacionPorConyuge() + asignacionPorHijo() + asignacionPorAntiguedad(); }

    private Double asignacionPorAntiguedad() { return 50d * antigüedad(); }

    private Double asignacionPorHijo() { return 150.0d * cantidadDeHijos(); }

    private Integer cantidadDeHijos() { return this.cantidadDeHijos; }

    @Override
    protected double obraSocial() { return (sueldoBruto() * 0.1d) + (20d * cantidadDeHijos()) ; }

    @Override
    protected double aporteJubilatorio() { return sueldoBruto() * 0.15d ; }

    private void cantidadDeHijos(Integer cantidadDeHijos) { this.cantidadDeHijos = cantidadDeHijos; }

    private void antigüedad(Integer añosDeAntigüedad) { this.añosDeAntigüedad = añosDeAntigüedad; }

    private Integer antigüedad() { return this.añosDeAntigüedad; }
}

