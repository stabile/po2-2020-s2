package ar.edu.unq.po2.tp2;

import java.time.LocalDate;
import java.time.Period;

public abstract class Empleado {

    private String nombre;
    private String direccion;
    private LocalDate fechaDeNacimiento;
    private Double sueldoBasico;
    private EstadoCivil estadoCivil;

    public Empleado(String nombre, String direccion, EstadoCivil estadoCivil, LocalDate fechaDeNacimiento, Double sueldoBasico) {
        super();
        nombre(nombre);
        direccion(direccion);
        fechaDeNacimiento(fechaDeNacimiento);
        sueldoBasico(sueldoBasico);
        estadoCivil(estadoCivil);
    }

    public abstract Double sueldoBruto();

    protected abstract double obraSocial();

    protected abstract double aporteJubilatorio();

    public Double sueldoNeto() { return sueldoBruto() - retenciones(); }

    private double retenciones() { return obraSocial() + aporteJubilatorio(); }

    protected Double sueldoBasico() { return this.sueldoBasico; }

    private void sueldoBasico(Double sueldoBasico) { this.sueldoBasico = sueldoBasico; }

    private void fechaDeNacimiento(LocalDate fechaDeNacimiento) { this.fechaDeNacimiento = fechaDeNacimiento; }

    private LocalDate fechaDeNacimiento(){ return this.fechaDeNacimiento; }

    private void nombre(String nombre){
        this.nombre = nombre;
    }

    private void estadoCivil(EstadoCivil unEstadoCivil) { this.estadoCivil = unEstadoCivil; }

    protected EstadoCivil estadoCivil() { return this.estadoCivil; }

    public String nombre() { return nombre; }

    private void direccion(String direccion){
        this.direccion = direccion;
    }

    public String direccion() { return direccion; }

    public Integer edad() { return Period.between(fechaDeNacimiento(), LocalDate.now()).getYears(); }

}
