package ar.edu.unq.po2.tp2;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.time.Period;

public class EmpleadoTest {

    Empleado empleadoPermanente;
    Empleado empleadoPermanente1;
    Empleado empleadoPermanente2;

    @BeforeEach
    void setUp() {
        empleadoPermanente = new EmpleadoPermanente("Juan Palotes","Aqui o alla", new Casado(), null, 1000d, 0, 1); 
        empleadoPermanente2 = new EmpleadoPermanente("Juan Palotes","Aqui o alla", new Casado(), null, 1000d, 4, 9); 
    }


    @Test
    void nuevoEmpleadoComletoNoEsNull(){
        assertNotNull(empleadoPermanente);
    }

    @Test   
    void nuevoEmpleadoNacidoHoyTieneEdadCero(){
        Empleado empleado = new EmpleadoPermanente(null, null, null, LocalDate.now(), null, null, null);
        assertEquals(0, empleado.edad());
    }

    @Test
    void nuevoEmpleadoNacidoHaceDiezYOchoAñosTieneHoyDiezYOchoAños(){
        Empleado empleado = new EmpleadoPermanente(null, null, null, LocalDate.now().minusYears(18), null, null, null);
        assertEquals(18, empleado.edad());
    }

    @Test
    void nuevoEmpleadoDePlantaPermanenteExiste(){
        assertNotNull(empleadoPermanente);
    }

    @Test
    void nuevoEmpleadoTieneNombre(){
        assertEquals("Juan Palotes", empleadoPermanente.nombre());
    }

    @Test
    void nuevoEmpleadoTieneDireccion(){
        assertEquals("Aqui o alla", empleadoPermanente.direccion());
    }

    @DisplayName("Emp Permanente tiene sueldo bruto")
    @Test
    void nuevoEmpleadoPermanenteTieneSueldoBruto(){
        assertEquals(1150.0d, empleadoPermanente.sueldoBruto());
    }


    @DisplayName("Emp Permanente tine sueldo bruto con hijos y antigüedad")
    @Test
    void testhijosyatigüedad() {
        Empleado empleadoPermanente2 = new EmpleadoPermanente("Juan Palotes","Aqui o alla", new Casado(), null, 1000d, 4, 9); 
        assertEquals(2150.0d, empleadoPermanente2.sueldoBruto());
    }

    @DisplayName("EmpPermanente tiene sueldo neto")
    @Test
    void testSueldoNeto() {
        empleadoPermanente = new EmpleadoPermanente("Juan Palotes","Aqui o alla", new Casado(), null, 1000d, 0, 1); 
        assertEquals(862.5d, empleadoPermanente.sueldoNeto());
    }

    @Test
    void casadoTieneAsignacionPorConyugueDe100(){
        EstadoCivil estadoCivil = new Casado();
        assertEquals(100.0d, estadoCivil.asignacionPorConyuge());
    }

    @Test 
    void otroEstadoCivilTieneAsignacionPorConyugeDe0(){
        EstadoCivil es = new OtroEstado();
        assertEquals(0.0d, es.asignacionPorConyuge());
    }

}
