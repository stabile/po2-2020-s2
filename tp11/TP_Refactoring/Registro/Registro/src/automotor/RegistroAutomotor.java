package automotor;

import java.time.LocalDate;

public class RegistroAutomotor {

	public Boolean debeRealizarVtv(Vehiculo vehiculo, LocalDate fecha) {

//		LocalDate fechaFabricacion = vehiculo.getFechaFabricacion();
//		Boolean esVehiculoPolicial = vehiculo.esVehiculoPolicial();
//		String ciudadRadicacion = vehiculo.ciudadRadicacion();

		//... Smells: Large Method , Feature Envy
		//... Refactor realizado:
		//... Primero se hizo un Extract Method sobre el concepto del comentario
		//... como es aconsejado en Long Method smell (pg 64 ed1)
		//... Luego se realizo Move Method
		//... A la vez se soluciono el smell de Vehiculo que era una Data Class
		//... Por eso ademas se realizo Hide Method en los getters en Vehiculo

		// vehiculos policiales no realizan vtv, ya que tienen otro tipo de control
		// sólo realizan vtv los vehículos con más de 1 anio de antiguedad y radicados
		// en 'Buenos Aires'
//		return (!esVehiculoPolicial && fecha.minusYears(1).isAfter(fechaFabricacion)
//				&& ciudadRadicacion.equalsIgnoreCase("Buenos Aires"));

        return vehiculo.debeRealizarVTV(fecha);

	}

}
