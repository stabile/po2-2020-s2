package automotor;

import java.time.LocalDate;

public class Vehiculo {
	private Boolean esVehiculoPolicial;
	private LocalDate fechaFabricacion;	
	private String ciudadRadicacion; 
	
	public Vehiculo(Boolean esVehiculoPolicial, LocalDate fechaFabricacion, String ciudadRadicacion) {
		this.esVehiculoPolicial = esVehiculoPolicial;
		this.fechaFabricacion = fechaFabricacion;
		this.ciudadRadicacion = ciudadRadicacion;
	}

	private Boolean esVehiculoPolicial() {
		return esVehiculoPolicial;
	}

	private LocalDate getFechaFabricacion() {
		return fechaFabricacion;
	}

	private String ciudadRadicacion() {
		return ciudadRadicacion;
	}

	//... Se hizo Move Method desde RegistroAutomotor
	//... Y Hide Method en los getters
	//... Y Comments To Good Names :)
	Boolean debeRealizarVTV(LocalDate fecha){
		return  noEsVehiculoPolicial()
			&&  esMasAntiguoQueUnAño(fecha)
			&&  estaRadicadoEnBuenosAires();
	}

	private Boolean noEsVehiculoPolicial() { return !esVehiculoPolicial(); }

	private Boolean esMasAntiguoQueUnAño(LocalDate fecha) {
		return fecha.minusYears(1).isAfter(getFechaFabricacion());
	}

	private Boolean estaRadicadoEnBuenosAires() {
		return ciudadRadicacion().equalsIgnoreCase("Buenos Aires");
	}
}
