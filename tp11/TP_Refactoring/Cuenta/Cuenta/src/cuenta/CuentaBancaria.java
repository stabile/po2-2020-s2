package cuenta;

public abstract class CuentaBancaria {
	protected HistorialMovimientos historialDeMovimientos;
	protected Notificador notificador;
	protected int saldo;
	
	public CuentaBancaria(HistorialMovimientos historialDeMovimientos, Notificador notificador, Integer saldo) {
		super();
		this.historialDeMovimientos = historialDeMovimientos;
		this.notificador = notificador;
		this.saldo = saldo;
	}

	public int getSaldo() {
		return saldo;
	}

	//... Duplicate code en hermanas
	//... Aplicamos Form Template Method
	//... 1) cambiar extraer(...) de primitive a hook. Test
	//... 2) Pull Up deducción de saldo y mensajes a historialDeMovimientos y
	//... notificador. Test
	//... 3) Extract Method de las condiciones en las hermanas. Test
	//... 4) Hacer el método-condición primitive. Compilar
	//... 5) Subir el condicional al template method: extraer(...). Test
	//... 6) Remover extraer(...) de las hermanas. Test
	public void extraer(Integer monto) {
		if (puedeExtraer(monto)) {
			this.saldo = saldo - monto;
			this.historialDeMovimientos.registrarMovimiento("Extracción", monto);
			this.notificador.notificarNuevoSaldoACliente(this);
		}
	}

	protected abstract Boolean puedeExtraer(Integer monto);
}
